#! must be sourced
[ -z "$TDAQ_INST_PATH" ] && echo "setup the tdaq release" || (

    if [ -z "$TDAQ_SETUP_POINT1" ]
    then
        echo "test outside P1"
        export CORAL_DBLOOKUP_PATH="/afs/cern.ch/user/a/attrgcnf/.dbauth/run3/read/"
    else
        echo "test at P1"
        export CORAL_DBLOOKUP_PATH="/det/tdaq/hlt/trigconf/run3/read/"
        echo "to test a lcoal version of the TriggerPanel, set the environment LOCAL_TPJAR, e.g. like this:"
        echo "export LOCAL_TPJAR=`pwd`/TriggerPanel.jar"
    fi

    export CORAL_AUTH_PATH=${CORAL_DBLOOKUP_PATH}

    testClass=triggerpanel.ConnectionManager
    _java=${JAVA_HOME_LINUX}/bin/java

    cmd="${_java} -classpath ${LOCAL_TPJAR}:${TDAQ_CLASSPATH} ${testClass}"

    exec ${cmd}
)
