# This script starts the test dialog panel, to aid GUI development

testClass=${TESTCLASS:-TestDialogs}

${JAVA_HOME_LINUX}/bin/java -classpath ${CLASSPATH} triggerpanel.${testClass}
