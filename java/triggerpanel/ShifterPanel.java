package triggerpanel;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class ShifterPanel extends JPanel {

    public ShifterPanel() {
        initComponents();
    }

    public void setKeys(RunState readystate, LVL level, KeyDescriptor kd) {
    }

    public void setCommitMessage(String msg) {
        if(msg==null) { // the default
            msg = "Please 'Commit & Reload' before leaving this panel";
        }
        lbl_commitMsg.setText(msg);
    }

    /********** private methods **********/
    final private void initComponents() {

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        Box panel_main = createVerticalBox(null);
        panel_main.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        Box panel_currentMenu = createHorizontalBox(null);
        JPanel panel_psSelection = new JPanel();
        panel_psSelection.setAlignmentX(Box.LEFT_ALIGNMENT);
        Box panel_ctpRow = createHorizontalBox(null);
        Box panel_version = createHorizontalBox(null);

        add(panel_messages);
        panel_main.add(panel_currentMenu);
        panel_main.add(Box.createVerticalStrut(5));
        panel_main.add(panel_currentPS);
        panel_main.add(Box.createVerticalStrut(5));
        panel_main.add(panel_psSelection);
        panel_main.add(Box.createVerticalStrut(5));
        panel_main.add(panel_ctpRow);
        panel_main.add(panel_version);
        add(panel_main);

        /***** commit message line *******/
        lbl_commitMsg.setForeground(new Color(247, 21, 218));
        lbl_version.setForeground(new Color(138, 117, 117));
        lbl_version.setAlignmentY(1f);
        panel_messages.add(Box.createRigidArea(new Dimension(10,40)));
        panel_messages.add(lbl_commitMsg);
        panel_messages.add(Box.createHorizontalGlue());
        lbl_commitMsg.addPropertyChangeListener("text", (evt) -> {
            panel_messages.setVisible(! evt.getNewValue().toString().isEmpty());
        });

        /******** Trigger menu line *******/
        setComponentBorderTitle(panel_currentMenu, "Trigger menu");
        panel_currentMenu.add(Box.createRigidArea(new Dimension(10,26)));
        panel_currentMenu.add(createLabel("SMK", new Font("Dialog", Font.BOLD, 14)));
        panel_currentMenu.add(Box.createHorizontalStrut(10));
        panel_currentMenu.add(lbl_curSmk);
        panel_currentMenu.add(Box.createHorizontalStrut(10));
        panel_currentMenu.add(lbl_curMenu);
        panel_currentMenu.add(Box.createHorizontalGlue());
        btn_changeSMK.setAlignmentY(0.7f);
        panel_currentMenu.add(btn_changeSMK);
        panel_currentMenu.add(Box.createHorizontalStrut(10));

        /******** Line with current L1 and HLT prescales ********/
        setComponentBorderTitle(panel_currentPS, "Prescale sets");
        panel_currentPS.add(Box.createRigidArea(new Dimension(10,26)));
        panel_currentPS.add(createLabel("L1", new Font("Dialog", Font.BOLD, 14)));
        panel_currentPS.add(Box.createHorizontalStrut(10));
        panel_currentPS.add(lbl_curL1Psk);
        panel_currentPS.add(Box.createHorizontalGlue());
        panel_currentPS.add(createLabel("HLT", new Font("Dialog", Font.BOLD, 14)));
        panel_currentPS.add(Box.createHorizontalStrut(10));
        panel_currentPS.add(lbl_curHltPsk);
        panel_currentPS.add(Box.createHorizontalGlue());
        panel_currentPS.add(lbl_pskError);
        panel_currentPS.add(Box.createHorizontalStrut(10));

        /********** prescale selection panels ********/
        panel_psSelection.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.LINE_START;
        RunState[] states = {RunState.STANDBY, RunState.EMITTANCE, RunState.PHYSICS};
        int line = 0;
        for(RunState state : states) {
            /******** build the full line consisting of label and PS box *******/
            final JLabel lbl_mode = createLabel(state.toString(), new Font("Dialog", Font.BOLD, 18));
            lbMap_runModes.put(state, lbl_mode);
            lbl_mode.setForeground(new Color(86, 185, 18));
            gbc.gridy = line++;
            gbc.gridx = 0; gbc.insets = new Insets(0,10,0,10); gbc.weightx = 0; 
            gbc.fill = GridBagConstraints.NONE; gbc.anchor = GridBagConstraints.WEST;
            panel_psSelection.add(lbl_mode, gbc);
            gbc.gridx = 1; gbc.insets = new Insets(3,0,3,0);   gbc.weightx = 1;
            gbc.fill = GridBagConstraints.HORIZONTAL; gbc.anchor = GridBagConstraints.EAST;
            panel_psSelection.add( createPrescalePanel(state), gbc );
        }

        /*********** Line with CTP configuration fields *********/
        JPanel panel_bunchGroup = createCtpBox("BG", "Bunch group set", btn_changeBGS, 350);
        JPanel panel_dt = createCtpBox("DT", "Deadtime config", btn_changeDT, 350);
        Box panel_actions = createVerticalBox(null);  // change LB and create new BGS buttons

        panel_ctpRow.add(panel_bunchGroup);
        panel_ctpRow.add(Box.createHorizontalStrut(5));
        panel_ctpRow.add(panel_lbLength);
        panel_ctpRow.add(Box.createHorizontalStrut(5));
        panel_ctpRow.add(panel_dt);
        panel_ctpRow.add(Box.createHorizontalStrut(55));
        panel_ctpRow.add(Box.createHorizontalGlue());
        panel_ctpRow.add(panel_actions);

        JLabel lbl_shifter = createLabel("Trigger shifter action only:", new Font("Ubuntu", 0, 10));
        lbl_shifter.setAlignmentX(1f);
        btn_startNewLB.setAlignmentX(1f);
        btn_createBGS.setAlignmentX(1f);
        panel_actions.add(btn_startNewLB);
        panel_actions.add(Box.createVerticalStrut(10));
        panel_actions.add(lbl_shifter);
        panel_actions.add(btn_createBGS);

        /***** last line with version *******/
        panel_version.add(Box.createHorizontalGlue());
        panel_version.add(lbl_version);

        
        Dimension mainMaxSize = new Dimension(
            panel_main.getMaximumSize().width,
            panel_main.getPreferredSize().height);
        panel_main.setMaximumSize(mainMaxSize);
        setPreferredSize(new Dimension(getPreferredSize().width,mainMaxSize.height+40));
        setCommitMessage("");
    }

    private Box createVerticalBox(Color color) {
        Box box = new Box(BoxLayout.PAGE_AXIS);
        box.setBackground(color); 
        box.setOpaque(true);
        return box;
    }

    private Box createHorizontalBox(Color color) {
        Box box = new Box(BoxLayout.X_AXIS);
        if(color != null) {
            box.setBackground(color); 
            box.setOpaque(true);
        }
        box.setAlignmentX(Box.LEFT_ALIGNMENT);
        return box;
    }

    private void setComponentBorderTitle(JComponent comp, String title) {
        comp.setBorder(BorderFactory.createTitledBorder(
            null, title, 
            TitledBorder.DEFAULT_JUSTIFICATION,
            TitledBorder.DEFAULT_POSITION, 
            new Font("Dialog", Font.BOLD, 12), new Color(74, 23, 229))
        );        
    }

    private JLabel createLabel(final String text, final Font font) {
        JLabel label = new JLabel(text);
        if(font != null) {
            label.setFont(font);
        }
        return label;
    }

    /**** generation of the prescale key box to be called for all 3 modes ******/
    private JPanel createPrescalePanel(final RunState mode) {
        final JLabel lbl_l1 = createLabel("L1", new Font("Dialog", Font.PLAIN, 14));
        final JLabel lbl_hlt = createLabel("HLT", new Font("Dialog", Font.PLAIN, 14));
        final JLabel lbl_l1psk = createLabel("1000", new Font("Dialog", Font.BOLD, 14));
        final JLabel lbl_hltpsk = createLabel("10000", new Font("Dialog", Font.BOLD, 14));
        final JLabel lbl_l1psName = createLabel("Name of the L1 prescale set", new Font("Dialog", Font.PLAIN, 12));
        final JLabel lbl_hltpsName = createLabel("Name of the HLT prescale set", new Font("Dialog", Font.PLAIN, 12));
        lbl_l1psk.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl_hltpsk.setHorizontalAlignment(SwingConstants.RIGHT);
        lbl_l1psk.setPreferredSize(new Dimension(60, 17));
        lbl_hltpsk.setPreferredSize(new Dimension(60, 17));
        final JButton changeButton = new JButton("Change");

        /******** build the PS box ********/
        JPanel panel_ps = new JPanel();
        setComponentBorderTitle(panel_ps, "Prescale set for " + mode);
        panel_ps.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = GridBagConstraints.WEST;
        gbc.insets = new Insets(3, 10, 3, 0);
        gbc.gridx = 0; gbc.gridy = 0; panel_ps.add(lbl_l1, gbc);
        gbc.gridx = 0; gbc.gridy = 1; panel_ps.add(lbl_hlt, gbc);
        gbc.gridx = 1; gbc.gridy = 0; panel_ps.add(lbl_l1psk, gbc);
        gbc.gridx = 1; gbc.gridy = 1; panel_ps.add(lbl_hltpsk, gbc);
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 2; gbc.gridy = 0; panel_ps.add(lbl_l1psName, gbc);
        gbc.gridx = 2; gbc.gridy = 1; panel_ps.add(lbl_hltpsName, gbc);
        gbc.weightx = 0.0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridheight = 2;
        gbc.insets = new Insets(10, 0, 17, 12);
        gbc.gridx = 3; gbc.gridy = 0; panel_ps.add(changeButton, gbc);

        pspanels.put(mode, panel_ps);
        lbMap_l1psKeys.put(mode, lbl_l1psk);
        lbMap_hltpsKeys.put(mode, lbl_hltpsk);
        lbMap_l1psNames.put(mode, lbl_l1psName);
        lbMap_hltpsNames.put(mode, lbl_hltpsName);
        btnMap_pskChange.put(mode, changeButton);

        return panel_ps;
    }

    class MinSizePanel extends JPanel {
        int m_prefWidth;
        MinSizePanel(LayoutManager mgr, int prefWidth) {
            super(mgr);
            m_prefWidth = prefWidth;
        }
        @Override
        public Dimension getPreferredSize() {
            Dimension original = super.getPreferredSize();
            return new Dimension(m_prefWidth, (int)original.getHeight());
        }
        @Override
        public Dimension getMaximumSize() {
            Dimension original = super.getMaximumSize();
            return new Dimension(original.width, 120);
        }
    }

    private JPanel createCtpBox(String ctpinfo, String title, JButton btn, int prefWidth) {
        MinSizePanel panel = new MinSizePanel(new GridBagLayout(), 180);
        setComponentBorderTitle(panel, title);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty=0;
        JLabel label = createLabel("value", new Font("Dialog", Font.BOLD, 18));
        gbc.gridx = 0; gbc.gridy = 0; gbc.weightx=1f; gbc.insets = new Insets(2, 10, 5, 5);
        gbc.anchor = GridBagConstraints.BASELINE_LEADING;
        panel.add(label, gbc);
        gbc.gridx = 0; gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.BASELINE_TRAILING;
        panel.add(btn, gbc);
        lbMap_ctpValues.put(ctpinfo,label);
        return panel;
    }

    /***** protected data *******/
    protected final JLabel label_physicsError = new JLabel("Physics error label");
    protected HashMap<RunState, JLabel> lbMap_runModes = new HashMap<>(3); // the mode labels
    protected HashMap<RunState, JLabel> lbMap_l1psKeys = new HashMap<>(3); // the L1 PSK labels
    protected HashMap<RunState, JLabel> lbMap_hltpsKeys = new HashMap<>(3); // the HLT PSK labels
    protected HashMap<RunState, JLabel> lbMap_l1psNames = new HashMap<>(3); // the L1 PS names labels
    protected HashMap<RunState, JLabel> lbMap_hltpsNames = new HashMap<>(3); // the HLT PS names labels
    protected HashMap<RunState, JButton> btnMap_pskChange = new HashMap<>(3); // the PS change buttons
    protected HashMap<String, JLabel> lbMap_ctpValues = new HashMap<>(3);

    protected JButton btn_changeBGS = new JButton("Change");
    protected JButton btn_changeDT = new JButton("Change");
    protected JButton btn_changeLBLength = new JButton("Change");
    protected JButton btn_changeSMK = new JButton("Change");
    protected JButton btn_startNewLB = new JButton("Start new LB");
    protected JButton btn_createBGS = new JButton("Create new BG set");

    // menu line labels
    protected JLabel lbl_curSmk = createLabel("SMK", new Font("Dialog", Font.BOLD, 18));
    protected JLabel lbl_curMenu = createLabel("Menu name", null);
    // psk line labels
    protected JLabel lbl_curL1Psk = createLabel("L1PSK", new Font("Dialog", Font.BOLD, 18));
    protected JLabel lbl_curHltPsk = createLabel("HLTPSK", new Font("Dialog", Font.BOLD, 18));
    protected JLabel lbl_pskError = createLabel("PSK error", new Font("Dialog", Font.ITALIC, 14));
    // message line labels
    private JLabel lbl_commitMsg = createLabel("Please 'Commit & Reload' before leaving this panel", new Font("Dialog", Font.BOLD, 18));
    protected JLabel lbl_version = createLabel(Utils.getBuiltDate(), new Font("Dialog", Font.PLAIN, 10));

    protected Box panel_currentPS = createHorizontalBox(null);
    protected Box panel_messages = createHorizontalBox(null);
    protected JPanel panel_lbLength = createCtpBox("LB", "LB length", btn_changeLBLength, 350);


    /***** private data *****/
    private HashMap<RunState, JPanel> pspanels = new HashMap<>(3);

    // list of open ps selection dialogs so their state can be updated from the main panel
    private ArrayList<PSSelectionDialog> openPsDialogs = new ArrayList<PSSelectionDialog>();
    public ArrayList<PSSelectionDialog> getOpenPsDialogs() {
        return openPsDialogs;
    }
    public void addOpenPsDialog(PSSelectionDialog dialog) {
        this.openPsDialogs.add(dialog);
    }
    public void removeOpenPsDialog(PSSelectionDialog dialog) {
        this.openPsDialogs.remove(dialog);
    }

    JFrame getParentFrame(final Component comp) {
        JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, comp);
        if(parent==null) {
            System.err.println("No parent found");
            parent = new JFrame();
        }
        return parent;
    }

    /***** for testing *****/
    public void showDialogNearMouse(JDialog dialog) {
        showDialogNearMouse(-20, -70, dialog);
    }
    public void showDialogNearMouse(int xoffset, int yoffset, JDialog dialog) {
        Point loc = MouseInfo.getPointerInfo().getLocation();
        dialog.setLocation(loc.x + xoffset, loc.y + yoffset);
        dialog.setVisible(true);
    }

    void addTestActions() {
        btn_changeSMK.addActionListener((evt) -> {
            new SMKSelectionDialog(new JFrame(), true).setVisible(true);
        });
        btn_startNewLB.addActionListener((evt) -> {
            setCommitMessage(lbl_commitMsg.getText().isEmpty()?null:"");
        });
        for(HashMap.Entry<RunState,JButton> entry: btnMap_pskChange.entrySet()) {
            entry.getValue().addActionListener((evt) -> {
                PSSelectionDialog dialog = new PSSelectionDialog(entry.getKey(), RunState.STANDBY, getParentFrame(this), true);
                addOpenPsDialog(dialog);
                dialog.setVisible(true);
                removeOpenPsDialog(dialog);
            });
        }
        btn_changeBGS.addActionListener((evt) -> {
            showDialogNearMouse(new BGSelectionDialog(getParentFrame(this), true));
        });
        btn_changeLBLength.addActionListener((evt) -> {
            showDialogNearMouse(new LumiBlockIntervalDialog(getParentFrame(this), true));
        });
        btn_changeDT.addActionListener((evt) -> {
            showDialogNearMouse(new CTPDeadtimeDialog(getParentFrame(this), true));
        });
        btn_createBGS.addActionListener((evt) -> {
            BunchGroupUploadDialog dialog = new BunchGroupUploadDialog(getParentFrame(this), false);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
        });
    };


    public static void main(String[] args) {
        ShifterPanel shpanel = new ShifterPanel();
        shpanel.addTestActions();

        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TestDialogs.class.getName()).log(Level.SEVERE, null, ex);
        }

        javax.swing.SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame();
            JDialog dialog = new JDialog(frame, false);
            JScrollPane sp = new JScrollPane(shpanel);
            sp.getVerticalScrollBar().setUnitIncrement(16); // make scrolling faster
            sp.getHorizontalScrollBar().setUnitIncrement(16); // make scrolling faster
            dialog.add(sp);
            dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setVisible(true);
            dialog.setAlwaysOnTop(true);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        });
    }
}
