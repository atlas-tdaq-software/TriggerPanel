package triggerpanel;

import Igui.IguiLogger;
import config.*;

public class LBLengthVariable extends OKSVariable<Integer> {

    public LBLengthVariable() {
        super("LBLength");
    }

    @Override
    protected void writeToRDB(Integer value, Configuration rdb_rw) {
        IguiLogger.warning("Only the CTP experts can update the LB length in OKS");
        return;
    }

    @Override
    protected Integer readFromRDB(Configuration rdb) {
        ConfigObject ctpLBLengthConfig = TPGlobal.getConfigHelper().getCTPLBLength(rdb);
        Integer lbLength = -1;
        if(ctpLBLengthConfig != null) {
            try {
                lbLength = ctpLBLengthConfig.get_int("Period");
            }
            catch(ConfigException ex) {
                IguiLogger.warning("Did not find LB length 'Period' in CTP Lumi block configuration. " + ex);
            }
        }
        return lbLength;
    }
    
}
