/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import Igui.IguiLogger;
import static ers.Logger.error;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author stelzer
 */
final class PrescaleAliasLoader {

    private class RowData {

        String name;
        String type;
        int psk;
        float lumiMin;
        float lumiMax;
        String comment;
    }

    PrescaleSetAliasHandle load(final int smk) throws SQLException {
        System.out.println("Loading aliases for SMK " + smk);
        if(smk == 0){
            return new PrescaleSetAliasHandle();
        }
        PrescaleSetAliasHandle h = new PrescaleSetAliasHandle();
        h.setSmk(smk);

        // boolean connectedHere = TPGlobal.getDbKeysLoader().DbConnect();

        ArrayList<Integer> aliases = readValidAliases(smk);
        //System.out.println("Get L1 Data");
        //List<RowData> l1Data = readDBL1(smk);
        List<RowData> l1Data = readDBL1forAlias(aliases);
        //System.out.println("Get HLT Data");
        //List<RowData> hltData = readDBHLT(smk);
        List<RowData> hltData = readDBHLTforAlias(aliases);
        /*if (connectedHere) {
            TPGlobal.getDbKeysLoader().DbDisconnect();
        }*/

        for (RowData d : l1Data) {
            //System.out.println("Incoming L1: " + d.type + " " + d.name + " " + d.psk + " " + d.lumiMin + " " + d.lumiMax);
            h.addLumiRange(d.type, d.name, 1, d.psk, d.lumiMin, d.lumiMax, d.comment);
        }

        for (RowData d : hltData) {
            //System.out.println("Incoming HLT: " + d.type + " " + d.name + " " + d.psk + " " + d.lumiMin + " " + d.lumiMax);
            h.addLumiRange(d.type, d.name, 2, d.psk, d.lumiMin, d.lumiMax, d.comment);
        }

        h.sortAndPack();

        IguiLogger.info("Loaded " + h.toString());
        System.out.println("Loaded " + aliases.size() + " aliases for SMK " + smk);

        return h;
    }

    ArrayList<Integer> readValidAliases(final int smk) throws SQLException {
        String query = "select SMT_ID,uber.PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from ( "
                + "select distinct SMT_ID,PSA_ID,PSA_NAME,PSA_NAME_FREE,PSA_COMMENT from (select SMT_ID,L1PSA_ALIAS_ID "
                + "from (select SMT_ID,L1TM2PS_ID from (select SMT_ID,L1MT_TRIGGER_MENU_ID from (select SMT_ID,SMT_L1_MASTER_TABLE_ID from "
                + "SUPER_MASTER_TABLE where SMT_ID = ?) lhs "
                + " join (select l1mt_id,l1mt_trigger_menu_id from L1_MASTER_TABLE) rhs"
                + " on lhs.smt_l1_master_table_id = rhs.l1mt_id) lhs1"
                + " join (select l1tm2ps_id,l1tm2ps_trigger_menu_id from L1_TM_TO_PS) rhs1"
                + " on lhs1.l1mt_trigger_menu_id = rhs1.l1tm2ps_trigger_menu_id) step1"
                + " join (select L1PSA_L1TM2PS_ID,L1PSA_ALIAS_ID from L1_PRESCALE_SET_ALIAS) step2"
                + " on step1.l1tm2ps_id = step2.L1PSA_L1TM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.L1PSA_ALIAS_ID = bigstep2.PSA_ID) uber"
                + " join "
                + " (select distinct PSA_ID from (select SMT_ID,HPSA_ALIAS_ID from (select SMT_ID,HTM2PS_ID from "
                + "(select SMT_ID,HMT_TRIGGER_MENU_ID from (select SMT_ID,SMT_HLT_MASTER_TABLE_ID from SUPER_MASTER_TABLE where SMT_ID = ?) lhs"
                + " join (select hmt_id,hmt_trigger_menu_id from HLT_MASTER_TABLE) rhs"
                + " on lhs.smt_hlt_master_table_id = rhs.hmt_id) lhs1"
                + " join (select htm2ps_id,htm2ps_trigger_menu_id from HLT_TM_TO_PS) rhs1"
                + " on lhs1.hmt_trigger_menu_id = rhs1.htm2ps_trigger_menu_id) step1"
                + " join (select HPSA_HTM2PS_ID,HPSA_ALIAS_ID from HLT_PRESCALE_SET_ALIAS) step2"
                + " on step1.htm2ps_id = step2.HPSA_HTM2PS_ID) bigstep1"
                + " join (select * from PRESCALE_SET_ALIAS) bigstep2"
                + " on bigstep1.HPSA_ALIAS_ID = bigstep2.PSA_ID) uber1"
                + " on uber.PSA_ID = uber1.PSA_ID";
        query = ConnectionManager.getInstance().fixSchemaName(query);

        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setFetchSize(200);
            ps.setInt(1, smk);
            ps.setInt(2, smk);
            ResultSet rset = ps.executeQuery();

            ArrayList<Integer> results = new ArrayList<>();
            while (rset.next()) {
                results.add(rset.getInt(2));
            }
            return results;
        }
    }

    List<RowData> readDBL1(final int smk) {

        List<RowData> data = new ArrayList<>();

        StringBuilder sb = new StringBuilder(1000);
        sb.append("SELECT DISTINCT ");
        sb.append("PSA.PSA_NAME,");
        sb.append("PSA.PSA_NAME_FREE,");
        sb.append("L1TM2PS.L1TM2PS_PRESCALE_SET_ID,");
        sb.append("L1PSA.L1PSA_LMIN,");
        sb.append("L1PSA.L1PSA_LMAX, ");
        sb.append("L1PSA.L1PSA_COMMENT ");
        sb.append("FROM ");
        sb.append("SUPER_MASTER_TABLE SMT, ");
        sb.append("L1_MASTER_TABLE L1MT, ");
        sb.append("L1_TM_TO_PS L1TM2PS, ");
        sb.append("L1_PRESCALE_SET_ALIAS L1PSA, ");
        sb.append("PRESCALE_SET_ALIAS PSA ");
        sb.append("WHERE SMT_ID = ? ");
        sb.append("AND SMT.SMT_L1_MASTER_TABLE_ID = L1MT.L1MT_ID ");
        sb.append("AND L1MT.L1MT_TRIGGER_MENU_ID = L1TM2PS.L1TM2PS_TRIGGER_MENU_ID ");
        sb.append("AND L1PSA.L1PSA_L1TM2PS_ID = L1TM2PS.L1TM2PS_ID ");
        sb.append("AND L1PSA.L1PSA_ALIAS_ID = PSA.PSA_ID ");
        sb.append("ORDER BY ");
        sb.append("PSA.PSA_NAME, L1PSA.L1PSA_LMIN");

        String query = ConnectionManager.getInstance().fixSchemaName(sb.toString());
        //System.out.println("TEST QUERY L1: " + query + " SMK " + smk);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            //ps.setFetchSize(200);
            ps.setInt(1, smk);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                final RowData rd = new RowData();
                rd.type = rset.getString(1);
                rd.name = rset.getString(2);
                rd.psk = rset.getInt(3);
                if (rset.getString(4).equals("~")) {
                    rd.lumiMin = 0;
                } else {
                    rd.lumiMin = Float.parseFloat(rset.getString(4));
                }
                if (rset.getString(5).equals("~")) {
                    rd.lumiMax = 0;
                } else {
                    rd.lumiMax = Float.parseFloat(rset.getString(5));
                }
                rd.comment = rset.getString(6);
                //System.out.println("DATA L1: " + rd.name + " " + rd.type + " " + rd.psk + " " + rd.lumiMin + " " + rd.lumiMax);
                data.add(rd);
            }
        } catch (SQLException ex) {
            error(new TriggerPanelIssue("SQLException when getting prescale aliases for smk " + smk, ex));
        }
        return data;
    }

    List<RowData> readDBHLT(final int smk) {

        List<RowData> data = new ArrayList<>();

        StringBuilder sb = new StringBuilder(1000);
        sb.append("SELECT DISTINCT PSA.PSA_NAME, PSA.PSA_NAME_FREE,");
        sb.append("HLTTM2PS.HTM2PS_PRESCALE_SET_ID, HLTPSA.HPSA_LMIN, HLTPSA.HPSA_LMAX, HLTPSA.HPSA_COMMENT ");
        sb.append("FROM ");
        sb.append("SUPER_MASTER_TABLE SMT, ");
        sb.append("HLT_MASTER_TABLE HLTMT, ");
        sb.append("HLT_TM_TO_PS HLTTM2PS, ");
        sb.append("HLT_PRESCALE_SET_ALIAS HLTPSA, ");
        sb.append("PRESCALE_SET_ALIAS PSA ");
        sb.append("WHERE SMT_ID = ? ");
        sb.append("AND SMT.SMT_HLT_MASTER_TABLE_ID = HLTMT.HMT_ID ");
        sb.append("AND HLTMT.HMT_TRIGGER_MENU_ID = HLTTM2PS.HTM2PS_TRIGGER_MENU_ID ");
        sb.append("AND HLTPSA.HPSA_HTM2PS_ID = HLTTM2PS.HTM2PS_ID ");
        sb.append("AND HLTPSA.HPSA_ALIAS_ID = PSA.PSA_ID ");
        sb.append("ORDER BY ");
        sb.append("PSA.PSA_NAME, HLTPSA.HPSA_LMIN");

        String query = ConnectionManager.getInstance().fixSchemaName(sb.toString());
        //System.out.println("TEST QUERY HLT: " + query + " SMK " + smk);

        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            //ps.setFetchSize(200);
            ps.setInt(1, smk);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                final RowData rd = new RowData();
                rd.type = rset.getString(1);
                rd.name = rset.getString(2);
                rd.psk = rset.getInt(3);
                if (rset.getString(4).equals("~")) {
                    rd.lumiMin = 0;
                } else {
                    rd.lumiMin = Float.parseFloat(rset.getString(4));
                }
                if (rset.getString(5).equals("~")) {
                    rd.lumiMax = 0;
                } else {
                    rd.lumiMax = Float.parseFloat(rset.getString(5));
                }
                rd.comment = rset.getString(6);
                //System.out.println("DATA HLT: " + rd.name + " " + rd.type + " " + rd.psk + " " + rd.lumiMin + " " + rd.lumiMax);
                data.add(rd);
            }
        } catch (SQLException ex) {
            error(new TriggerPanelIssue("SQLException when getting prescale aliases for smk " + smk, ex));
        }
        return data;
    }

    List<RowData> readDBL1forAlias(final ArrayList<Integer> aliases) {

        List<RowData> data = new ArrayList<>();
        if (aliases.size() > 1000) {
            TPL.error("This SMK has more than 1000 associated aliases, unable to process");
            return new ArrayList<>();
        }

        if (aliases.isEmpty()) {
            TPL.info("This SMK has no associated aliases, unable to proceed");
            return new ArrayList<>();
        }

        String toAdd = "?";
        if (aliases.size() > 1) {
            String str = ",?";
            String repeated = StringUtils.repeat(str, aliases.size() - 1);
            toAdd += repeated;
        }

        String query = "select PSA_NAME,PSA_NAME_FREE,L1TM2PS_PRESCALE_SET_ID,L1PSA_LMIN,L1PSA_LMAX,L1PSA_COMMENT from ("
                + "select * from (select * from (select PSA_ID,PSA_NAME,PSA_NAME_FREE from PRESCALE_SET_ALIAS where PSA_ID in (" + toAdd + ")) lhs "
                + "join (select L1PSA_ALIAS_ID,L1PSA_L1TM2PS_ID,L1PSA_LMIN,L1PSA_LMAX,L1PSA_COMMENT from L1_PRESCALE_SET_ALIAS) rhs "
                + "on lhs.PSA_ID = rhs.L1PSA_ALIAS_ID) uberleft "
                + "join (select L1TM2PS_ID,L1TM2PS_PRESCALE_SET_ID from L1_TM_TO_PS) uberright "
                + "on uberleft.L1PSA_L1TM2PS_ID = uberright.L1TM2PS_ID)";

        query = ConnectionManager.getInstance().fixSchemaName(query);
        //System.out.println("TEST QUERY L1: " + query + " SMK " + smk);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setFetchSize(200);
            Integer counter = 1;
            for (Integer val : aliases) {
                ps.setInt(counter, val);
                counter++;
            }
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                final RowData rd = new RowData();
                rd.type = rset.getString(1);
                rd.name = rset.getString(2);
                rd.psk = rset.getInt(3);
                if (rset.getString(4).equals("~")) {
                    rd.lumiMin = 0;
                } else {
                    rd.lumiMin = Float.parseFloat(rset.getString(4));
                }
                if (rset.getString(5).equals("~")) {
                    rd.lumiMax = 0;
                } else {
                    rd.lumiMax = Float.parseFloat(rset.getString(5));
                }
                rd.comment = rset.getString(6);
                //System.out.println("DATA L1: " + rd.name + " " + rd.type + " " + rd.psk + " " + rd.lumiMin + " " + rd.lumiMax);
                data.add(rd);
            }
        } catch (SQLException ex) {
            error(new TriggerPanelIssue("SQLException when getting L1 prescale aliases: ", ex));
        }
        return data;
    }

    List<RowData> readDBHLTforAlias(final ArrayList<Integer> aliases) {

        List<RowData> data = new ArrayList<>();
        if (aliases.size() > 1000) {
            TPL.error("This SMK has more than 1000 associated aliases, unable to process");
            return new ArrayList<>();
        }

        if (aliases.isEmpty()) {
            TPL.info("This SMK has no associated aliases, unable to proceed");
            return new ArrayList<>();
        }

        String toAdd = "?";
        if (aliases.size() > 1) {
            String str = ",?";
            String repeated = StringUtils.repeat(str, aliases.size() - 1);
            toAdd += repeated;
        }

        String query = "select PSA_NAME,PSA_NAME_FREE,HTM2PS_PRESCALE_SET_ID,HPSA_LMIN,HPSA_LMAX,HPSA_COMMENT from ("
                + "select * from (select * from (select PSA_ID,PSA_NAME,PSA_NAME_FREE from PRESCALE_SET_ALIAS where PSA_ID in (" + toAdd + ")) lhs "
                + "join (select HPSA_ALIAS_ID,HPSA_HTM2PS_ID,HPSA_LMIN,HPSA_LMAX,HPSA_COMMENT from HLT_PRESCALE_SET_ALIAS) rhs "
                + "on lhs.PSA_ID = rhs.HPSA_ALIAS_ID) uberleft "
                + "join (select HTM2PS_ID,HTM2PS_PRESCALE_SET_ID from HLT_TM_TO_PS) uberright "
                + "on uberleft.HPSA_HTM2PS_ID = uberright.HTM2PS_ID)";

        query = ConnectionManager.getInstance().fixSchemaName(query);
        //System.out.println("TEST QUERY HLT: " + query + " SMK " + smk);

        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setFetchSize(200);
            Integer counter = 1;
            for (Integer val : aliases) {
                ps.setInt(counter, val);
                counter++;
            }
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                final RowData rd = new RowData();
                rd.type = rset.getString(1);
                rd.name = rset.getString(2);
                rd.psk = rset.getInt(3);
                if (rset.getString(4).equals("~")) {
                    rd.lumiMin = 0;
                } else {
                    rd.lumiMin = Float.parseFloat(rset.getString(4));
                }
                if (rset.getString(5).equals("~")) {
                    rd.lumiMax = 0;
                } else {
                    rd.lumiMax = Float.parseFloat(rset.getString(5));
                }
                rd.comment = rset.getString(6);
                //System.out.println("DATA HLT: " + rd.name + " " + rd.type + " " + rd.psk + " " + rd.lumiMin + " " + rd.lumiMax);
                data.add(rd);
            }
        } catch (SQLException ex) {
            error(new TriggerPanelIssue("SQLException when getting HLT prescale aliases: ", ex));
        }
        return data;
    }
}
