package triggerpanel.test;

import java.io.File;
import java.sql.Connection;

import triggerpanel.ConnectionManager;
import triggerpanel.InitInfo;

public class TestDbConnection {

    static Connection connect(final String triggerdbAlias) throws Exception {
        System.out.println("---> Test connecting to " + triggerdbAlias);

        InitInfo initInfo = new InitInfo();
        initInfo.setConnectionFromCommandLine(triggerdbAlias);
            
        ConnectionManager.getInstance().connect(initInfo);

        return ConnectionManager.getInstance().getConnection();
    }

    static void testConnectTriggerDb() throws Exception {
        final String triggerdbAlias = "TRIGGERDB_RUN3";
        Connection connection = connect(triggerdbAlias);

        int schemaVersion = ConnectionManager.getInstance().getSchemaVersion(connection);
        System.out.println("Schema version: " + schemaVersion);

        connection.prepareStatement("Select 1 from dual").executeQuery();

        ConnectionManager.getInstance().disconnect();
    }

    public static void main(String[] args) {
        boolean printClassPath = false;
        if(printClassPath) {
            String classpath = System.getProperty("java.class.path");
            String[] classPathValues = classpath.split(File.pathSeparator);
            for (String string : classPathValues) {
                System.out.println("   " + string);
            }
        }
 
        try {
            testConnectTriggerDb();
        } catch (Exception e) {
            System.out.println("Exception caught when connecting to TriggerDB" + e);
            e.printStackTrace();
        }
    }

}
