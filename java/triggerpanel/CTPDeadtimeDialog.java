/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

public class CTPDeadtimeDialog extends JDialog {

    private String m_selection;
    private Map<String,String> m_selectionChoices = new LinkedHashMap<String,String>();

    final protected String getSelection() {
        return m_selection;
    }
    ActionListener rbActionListener = new ActionListener() {
        public void actionPerformed(ActionEvent actionEvent) {
            btnApply.setEnabled(true);
            AbstractButton button = (AbstractButton) actionEvent.getSource();
            m_selection = m_selectionChoices.get(button.getText());
        }
    };

    public CTPDeadtimeDialog(JFrame parent, boolean modal) {
        super(parent, modal);

        ArrayList<String> all_ctp_deadtime_cfgs = getAllTCTPDeadtimeConfigObjects();
        if(all_ctp_deadtime_cfgs.contains("Deadtime_Physics")) {
            m_selectionChoices.put("Deadtime Physics", "Deadtime_Physics");            
        }
        if(all_ctp_deadtime_cfgs.contains("Deadtime_Cosmics")) {
            m_selectionChoices.put("Deadtime Cosmics", "Deadtime_Cosmics");
        }
        for(String cfg_uid: all_ctp_deadtime_cfgs) {
            if(cfg_uid.equals("Deadtime_Physics") || cfg_uid.equals("Deadtime_Cosmics")) {
                continue;
            }
            String display_uid = cfg_uid.replace('_', ' ');
            m_selectionChoices.put(display_uid, cfg_uid);
        }

        initComponents();

        setTitle("CTP deadtime settings selection");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void initComponents() {

        // set some properties of the labels and buttons
        final JLabel lb_title = new JLabel("Select deadtime configuration");
        lb_title.setFont(new Font("Serif", Font.BOLD, 16));
        lb_title.setForeground(new Color(0, 0, 153));

        final JLabel labelInstruction = new javax.swing.JLabel("For any other configurations, please contact CTP on-call.");
        labelInstruction.setFont(new Font("Tahoma", Font.ITALIC, 13));

        btnApply.setEnabled(false);
        btnApply.setPreferredSize(new Dimension(85, 24));
        btnCancel.setPreferredSize(new Dimension(85, 24));
        btnApply.addActionListener((evt) -> {
            applyActionPerformed(evt);
        });
        btnCancel.addActionListener((evt) -> {
            cancelButtonActionPerformed(evt);
        });

        /**** arrange the components *****/
        // main layout is a vertical flow
        Box box_main = new Box(BoxLayout.Y_AXIS);
        box_main.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        /**** the title panel *****/
        Box box_title = new Box(BoxLayout.LINE_AXIS);
        box_main.add(box_title);
        box_title.setAlignmentX(LEFT_ALIGNMENT);
        box_title.add(lb_title);

        /**** the selection panel *****/
        Box box_selection = new Box(BoxLayout.PAGE_AXIS);
        box_main.add(box_selection, LEFT_ALIGNMENT);
        box_selection.setAlignmentX(LEFT_ALIGNMENT);
        box_selection.setBorder(BorderFactory.createEmptyBorder(5, 10, 20, 10));
        // group the radio buttons
        ButtonGroup group = new ButtonGroup();
        for (String key : m_selectionChoices.keySet()) {
            JRadioButton btn = new JRadioButton(key);
            btn.setAlignmentX(LEFT_ALIGNMENT);
            btn.addActionListener(rbActionListener);    
            group.add(btn);
            radioButtons.add(btn);
            box_selection.add(Box.createVerticalStrut(10));
            box_selection.add(btn);
        }
        box_selection.add(Box.createVerticalStrut(15));
        box_selection.add(labelInstruction);

        /**** the button panel *****/
        final Box box_buttons = new Box(BoxLayout.LINE_AXIS);
        box_buttons.setAlignmentX(LEFT_ALIGNMENT);
        box_buttons.add(Box.createHorizontalGlue());
        box_buttons.add(btnCancel);
        box_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        box_buttons.add(btnApply);
        box_main.add(box_buttons, BorderLayout.SOUTH);

        setContentPane(box_main);

        pack();

        Dimension minsize = getMinimumSize();
        minsize.width *= 1.1;
        setMinimumSize(minsize);
    }

    protected ArrayList<String> getAllTCTPDeadtimeConfigObjects() {
        ArrayList<String> cfg_names = new ArrayList<>();
        cfg_names.add("Test 2");
        cfg_names.add("Deadtime_Cosmics");
        cfg_names.add("Deadtime_Physics");
        cfg_names.add("Test_1");
        cfg_names.add("Test_3_a");
        return cfg_names;
    }

    protected void applyActionPerformed(ActionEvent evt) {
        System.out.println("Selected " + getSelection());
        dispose();
    }

    protected void cancelButtonActionPerformed(ActionEvent evt) {
        m_selection = "";
        dispose();
    }

    private ArrayList<JRadioButton> radioButtons = new  ArrayList<JRadioButton>();
    protected JButton btnCancel = new javax.swing.JButton("Cancel");
    protected JButton btnApply = new javax.swing.JButton("Select");

    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(CTPDeadtimeDialog.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(() -> {
            CTPDeadtimeDialog dialog = new CTPDeadtimeDialog(new JFrame(), true);
            dialog.setVisible(true);
            System.exit(0);
        });
    }

}
