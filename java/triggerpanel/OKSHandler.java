package triggerpanel;

import Igui.IguiLogger;
import java.util.ArrayList;


class OKSHandler {

    private SMKVariable          SMKVar = null;
    private L1PSKVariable        L1PSKVar = null;
    private HLTPSKVariable       HLTPSKVar = null;
    private BGSKVariable         BGSKVar = null;
    private DBConnectionVariable DBConnVar = null;
    private CTPDeadtimeVariable  CTPDTVar = null;
    private LBLengthVariable     lbLengthVar = null;
    private final ArrayList<OKSVariable<?> > varList;
    private final ArrayList<TPCallable> toCall;
    
    void addVar(OKSVariable<?> v) {
        varList.add(v);
    }
    
    void bind(TPCallable c) {
        toCall.add(c);
    }
    
    public OKSHandler() {
        this.toCall = new ArrayList<>();
        this.varList = new ArrayList<>();
    }
    
    void createVariables() {
        SMKVar    = new SMKVariable();
        L1PSKVar  = new L1PSKVariable();
        HLTPSKVar = new HLTPSKVariable();
        BGSKVar   = new BGSKVariable();
        DBConnVar = new DBConnectionVariable();
        CTPDTVar  = new CTPDeadtimeVariable();
        lbLengthVar = new LBLengthVariable();
    }

    public void readOKS() {
        for(TPCallable c : toCall) {
            c.call();
        }
        IguiLogger.info("Read OKS: " + this);
    } 

    SMKVariable          getSMKVar()    { return SMKVar; }
    L1PSKVariable        getL1PSKVar()  { return L1PSKVar; }
    HLTPSKVariable       getHLTPSKVar() { return HLTPSKVar; }
    BGSKVariable         getBGSKVar()   { return BGSKVar; }
    DBConnectionVariable getDBConnVar() { return DBConnVar; }
    CTPDeadtimeVariable  getCTPDeadtimeVar() { return CTPDTVar; }
    LBLengthVariable     getLBLengthVar() { return lbLengthVar; }
    
    public void print() {
        System.out.println(toString());
    }

    @Override
    public String toString() {
        StringBuilder msg = new StringBuilder();
        for(OKSVariable<?> var: varList) {
            msg.append("\n   ").append(var.toString());
        }
        return msg.toString();
    } 

    public boolean uncommitedDbChanges() {
        boolean allCommitted = true;
        for(OKSVariable<?> var: varList) {
            if(! var.isCommitted() ) { allCommitted = false; break; }
        }
        return ! allCommitted;
    }

    public void abortRdbChanges() {
        for(OKSVariable<?> var: varList) {
            var.abortRdbChanges();
        }
    }

}
