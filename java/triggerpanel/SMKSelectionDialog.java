/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;

/**
 *
 * @author stelzer
 */
class SMKSelectionDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    static final int CANCEL = 0;
    static final int SELECT = 1;

    public SMKSelectionDialog(JFrame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        setTitle("Trigger menu selection");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    final public int button_pressed() {
        return _button_pressed;
    }

    final public boolean is_selected() {
        return _button_pressed == SELECT;
    }

    final public boolean is_cancelled() {
        return _button_pressed == CANCEL;
    }

    /****** non-public methods *******/
    private void initComponents() {

        /**** define the appearance of the components *****/
        // title line
        JLabel lb_title = new JLabel("Select new trigger menu (SMK)");
        lb_title.setFont(new Font("Serif", Font.BOLD, 16));
        lb_title.setForeground(new Color(0, 0, 153));
        lb_title.setHorizontalAlignment(SwingConstants.LEFT);
        lb_title.setVerticalAlignment(SwingConstants.BOTTOM);
        lb_title.setAlignmentX(0.5F);

        // selection line
        JLabel lb_txt = new JLabel("SMK");
        lb_txt.setFont(new Font("Dialog", Font.PLAIN, 14));
        lb_current_smk.setFont(new Font("Dialog", Font.BOLD, 14));

        // bind actions
        cob_smk.addActionListener((e) -> smkSelectionChanged(e));
        chb_showHidden.addActionListener((evt) -> showHiddenAction(evt));
        btn_cancel.addActionListener((evt) -> cancelButtonAction(evt));
        btn_select.addActionListener((evt) -> selectButtonAction(evt));

        /**** arrange the components *****/
        // main layout is a vertical flow
        JPanel panel_main = new JPanel();
        panel_main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_main.setLayout(new BoxLayout(panel_main, BoxLayout.PAGE_AXIS));

        /**** the title panel *****/
        JPanel panel_title = new JPanel();
        panel_main.add(panel_title);
        panel_title.setLayout(new BoxLayout(panel_title, BoxLayout.LINE_AXIS));
        panel_title.add(lb_title);
        panel_title.add(Box.createHorizontalGlue());
        panel_title.add(chb_showHidden);

        /*****  the panel with the selection drop down menus  ******/
        JPanel panel_selection = new JPanel();        
        panel_main.add(panel_selection);
        panel_selection.setBorder(BorderFactory.createEmptyBorder(15, 15, 5,5));
        panel_selection.setLayout(new BoxLayout(panel_selection, BoxLayout.LINE_AXIS));
        panel_selection.add(lb_txt);
        panel_selection.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_selection.add(lb_current_smk);
        panel_selection.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_selection.add(cob_smk);

        /******* panel with the buttons *******/
        JPanel panel_buttons = new JPanel();
        panel_main.add(panel_buttons);
        panel_buttons.setLayout(new BoxLayout(panel_buttons, BoxLayout.LINE_AXIS));
        panel_buttons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_buttons.add(Box.createHorizontalGlue());
        panel_buttons.add(btn_cancel);
        panel_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_buttons.add(btn_select);

        // make the main panel the content of the dialog
        setContentPane(panel_main);

        pack();

        // prevent combobox from stretching vertically
        Dimension maxdim = cob_smk.getMaximumSize();
        maxdim.height = cob_smk.getSize().height;
        cob_smk.setMaximumSize(maxdim);

        // set min and max dimensions, allowing extension only in width
        int currentHeight = getSize().height;
        setMinimumSize(new Dimension(650, currentHeight));
        setMaximumSize(new Dimension(getMaximumSize().width, currentHeight));

    }

    // actions must be overriden in implementation
    protected void smkSelectionChanged(ActionEvent evt) {}
    protected void showHiddenAction(ActionEvent evt) {}
    protected void cancelButtonAction(ActionEvent evt) {
        _button_pressed = CANCEL;
        dispose();
    }
    protected void selectButtonAction(ActionEvent evt) {
        _button_pressed = SELECT;
        dispose();
    }

    private int _button_pressed = 0;
    
    // panel objects that need to be accessible in the implementation
    protected JLabel lb_current_smk = new JLabel("1000");
    protected JComboBox<KeyDescriptor> cob_smk = new JComboBox<KeyDescriptor>();
    protected JButton btn_cancel = new JButton("Cancel");
    protected JButton btn_select = new JButton("Select");
    protected JCheckBox chb_showHidden = new JCheckBox("show hidden keys");
    
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("GTK+".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SMKSelectionDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(() -> {
                SMKSelectionDialog dialog = new SMKSelectionDialog(new JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        System.exit(0);
                    }
                    
                });
                dialog.pack();
                dialog.cob_smk.addItem(new KeyDescriptor(1,"Physics menu",1,"The physics menu",false));
                dialog.cob_smk.addItem(new KeyDescriptor(2,"Cosmics menu",1,"The cosmics menu",false));
                dialog.cob_smk.addItem(new KeyDescriptor(3,"ALFA menu",1,"The ALFA menu",false));
                dialog.cob_smk.addItem(new KeyDescriptor(4,"VdM menu",1,"The VdM menu",true));
                dialog.cob_smk.addItem(new KeyDescriptor(5,"VdM menu",2,"The updated VdM menu",false));
                dialog.setVisible(true);
                System.out.println("SMK selection dialog closed with " + dialog.button_pressed());
            });
    }

}
