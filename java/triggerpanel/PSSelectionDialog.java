package triggerpanel;

import java.awt.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

class PSSelectionDialog extends JDialog {

    /*** public methods */
    void notifyOfCurrentState(RunState currentState) {
        stateChangeAction(currentState);
    }

    boolean isCurrentState() {
        return currentState==panelState;
    }

    boolean isBGSSelectionActive() {
        return dropdown_BGS.isVisible();
    }

    /** PSSelectionDialogNew constructor */
    public PSSelectionDialog(RunState state, RunState currentState, Frame parent, boolean modal) {
        super(parent, modal);
        this.panelState = state;

        initComponents();

        setTitle("Prescale keys selection for " + state);
        title.setText("Select " + state + " prescale keys");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // only now that the panel size is determined hide what is not wanted initially or at all
        check_showBGSelection.setSelected(false);
        if(state != RunState.STANDBY) {
            check_showBGSelection.setVisible(modal);
        }
    }

    /*** private methods and data */
    final RunState panelState;
    RunState currentState;

    private void stateChangeAction(final RunState currentState) {
        this.currentState = currentState;

        System.out.println("Am in EventHandling thread: " + javax.swing.SwingUtilities.isEventDispatchThread());
        System.out.println("Panel is for " + panelState + ", currentState is " + currentState);
        check_showBGSelection.setVisible(isCurrentState());
        selectButton.setText(isCurrentState()? "Apply" : "Select");
        if(isCurrentState()) {
            title.setText("Select and Apply " + panelState + " prescale keys");
        } else {
            title.setText("Select " + panelState + " prescale keys");
            check_showBGSelection.setSelected(false);
        }
    }

    /****** build the dialog GUI *****/
    private void initComponents() {

        // title line
        title.setFont(new Font("Serif", Font.BOLD, 16));
        title.setForeground(new Color(0, 0, 153));
        title.setHorizontalAlignment(SwingConstants.LEFT);
        title.setVerticalAlignment(SwingConstants.BOTTOM);
        title.setAlignmentX(0.5F);

        // selection text
        JLabel StandbyL1Lbl = new JLabel("L1 Psk:");
        JLabel StandbyHLTLbl = new JLabel("HLT Psk:");
        Font font_plain = new Font("Dialog", Font.PLAIN, 14);
        Font font_bold = new Font("Dialog", Font.BOLD, 14);
        StandbyL1Lbl.setFont(font_plain);
        StandbyHLTLbl.setFont(font_plain);
        lb_bgsk.setFont(font_plain);
        curL1Label.setFont(font_bold);
        curL1Label.setHorizontalAlignment(SwingConstants.RIGHT);
        curHLTLabel.setFont(font_bold);
        curHLTLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        lb_current_bgsk.setFont(font_bold);
        lb_current_bgsk.setHorizontalAlignment(SwingConstants.RIGHT);
        
        // drop down menus
        dropdown_L1.addItemListener((evt) -> {
            selectionL1KeyChanged(evt);
        });
        dropdown_HLT.addItemListener((evt) -> {
            selectionHLTKeyChanged(evt);
        });
        dropdown_BGS.addItemListener((evt) -> {
            selectionBGSKeyChanged(evt);
        });
        
        // buttons
        selectButton.setEnabled(false);
        cancelButton.setPreferredSize(new Dimension(90, 24));
        selectButton.setPreferredSize(new Dimension(90, 24));
        selectButton.addActionListener((evt) -> {
            actionSelectButton(evt);
        });
        
        cancelButton.addActionListener((evt) -> {
            actionCancelButton(evt);
        });
        
        // show hidden checkbox
        showHidden.addActionListener((evt) -> {
            actionShowHidden(evt);
        });

        check_showBGSelection.addItemListener((evt) -> { 
            actionShowBGSSelection(evt);           
        });

        /**** arrange the components *****/
        JPanel panel_main = new JPanel();
        panel_main.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_main.setLayout(new BorderLayout());

        /**** the title panel *****/
        JPanel panel_title = new JPanel();
        panel_main.add(panel_title, BorderLayout.NORTH);
        panel_title.setLayout(new BoxLayout(panel_title, BoxLayout.LINE_AXIS));
        panel_title.add(title);
        panel_title.add(Box.createHorizontalGlue());
        panel_title.add(showHidden);

        /*****  the panel with the selection drop down menus  ******/
        JPanel selectionPanel = new JPanel();        
        panel_main.add(selectionPanel, BorderLayout.CENTER);
        selectionPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 5,5));
        selectionPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        // first line
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
        gbc.ipadx = 10;
        gbc.gridx = 0; gbc.weightx = 0;
        selectionPanel.add(StandbyL1Lbl, gbc);
        gbc.gridx = 1; gbc.weightx = 0;
        selectionPanel.add(curL1Label, gbc);
        gbc.gridx = 2; gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(6, 10, 6, 20);
        selectionPanel.add(dropdown_L1, gbc);
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 0, 0, 0);
        
        // second line
        gbc.gridy = 1;
        gbc.gridx = 0; gbc.weightx = 0;
        selectionPanel.add(StandbyHLTLbl, gbc);
        gbc.gridx = 1; gbc.weightx = 0;
        selectionPanel.add(curHLTLabel, gbc);
        gbc.gridx = 2; gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(6, 10, 6, 20);
        selectionPanel.add(dropdown_HLT, gbc);
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 0, 0, 0);

        // third line
        gbc.gridy = 2;
        gbc.gridx = 0; gbc.weightx = 0;
        selectionPanel.add(lb_bgsk, gbc);
        gbc.gridx = 1; gbc.weightx = 0;
        selectionPanel.add(lb_current_bgsk, gbc);
        gbc.gridx = 2; gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(6, 10, 6, 20);
        selectionPanel.add(dropdown_BGS, gbc);
        
        
        /******* panel with the buttons *******/
        JPanel panel_buttons = new JPanel();
        panel_buttons.setLayout(new BoxLayout(panel_buttons, BoxLayout.LINE_AXIS));
        panel_buttons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_buttons.add(check_showBGSelection);
        panel_buttons.add(Box.createHorizontalGlue());
        panel_buttons.add(cancelButton);
        panel_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_buttons.add(selectButton);
        panel_main.add(panel_buttons, BorderLayout.SOUTH);
        
        // make the main panel the content of the dialog
        setContentPane(panel_main);
        
        pack();
        
        // set min and max dimensions, allowing extension only in width
        int currentHeight = getSize().height;
        setMinimumSize(new Dimension(650, currentHeight));
        setMaximumSize(new Dimension(getMaximumSize().width, currentHeight));
        
    }

    protected void actionSelectButton(ActionEvent evt) {
        dispose();
    }

    protected void actionCancelButton(ActionEvent evt) {
        dispose();
    }
    
    protected void selectionL1KeyChanged(ItemEvent evt)
    {}

    protected void selectionHLTKeyChanged(ItemEvent evt)
    {}
    
    protected void selectionBGSKeyChanged(ItemEvent evt)
    {}

    protected void actionShowBGSSelection(ItemEvent evt) {
        showBGSSelection(evt.getStateChange() == ItemEvent.SELECTED);
    }

    protected void showBGSSelection(boolean doShow) {
        lb_bgsk.setVisible(doShow);
        lb_current_bgsk.setVisible(doShow);
        dropdown_BGS.setVisible(doShow);
    }

    protected void actionShowHidden(ActionEvent evt)
    {};

    // Variables declaration
    protected final JLabel title = new JLabel("Select MODE prescale keys");
    protected final JLabel curL1Label = new JLabel("1000");
    protected final JLabel curHLTLabel = new JLabel("1000");
    protected final JButton cancelButton = new JButton("Cancel");
    protected final JButton selectButton = new JButton("Apply");
    protected final JCheckBox showHidden = new JCheckBox("show hidden", false);
    protected final JComboBox<KeyDescriptor> dropdown_HLT = new JComboBox<KeyDescriptor>();
    protected final JComboBox<KeyDescriptor> dropdown_L1 = new JComboBox<KeyDescriptor>();
    protected final JComboBox<KeyDescriptor> dropdown_BGS = new JComboBox<KeyDescriptor>();
    private final JLabel lb_bgsk = new JLabel("Bgsk");
    protected final JLabel lb_current_bgsk = new JLabel("1000");
    private final JCheckBox check_showBGSelection = new JCheckBox("Simultaneously select bunch group set", true);

    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PSSelectionDialog.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        EventQueue.invokeLater(() -> {
            RunState state = RunState.PHYSICS;
            RunState currentState = RunState.PHYSICS;

            PSSelectionDialog dialog = new PSSelectionDialog(state, currentState, new JFrame(), false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.dropdown_L1.addItem(new KeyDescriptor(1, "Cosmics prescales",1, "Cosmics prescales with RPC, TRT and Calo", false));
            dialog.setVisible(true);
        });
    }

}
