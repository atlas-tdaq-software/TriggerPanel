package triggerpanel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

public class BGSelectionDialog extends JDialog {
    
    /** Creates new form BGSelectionDialog */
    public BGSelectionDialog(Frame parent, boolean modal) {
        super(parent, modal);
        // set up the panel
        initComponents();
        
        setTitle("Bunchgroup set selection");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void initComponents() {

        // set some properties of the labels and buttons
        final JLabel lb_title = new JLabel("Select L1 Bunch Group Set:");
        lb_title.setFont(new Font("Serif", Font.BOLD, 16));
        lb_title.setForeground(new Color(0, 0, 153));
        
        final JLabel lb_bgsk = new JLabel("BGSK");
        lb_bgsk.setFont(new Font("Dialog", Font.PLAIN, 14));
        
        lb_current_bgk.setFont(new Font("Dialog", Font.BOLD, 14));

        // show hidden checkbox
        showHidden.addActionListener((evt) -> {
            actionShowHidden(evt);
        });
        
        
        Dimension btn_size = btn_cancel.getPreferredSize();
        btn_apply.setPreferredSize(btn_size);
        btn_apply.setMinimumSize(btn_size);
        btn_apply.setEnabled(false);
        
        // create and fill the panel
        Box panel_master = Box.createVerticalBox();
        panel_master.setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
        
        /**** the title panel *****/
        JPanel panel_title = new JPanel();
        panel_master.add(panel_title, BorderLayout.NORTH);
        panel_title.setLayout(new BoxLayout(panel_title, BoxLayout.LINE_AXIS));
        panel_title.add(lb_title);
        panel_title.add(Box.createHorizontalGlue());
        panel_title.add(showHidden);
        
        JPanel panel_main = new JPanel();
        panel_master.add(panel_main);

        // use the GridBagLayout to arrange the components
        panel_main.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // start second line
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
        gbc.gridx = 0;
        gbc.weightx = 0;
        gbc.ipadx = 10;
        panel_main.add(lb_bgsk, gbc);
        gbc.gridx = 1;
        gbc.weightx = 0;
        panel_main.add(lb_current_bgk, gbc);
        gbc.gridx = 2; 
        gbc.weightx = 1;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(20, 10, 20, 0);
        panel_main.add(comboL1BunchGroupKey, gbc);
        gbc.gridwidth = 1;
        gbc.fill = GridBagConstraints.NONE;
        gbc.insets = new Insets(0, 0, 0, 0);
        gbc.ipadx = 0;

        // start third line
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.ABOVE_BASELINE_LEADING;
        gbc.gridx = 0; gbc.weightx=0; gbc.gridwidth = 3;
        final JLabel lb_hint = new JLabel("To change BGK and PSK simultaneously use PSK selection dialog");
        lb_hint.setFont(new Font("Serif", Font.ITALIC, 12));
        panel_main.add(lb_hint, gbc);
        gbc.gridwidth = 1;
        gbc.anchor = GridBagConstraints.EAST;
        gbc.gridx = 3; gbc.weightx=0.1;  
        gbc.insets = new Insets(0, 10, 0, 0);
        panel_main.add(btn_cancel, gbc);
        gbc.gridx = 4; gbc.weightx=0;
        panel_main.add(btn_apply, gbc);
        
        setContentPane(panel_master);
        pack();

        Dimension minsize = getMinimumSize();
        minsize.width += 10;
        setMinimumSize(minsize);
    }

    protected void actionShowHidden(ActionEvent evt) {
    }

    protected final JComboBox<KeyDescriptor> comboL1BunchGroupKey = new JComboBox<KeyDescriptor>();
    protected final JLabel lb_current_bgk = new JLabel("1000", SwingConstants.RIGHT);
    protected final JCheckBox showHidden = new JCheckBox("show hidden", false);
    protected final JButton btn_apply = new JButton("Apply");
    protected final JButton btn_cancel = new JButton("Cancel");

    public static void main(String[] args) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(BGSelectionDialog.class.getName()).log(Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        EventQueue.invokeLater(() -> {
            BGSelectionDialog dialog = new BGSelectionDialog(null, true);
            dialog.comboL1BunchGroupKey.addItem(new KeyDescriptor(13, "Bunchgroup for vdM scan", 1, "created on Apr 17 2023", false));
            dialog.comboL1BunchGroupKey.addItem(new KeyDescriptor(15, "Bunchgroup for vdM scan", 2, "created on May 21 2023", true));
            dialog.setVisible(true);     
        });
    }
}
