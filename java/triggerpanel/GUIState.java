package triggerpanel;

import Igui.IguiConstants;
import Igui.IguiConstants.AccessControlStatus;
import Igui.RunControlFSM.State;

final class GUIState {
    // This class should only be used in the EventDispatchThread

    private GUIState() {}
    
    static State               currentState = State.ABSENT;

    /****
     * Access control
     * 
     * is either CONTROL or DISPLAY
     */
    static AccessControlStatus currentAccess = AccessControlStatus.DISPLAY;
    static boolean isControl() {
        return currentAccess==IguiConstants.AccessControlStatus.CONTROL;
    }

    /******
     * Panel state
     * 
     * Possible values are RunState.STANDBY, RunState.PHYSICS, and RunState.EMITTANCE
     * 
     * The value is determined from the emittance scan flag and the ready4physics flag
     */
    static private Boolean emittanceState = false;
    public static Boolean isInStateEmittance() {
        return emittanceState;
    }
    public static void setEmittanceState(Boolean emittanceState) {
        GUIState.emittanceState = emittanceState;
    }

    private static RunState standbyOrPhysics = RunState.STANDBY;
    static void setStandbyOrPhysics(RunState state) {
        standbyOrPhysics = state;
    }
    static RunState getStandbyOrPhysics() {
        return standbyOrPhysics;
    }
    /*****
     * Returns the current state of datat taking based on the emittance flag and th readyforPhysics flag
     * @return RunState.STANDBY, RunState.PHYSICS, or RunState.EMITTANCE
     */
    static RunState getCurrentRunState() {
        final RunState currentState;
        if(GUIState.isInStateEmittance()) {
            currentState = RunState.EMITTANCE;
        } else {
            currentState = GUIState.getStandbyOrPhysics();
        }
        return currentState;
    }


    // LB interval in seconds
    public static int getLBinterval() {
        return lbinterval;
    }
    public static void setLBinterval(int lbinterval) {
        GUIState.lbinterval = lbinterval;
    }
    static private int lbinterval = 0;

    // prescale keys for standby and physics
    public static int getStandbyhltpsk() {
        return standbyhltpsk;
    }
    public static void setStandbyhltpsk(int standbyhltpsk) {
        GUIState.standbyhltpsk = standbyhltpsk;
    }
    public static int getPhysicshltpsk() {
        return physicshltpsk;
    }
    public static void setPhysicshltpsk(int physicshltpsk) {
        GUIState.physicshltpsk = physicshltpsk;
    }
    public static int getEmittancehltpsk() {
        return emittancehltpsk;
    }
    public static void setEmittancehltpsk(int emittancehltpsk) {
        GUIState.emittancehltpsk = emittancehltpsk;
    }

    public static int getStandbyl1psk() {
        return standbyl1psk;
    }
    public static void setStandbyl1psk(int standbyl1psk) {
        GUIState.standbyl1psk = standbyl1psk;
    }
    public static int getPhysicsl1psk() {
        return physicsl1psk;
    }
    public static void setPhysicsl1psk(int physicsl1psk) {
        GUIState.physicsl1psk = physicsl1psk;
    }
    public static int getEmittancel1psk() {
        return emittancel1psk;
    }
    public static void setEmittancel1psk(int emittancel1psk) {
        GUIState.emittancel1psk = emittancel1psk;
    }
    
    static private int    standbyl1psk = 0;
    static private int    standbyhltpsk = 0;
    static private int    physicsl1psk = 0;
    static private int    physicshltpsk = 0;
    static private int    emittancel1psk = 0;
    static private int    emittancehltpsk = 0;


    // run and LB number
    static int            runNumber = 0;
    static int            lbNumber = 0;

}
