/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import Igui.IguiLogger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 *
 * @author stelzer
 */
class PrescaleSetAliasHandle {

    final class LumiRangeWithPrescaleSet {
        final int l1psk;
        final int hltpsk;
        final float lumiMin;
        final float lumiMax;
        final String comment;

        public LumiRangeWithPrescaleSet(int l1psk, int hltpsk, float lumiMin, float lumiMax, String comment) {
            this.l1psk = l1psk;
            this.hltpsk = hltpsk;
            this.lumiMin = lumiMin;
            this.lumiMax = lumiMax;
            this.comment = comment;
        }
        
        public float getLowerLumi(){
            return lumiMin;
        }
        
        @Override
        public String toString() {
            return "Lumi " + lumiMin + " - " + this.lumiMax + "  with l1 psk " + this.l1psk + "  and hlt psk " + this.hltpsk + " and comment " + this.comment;
        }
    }
    
    final class AliasComponent {
        private final String type;
        private final String name;
        private final List<LumiRangeWithPrescaleSet> lumis = new ArrayList<>();

        public AliasComponent(String type, String name) {
            this.type = type;
            this.name = name;
        }

        void addLumiRange(LumiRangeWithPrescaleSet lumiRange) {
            this.lumis.add(lumiRange);
        }
        
        public List<LumiRangeWithPrescaleSet> getLumis() {
            return lumis;
        }
        
        String getType() {
            return this.type;
        }

        String getName() {
            return this.name;
        }

        int getSize() {
            return this.lumis.size();
        }

        void sortAndPack() {
            
            Set<Float> lumiBoundsSet = new TreeSet<Float>();
            lumis.stream().forEach(lr -> {
                lumiBoundsSet.add(lr.lumiMin);
                lumiBoundsSet.add(lr.lumiMax);
            });
            List<Float> lumiBounds = lumiBoundsSet.stream().sorted().collect(Collectors.toList());
            if(lumiBounds.size()==1) { // the case of single value, e.g. for STANDBY 
                lumiBounds.add(Float.POSITIVE_INFINITY);
            }
            List<Integer> l1psk = new ArrayList<>(lumiBounds.size()-1);
            List<Integer> hltpsk = new ArrayList<>(lumiBounds.size()-1);
            List<String> commentsL1 = new ArrayList<>(lumiBounds.size() - 1);
            List<String> commentsHLT = new ArrayList<>(lumiBounds.size() - 1);

            for(int i = 0; i<lumiBounds.size()-1; i++) {
                l1psk.add(0);
                hltpsk.add(0);
                commentsL1.add("");
                commentsHLT.add("");
            }
            
            try {
                //System.out.println("new set " + l1psk.size());
                for(int i=0; i<l1psk.size(); i++ ) {
                    final int lumiRangeIndex = i;
                    Float lumiRangeLowerBound = lumiBounds.get(lumiRangeIndex);
                    //System.out.println("LUMIS " + lumis.size() + " lower bound " + lumiRangeLowerBound);
                    lumis.stream().forEach((LumiRangeWithPrescaleSet lr) -> {
                        //System.out.println(lr.toString());
                        if( ( lr.lumiMin <= lumiRangeLowerBound && lr.lumiMax > lumiRangeLowerBound) ||
                            ( lr.lumiMin == lr.lumiMax && lr.lumiMin == lumiRangeLowerBound ) )
                        {
                            if(lr.l1psk!=0) { 
                                //System.out.println("L1 save " + lumiRangeIndex + " " + lr.l1psk);
                                l1psk.set(lumiRangeIndex, lr.l1psk);
                                commentsL1.set(lumiRangeIndex, lr.comment);

                            }
                            if(lr.hltpsk!=0) { 
                                //System.out.println("HLT save " + lumiRangeIndex + " " + lr.hltpsk);
                                hltpsk.set(lumiRangeIndex, lr.hltpsk);
                                commentsHLT.set(lumiRangeIndex, lr.comment);
                            }
                        }
                    });
                }
                //System.out.println("new set done");
            }
            catch(IndexOutOfBoundsException ex) {
                IguiLogger.error("Caught IndexOutOfBoundsException " + ex.toString());
            }
            //System.out.println("SIZES " + commentsL1.size() + " " + commentsHLT.size());
            lumis.clear();
            for(int i=0; i<l1psk.size(); i++ ) {
                //System.out.println("OUTPUT " + l1psk.get(i) + " " + hltpsk.get(i) + " " + lumiBounds.get(i) + " " + lumiBounds.get(i+1));
                String commentSum = "";
                if(commentsL1.get(i).equals(commentsHLT.get(i))){
                    commentSum = commentsL1.get(i);
                }
                else{
                    commentSum = "L1: " + commentsL1.get(i) + ", HLT: " + commentsHLT.get(i);
                }
                lumis.add(new LumiRangeWithPrescaleSet(l1psk.get(i), hltpsk.get(i), lumiBounds.get(i), lumiBounds.get(i+1), commentSum));
            }
            lumis.sort(Comparator.comparingDouble(LumiRangeWithPrescaleSet::getLowerLumi).reversed());
        }
        
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder(1000);
            sb.append("Prescale alias set " + getName() + " [Type " + getType() + "] has " + getSize() + " lumi range(s):\n");
            getLumis().stream().forEach((lumi)-> { sb.append("        ").append(lumi.toString()).append("\n"); } );
            return sb.toString();
        }
        
    }
    
    private int smk = 0;
    final private List<AliasComponent> aliasComponents = new ArrayList<>();
    
    PrescaleSetAliasHandle() 
    {}

    public int getSmk() {
        return smk;
    }

    public AliasComponent getAliasComponent(final String type, final String name) {
        for(AliasComponent c : aliasComponents) {
            if(c.type.equals(type) && c.name.equals(name)) {
                return c;
            }
        }
        return null;
    }
    
    public List<AliasComponent> getAliasComponents() {
        return aliasComponents;
    }
    
    int size() {
        return aliasComponents.size();
    }
    
    void clear() {
        this.smk = 0;
        this.aliasComponents.clear();
    };

    void setSmk(int smk) {
        if(this.smk == smk) {
            return;
        }
        clear();
        this.smk = smk;
    }

    void addAliasComponent(AliasComponent component) {
        this.aliasComponents.add(component);
    }

    void addLumiRange(final String type, final String name, int level, int psk, float lumiMin, float lumiMax, String comment) {
        AliasComponent comp = getAliasComponent(type, name);
        if( comp == null ) {
            comp = new AliasComponent(type, name);
            aliasComponents.add(comp);
        }
        //System.out.println("Number of components " + aliasComponents.size());
        //System.out.println("num lumis in comp " + comp.lumis.size());
        // some logic needed here
        int l1psk = 0; int hltpsk = 0;
        if(level==1) {
            l1psk = psk;
        } else {
            hltpsk = psk;
        }
        comp.addLumiRange(new LumiRangeWithPrescaleSet(l1psk, hltpsk, lumiMin, lumiMax, comment));
    }

    void sortAndPack() {
        aliasComponents.stream().forEach(AliasComponent::sortAndPack);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(1000);
        sb.append("Alias Handle for SMK " + smk + "\n");
        aliasComponents.stream().forEach((acomp) -> {
            sb.append("  ").append(acomp.toString());
        });
        return sb.toString();
    }
    

}
