/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import Igui.Igui;
import Igui.IguiConstants;
import Igui.IguiConstants.MessageSeverity;
import Igui.IguiException;
import Igui.IguiLogger;
import Igui.IguiPanel;
import Igui.RunControlFSM;
import TRIGGER.CommandExecutionFailed;
import TRIGGER.TriggerCommander;
import TTCInfo.TrigConfL1PsKeyNamed;
import config.NotAllowedException;
import config.SystemException;
import dal.MasterTrigger;
import dal.RunControlApplicationBase;
import is.InfoNotCompatibleException;
import is.RepositoryNotFoundException;
import java.awt.BorderLayout;
import java.sql.SQLException;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import static triggerpanel.TPGlobal.OKS;

/**
 *
 * @author stelzer
 */
public class TriggerPanel extends IguiPanel {

    final TriggerPanelGUIImpl tpgui;

    // the interface to send commands to the CTP
    TriggerCommander triggerCommander;


    // constructor
    public TriggerPanel(Igui igui) {

        super(igui);

        TPGlobal.init(igui);
        
        TPGlobal.setRoDb(this.getDb());
        TPGlobal.setRwDb(this.getRwDb());        

        OKS.createVariables();

        triggerCommander = getMasterTrigger();

        setLayout(new BorderLayout());
        tpgui = new TriggerPanelGUIImpl(this);
        add(tpgui); // add tpgui component to the TP
    }

    @Override
    public void panelInit(RunControlFSM.State state, IguiConstants.AccessControlStatus acs) throws IguiException {
        GUIState.currentState = state;
        GUIState.currentAccess = acs;
        TPGlobal.setRwDb(this.getRwDb());

        try {
            // read from oks (also updates menu list and psk lists for current smk)
            read_oks();
        } catch (SQLException ex) {
            Utils.logException(ex);
        }

        // subscribe to IS
        boolean silent = false;
        TPGlobal.IS.subscribe(silent);

        this.tpgui.Initialize();
        
        tpgui.UpdatePanelForStateAndAccessControl();
    }

    public TriggerPanelGUIImpl getTPgui() {
        return tpgui;
    }

    public int getCurrentSMK() {
        return OKS.getSMKVar().value();
    }

    public int getCurrentL1PSK() {
        return RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState())
                ? /*<=INITIAL*/ OKS.getL1PSKVar().value()
                : /*>INITIAL*/ TPGlobal.IS.CurrentL1Psk.getinfo().L1PrescaleKey;
    }

    public String getCurrentL1PSKInfo() {
        if (RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState())) {
            // <= INITIAL
            return "From OKS";
        }
        // > INITIAL
        if (TPGlobal.IS.CurrentL1Psk.isSubscribed()) {
            return TPGlobal.IS.CurrentL1Psk.infoProvider();
        } else {
            return TPGlobal.IS.CurrentL1Psk.stateInfo();
        }
    }

    public int getCurrentHLTPSK() {
        return RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()) ? /*<=INITIAL*/ OKS.getHLTPSKVar().value() : /*>INITIAL*/ TPGlobal.IS.CurrentHLTPsk.getinfo().HltPrescaleKey;
    }

    public String getCurrentHLTPSKInfo() {
        if (RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState())) {
            // <= INITIAL
            return "From OKS";
        }
        // > INITIAL
        if (TPGlobal.IS.CurrentHLTPsk.isSubscribed()) {
            return TPGlobal.IS.CurrentHLTPsk.infoProvider();
        } else {
            return TPGlobal.IS.CurrentHLTPsk.stateInfo();
        }
    }

    // bunch group key and info
    public int getCurrentBG() {
        return RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()) ? /*<=INITIAL*/ OKS.getBGSKVar().value() : /*>INITIAL*/ TPGlobal.IS.CurrentBGSk.getinfo().L1BunchGroupKey;
    }

    public String getCurrentBGInfo() {
        return RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()) ? /*<=INITIAL*/ "From OKS" : /*>INITIAL*/ TPGlobal.IS.CurrentBGSk.infoProvider();
    }

    // bunch group key and info
    public String getCurrentDeadtimeConfig() {
        return OKS.getCTPDeadtimeVar().value();
    }
    
    @Override
    public String getPanelName() {
        return "Trigger";
    }

    @Override
    public String getTabName() {
        return "Trigger";
    }

    @Override
    public void panelTerminated() {
        checkForUncommitRDBChanges("The trigger panel got terminated");
        TPGlobal.IS.unsubscribe();
    }

    @Override
    public boolean rcStateAware() {
        return true;
    }

    /**
     * Function called when access control status changes.
     *
     * @param acs - level of access
     */
    @Override
    public void accessControlChanged(IguiConstants.AccessControlStatus acs) {
        super.accessControlChanged(acs);
        GUIState.currentAccess = acs;
        TPGlobal.setRwDb(this.getRwDb());        
        //this.panel.UpdateStateAccessControl();
        //this.panel.UpdateCommitted(); // when access in control we have access to the rw DB to check if changes have been committed
        tpgui.UpdatePanelForStateAndAccessControl();
    }

    public void checkForUncommitRDBChanges(final String action) {
        if (!OKS.uncommitedDbChanges()) {
            return;
        } // db has not changed

        IguiLogger.info("OKS variables:" + OKS);

        String message = action + " and the following trigger keys were not commited to OKS: ";
        if (!OKS.getSMKVar().isCommitted()) {
            message += "SMK=" + OKS.getSMKVar().value() + " ";
        }
        if (!OKS.getL1PSKVar().isCommitted()) {
            message += "L1PSK=" + OKS.getL1PSKVar().value() + " ";
        }
        if (!OKS.getHLTPSKVar().isCommitted()) {
            message += "HLTPSK=" + OKS.getHLTPSKVar().value() + " ";
        }
        if (!OKS.getBGSKVar().isCommitted()) {
            message += "BGSK=" + OKS.getBGSKVar().value() + " ";
        }

        showDiscardDbDialog(message);
    }

    /**
     * Read relevant info from OKS
     *
     * @throws java.sql.SQLException
     */
    private void read_oks() throws SQLException {

        OKS.readOKS();

        // set connection string
        //TPGlobal.getDbKeysLoader().SetConnectionAlias(OKS.getDBConnVar().value() + "R");
        TPGlobal.theDbKeysLoader().SetConnectionAlias(OKS.getDBConnVar().value());

        // update everything that has been read
        TPGlobal.theDbKeysLoader().UpdateMenus();
        TPGlobal.theDbKeysLoader().UpdatePrescales(OKS.getSMKVar().value());

        // now update labels
        tpgui.UpdateLabelsAfterOKSRead();

    }

    private TriggerCommander getMasterTrigger() {
        String mtControllerName = null;

        try {
            config.Configuration db = getDb();
            dal.Partition dalPartition = dal.Partition_Helper.get(db, TPGlobal.igui.getPartition().getName());

            final MasterTrigger mt = dalPartition.get_MasterTrigger();
            if (mt != null) {
                final RunControlApplicationBase rc = mt.get_Controller();
                if (rc != null) {
                    mtControllerName = rc.UID();
                    IguiLogger.info("The partition Master Trigger is " + mtControllerName);
                } else {
                    IguiLogger.warning("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger");
                }
            } else {
                IguiLogger.warning("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger");
            }
        } catch (final Exception ex) {
            IguiLogger.error("Error looking for the partition Master Trigger: " + ex, ex);
        }

        if (mtControllerName != null) {
            return new TriggerCommander(TPGlobal.igui.getPartition(), mtControllerName);
        } else {
            return null;
        }
    }

    /**
     * Access to the full configuration description
     *
     * @param key key for which the description should be retrieved
     * @param level level for which the key is valid
     * @return the full configuration description
     */
    KeyDescriptor getKeyDescriptor(final int key, final LVL level) {
        if (key == -1) {
            return KeyDescriptor.NotInIS;
        }

        if (key == 0) {
            return KeyDescriptor.NULL; // 0 is an invalid key and used as such throughout the code, -1 means 'not present in IS'
        }

        final Vector<KeyDescriptor> keylist;
        switch(level) {
            case MENU:
                keylist = TPGlobal.theDbKeysLoader().GetMenus();
                break;
            case BGS:
                keylist = TPGlobal.theDbKeysLoader().GetBunchgroups();
                break;
            case L1:
            case HLT:
                keylist = TPGlobal.theDbKeysLoader().GetPrescales(level);
                break;
            default:
                keylist = null;
        }

        if (keylist == null) {
            throw new RuntimeException("No Vector<KeyDescriptor> for level " + level);
        }

        for (KeyDescriptor kd : keylist) {
            if (kd.key == key) {
                return kd;
            }
        }

        IguiLogger.warning("Did not find key " + key + " in " + level + " ( " + keylist.size() + " keys available)");
        return KeyDescriptor.NotValidForSMK;
    }

    /**
     * Access to the full configuration description
     *
     * @param key key for which the description should be retrieved
     * @return the full configuration description
     */
    KeyDescriptor getKeyDescriptor(final LVL level, final RunState runstate) throws SQLException {
        int key = 0;
        switch(level) {
            case L1:
                switch(runstate) {
                    case STANDBY:
                        key = GUIState.getStandbyl1psk();
                        break;
                    case EMITTANCE:
                        key = GUIState.getEmittancel1psk();
                        break;
                    case PHYSICS:
                        key = GUIState.getPhysicsl1psk();
                        break;
                }
                break;
            case HLT:
                switch(runstate) {
                    case STANDBY:
                        key = GUIState.getStandbyhltpsk();
                        break;
                    case EMITTANCE:
                        key = GUIState.getEmittancehltpsk();
                        break;
                    case PHYSICS:
                        key = GUIState.getPhysicshltpsk();
                        break;
                }
                break;
            case BGS:
            case MENU:
                break;
        }

        KeyDescriptor kd = getKeyDescriptor(key, level);
        if (kd == KeyDescriptor.NotValidForSMK) {
            // try again after reloading prescales for current SMK
            TPGlobal.theDbKeysLoader().UpdatePrescales(OKS.getSMKVar().value());
            kd = getKeyDescriptor(key, level);
        }
        return kd;
    }

    void sendPrescalesToMasterTrigger(final KeyDescriptor l1ps, final KeyDescriptor hltps, final KeyDescriptor bgsk)
        throws InfoNotCompatibleException, RepositoryNotFoundException
    {
        boolean sendL1 = l1ps.key != 0 && (l1ps.key != getCurrentL1PSK());
        boolean sendHLT = hltps.key != 0 && (hltps.key != getCurrentHLTPSK());
        boolean sendBgs = bgsk != null && bgsk.key != 0 && (bgsk.key != getCurrentBG());

        String msg1 = "Sending to MasterTrigger\n";
        msg1 += "  L1: new=" + l1ps.key + ", ctp has " + getCurrentL1PSK() + " -> " + (sendL1 ? "yes" : "no" + "\n");
        msg1 += "  HLT: new=" + hltps.key + ", ctp has " + getCurrentHLTPSK() + " -> " + (sendHLT ? "yes" : "no");
        if(bgsk!=null) {
            msg1 += "  BGS: new=" + bgsk.key + ", ctp has " + getCurrentBG() + " -> " + (sendBgs ? "yes" : "no");
        }
        IguiLogger.info(msg1);

        if (!sendL1 && !sendHLT && !sendBgs) {
            return;
        }

        if(sendBgs) {
            // send prescale and bunchgroup set keys to CTP
            if( l1ps.key != 0 && hltps.key != 0 ) {
                // both prescale keys must be proper keys (not 0)
                try {
                    triggerCommander.setPrescales(l1ps.key, hltps.key, bgsk.key);
                } catch (CommandExecutionFailed exc) {
                    final String msg = "Error when sending user command with L1 PSK " + l1ps.key + ", HLT psk " + hltps.key + " and bunchgroupset key " + bgsk.key + " to CTP. \n(" + exc + ")";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    IguiLogger.error(msg);
                }
            }
        } else {
            // send only prescale keys to CTP
            if(sendL1 && sendHLT) {
                // send L1 and HLT prescale keys
                try {
                    final String msg = "Sending L1 psk " + l1ps.key + " and HLT psk " + hltps.key + " to the CTP.";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                    triggerCommander.setPrescales(l1ps.key, hltps.key);
                } catch (CommandExecutionFailed exc) {
                    String msg = "Error when sending L1 psk " + l1ps.key + " and HLT psk " + hltps.key + " to the CTP. (" + exc + ")";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    IguiLogger.error(msg);
                }
            } else if(sendL1) {
                // send only L1 prescale key
                try {
                    String msg = "Sending L1 psk " + l1ps.key + " to the CTP";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                    triggerCommander.setL1Prescales(l1ps.key);
                } catch (CommandExecutionFailed exc) {
                    String msg = "Error when sending L1 psk " + l1ps.key + " to the CTP. (" + exc + ")";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    IguiLogger.error(msg);
                }
            } else {
                // send only HLT prescale key
                try {
                    String msg = "Sending HLT psk " + hltps.key + " to CTP";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION, msg);
                    IguiLogger.info(msg);
                    triggerCommander.setHLTPrescales(hltps.key);
                } catch (CommandExecutionFailed exc) {
                    String msg = "Error when sending HLT psk " + hltps.key + " to the CTP. (" + exc + ")";
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
                    IguiLogger.error(msg);
                }
            }
        }

        if (sendHLT) {
            if (!RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()) && RunControlFSM.State.RUNNING.follows(TPGlobal.igui.getRCState())) { // >= CONFIGURED && < RUNNING
                TrigConfHltPsKeyNamed hltIS = new TrigConfHltPsKeyNamed(TPGlobal.igui.getPartition(), "RunParams.TrigConfHltPsKey");
                hltIS.HltPrescaleKey = hltps.key;
                hltIS.HltPrescaleComment = (hltps.name == null) ? "~" : hltps.name;
                hltIS.checkin();
            }
        }

        // increase lb
        if (TPGlobal.igui.getRCState() == RunControlFSM.State.RUNNING) {
            increaseLB();
        }
    }

    void setBGSK(int bgsk) throws CommandExecutionFailed{
        triggerCommander.setBunchGroup(bgsk);
    }
    
    void increaseLB() {
        int run = getRunnumber();
        if (run > 0) {
            TPGlobal.igui.internalMessage(MessageSeverity.INFORMATION, "Calling increaseLumiBlock of trigger commander for run " + run);
            IguiLogger.info("Calling TriggerMaster increaseLumiBlock with run number " + run);
            try {
                triggerCommander.increaseLumiBlock(run);
            } catch (TRIGGER.CommandExecutionFailed ex) {
                final String errMsg = "Failed to execute LB increase: " + ex;
                IguiLogger.error(errMsg, ex);
                TPGlobal.igui.internalMessage(MessageSeverity.ERROR, errMsg);
            }
        } else {
            IguiLogger.error("Can't increase LB since no run number found");
            TPGlobal.igui.internalMessage(MessageSeverity.ERROR, "Cannot increase LB since no run number found");
        }
    }

    public int getRunnumber() {
        // get the run number from IS
        final int run_number;
        try {
            rc.RunParamsNamed rp = new rc.RunParamsNamed(TPGlobal.igui.getPartition(), "RunParams.RunParams");
            rp.checkout();
            //System.out.print(rp);
            run_number = rp.run_number;
            IguiLogger.info("Got run number " + run_number + " from IS.");
        } catch (is.RepositoryNotFoundException | is.InfoNotFoundException | is.InfoNotCompatibleException e) {
            IguiLogger.error("IS server not found, can not request LB increase ", e);
            return -1;
        }
        return run_number;
    }

    /**
     * Method called after the configuration database is reloaded. This method
     * is executed in the panel own thread.
     */
    @Override
    @SuppressWarnings("CallToThreadDumpStack")
    public void dbReloaded() {
        IguiLogger.info("CALLING" + OKS);
        try {
            panelInit(TPGlobal.igui.getRCState(), TPGlobal.igui.getAccessLevel());
        } catch (IguiException ex) {
            Utils.logException(ex);
        }
    }

    /**
     * Method called after some database changes have been discarded.
     * <p>
     * Subclasses should override this method to synch with the database after
     * some database changes have been discarded.
     * <p>
     * This method will always be executed in the panel own thread.
     */
    @Override
    public void dbDiscarded() {
        try {
            panelInit(TPGlobal.igui.getRCState(), TPGlobal.igui.getAccessLevel());
        } catch (IguiException ex) {
            IguiLogger.error("" + ex);
        }
    }

    /**
     * Method called any time the panel is unselected. A typical usage of this
     * method is to remove subscriptions to services not needed only when the
     * panel is not visible. Such subscriptions may be renewed when the panel is
     * selected again. This method will always be executed in the panel own
     * thread.
     */
    @Override
    public void panelDeselected() {
        checkForUncommitRDBChanges("The trigger panel got deselected");
    }

    private void DiscardRdbChanges() throws RepositoryNotFoundException, InfoNotCompatibleException {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setBusy(true);
                setBusyMessage("Re-building the panel...");
            }
        });
        try {
            this.getRwDb().abort(); //abort the db changes
            IguiLogger.info("Discarded changes on RDB server." + OKS);
        } catch (final SystemException | NotAllowedException ex) {
            final String errMsg = "Failed aborting database changes: " + ex;
            Utils.logException(ex);
            TPGlobal.igui.internalMessage(MessageSeverity.ERROR, errMsg);
        }

        // re-init panel
        try {
            panelInit(TPGlobal.igui.getRCState(), TPGlobal.igui.getAccessLevel());
        } catch (IguiException ex) {
            Utils.logException(ex);
        }

        // revert the StandBy prescale to the values in OKS
        KeyDescriptor l1pskd = getKeyDescriptor(OKS.getL1PSKVar().value(), LVL.L1);
        KeyDescriptor hltpskd = getKeyDescriptor(OKS.getHLTPSKVar().value(), LVL.HLT);
        ChangePrescaleInIS(LVL.L1, RunState.STANDBY, l1pskd);
        ChangePrescaleInIS(LVL.HLT, RunState.STANDBY, hltpskd);

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setBusy(false);
            }
        });

    }

    /**
     * Writes the prescale key to IS (by level and ready-state)
     *
     * @param level - level
     * @param readystate - ready-state
     * @param kd - key to be written to IS
     */
    void ChangePrescaleInIS(final LVL level, final RunState readystate, final KeyDescriptor kd)
            throws RepositoryNotFoundException, InfoNotCompatibleException {
        String firstpart = "RunParams.";
        if(readystate == RunState.EMITTANCE){
            firstpart += "Emittance.";
        }
        else{
            firstpart += (readystate == RunState.STANDBY ? "Standby." : "Physics.");      
        }

        // it is possible there is no comment in the db, in which case we put "~" (cannot checkin null to IS)
        String opsname = "~";
        if (kd.name != null) {
            opsname = kd.name;
        }
        if (level == LVL.L1) {
            TrigConfL1PsKeyNamed l1IS = new TrigConfL1PsKeyNamed(TPGlobal.igui.getPartition(), firstpart + "L1PsKey");
            l1IS.L1PrescaleKey = kd.key;
            l1IS.L1PrescaleComment = opsname;
            if(readystate == RunState.EMITTANCE){
                IguiLogger.info("Writing to IS: Emittance. L1 PSK " + kd.key);                
            }
            else{
                IguiLogger.info("Writing to IS: " + (readystate == RunState.STANDBY ? "Standby." : "Physics.") + " L1 PSK " + kd.key);
            }
            l1IS.checkin();
        } else {
            TrigConfHltPsKeyNamed hltIS = new TrigConfHltPsKeyNamed(TPGlobal.igui.getPartition(), firstpart + "HltPsKey");
            hltIS.HltPrescaleKey = kd.key;
            hltIS.HltPrescaleComment = opsname;
            if(readystate == RunState.EMITTANCE){
                IguiLogger.info("Writing to IS: Emittance.HltPsKey " + kd.key);
            }
            else{
                IguiLogger.info("Writing to IS: " + (readystate == RunState.STANDBY ? "Standby." : "Physics.") + "HltPsKey " + kd.key);
            }
            hltIS.checkin();
        }
    }

    /**
     * Function called when the run state changes
     *
     * @param oldstate - the old state
     * @param newstate - the new state
     */
    @Override
    public void rcStateChanged(RunControlFSM.State oldstate, RunControlFSM.State newstate) {
        GUIState.currentState = newstate;
        IguiConstants.AccessControlStatus a = TPGlobal.igui.getAccessLevel();
        if (a.equals(IguiConstants.AccessControlStatus.CONTROL)) {
            // Initialize
            if (RunControlFSM.State.INITIAL.follows(oldstate) && // from < INITIAL
                    !RunControlFSM.State.INITIAL.follows(newstate)) { // to >= INITIAL
                InitializeTransition();
            }
            // Terminate
            if (!RunControlFSM.State.INITIAL.follows(oldstate) && // from >= INITIAL
                    RunControlFSM.State.INITIAL.follows(newstate)) { // to < INITIAL
                KeyDescriptor l1pskd = getKeyDescriptor(OKS.getL1PSKVar().value(), LVL.L1);
                KeyDescriptor hltpskd = getKeyDescriptor(OKS.getHLTPSKVar().value(), LVL.HLT);
                try {
                    ChangePrescaleInIS(LVL.L1, RunState.STANDBY, l1pskd);
                    ChangePrescaleInIS(LVL.HLT, RunState.STANDBY, hltpskd);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    Logger.getLogger(TriggerPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        tpgui.UpdatePanelForStateAndAccessControl();
        if(tpgui.getAliasPanel() != null) {
            tpgui.getAliasPanel().UpdatePanelForState();
        }
    }

    /**
     * Method called in INITIALIZE transition (BOOTED -> INITIAL) Discards
     * uncommitted trigger keys
     */
    public void InitializeTransition() {

        // subscribe a second time to IS, this time warn if info is not there
        boolean silent = false;
        TPGlobal.IS.subscribe(silent);

        if (!OKS.uncommitedDbChanges()) {
            return;
        } // db has not changed

        IguiLogger.info("Initializing without commiting everything, going to discard");

        try {
            DiscardRdbChanges();
        } catch (RepositoryNotFoundException ex) {
            Logger.getLogger(TriggerPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InfoNotCompatibleException ex) {
            Logger.getLogger(TriggerPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        // show information dialog
        SwingUtilities.invokeLater(new FutureTask<>(new Callable<Void>() {
            @Override
            public Void call() throws Exception {

                final String message = "Some trigger keys were not commited to OKS! "
                        + "They were discarded and you configured the trigger with\n"
                        + "SMK=" + OKS.getSMKVar().value() + ", L1 PSK=" + OKS.getL1PSKVar().value() + ", and HLT PSK=" + OKS.getHLTPSKVar().value() + " !";

                JOptionPane.showMessageDialog(null, message, getTabName(), JOptionPane.WARNING_MESSAGE);
                return null;
            }
        })
        );
    }

//    @Override
//    @SuppressWarnings("deprecation")
//    public void rcTransitionCommandSending(final RunControlFSM.Transition transition) {
//        IguiLogger.info("" + transition + "  " + GUIState.currentAccess);
//        if (transition.toString().equals("INITIALIZE")) {
//            if (GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL) {
//                panel.UpdatePanelForState(RunControlFSM.State.CONFIGURED, IguiConstants.AccessControlStatus.CONTROL);
//            }
//        }
//    }

    /**
     * Change the lumi block length
     *
     * @param time - true for lbmode time false for events - ignored in current
     * implementation
     * @param interval - the size of the lbinterval
     */
    public void changeLumiBlockInterval(boolean time, Integer interval) {
        // time 
        try {
            triggerCommander.setLumiBlockInterval(interval);
        } catch (TRIGGER.CommandExecutionFailed ex) {
            final String errMsg = "Failed to set LB length to " + interval + " : " + ex;
            IguiLogger.error(errMsg, ex);
            TPGlobal.igui.internalMessage(MessageSeverity.ERROR, errMsg);
        }

    }


}
