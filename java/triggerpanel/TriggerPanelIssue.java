/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

/**
 *
 * @author stelzer
 */
class TriggerPanelIssue extends ers.Issue {

    TriggerPanelIssue(final String message) {
        super("TriggerPanelIssue:" + message);
    }
    TriggerPanelIssue(final String message, final Exception ex) {
        super("TriggerPanelIssue:" + message, ex);
    }
    
    public static void main(String[] args) {
        ers.Logger.error(new TriggerPanelIssue("TEST 1"));
        ers.Logger.error(new TriggerPanelIssue("TEST 2", new Exception("Test exception")));
    }
}
