/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import is.NamedInfo;

/**
 *
 * @author William
 */
public class BooleanInfo extends NamedInfo {

    public boolean variable;

    public BooleanInfo(ipc.Partition partition, String name) {
        super(partition, name, "Boolean");
    }

    @Override
    public void publishGuts(is.Ostream out) {
        super.publishGuts(out);
        out.put(variable);
    }

    @Override
    public void refreshGuts(is.Istream in) {
        super.refreshGuts(in);
        variable = in.getBoolean();
    }

}
