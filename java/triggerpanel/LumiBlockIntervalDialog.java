package triggerpanel;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * @author  Joerg Stelzer 
 */
class LumiBlockIntervalDialog extends JDialog {

    /** Creates new form lumiBlockIntervalDialog
     * @param parent
     * @param modal
     **/
    LumiBlockIntervalDialog(Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        setTitle("Lumiblock length selection");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation( WindowConstants.DISPOSE_ON_CLOSE );
    }

    private static String defaultFont = "Dialog";

    private void initComponents() {

        // title line
        final JLabel lbl_title = new JLabel("Select LB interval length");
        lbl_title.setFont(new Font("Serif", Font.BOLD, 16));
        lbl_title.setForeground(new Color(0, 0, 153));
        lbl_title.setHorizontalAlignment(SwingConstants.LEFT);
        lbl_title.setVerticalAlignment(SwingConstants.BOTTOM);
        lbl_title.setAlignmentX(0.5F);

        JLabel title = new JLabel("Select LB interval length");
        title.setFont(new Font("Serif", Font.BOLD, 16));
        title.setForeground(new Color(0, 0, 153));

        JLabel timeLabel = new JLabel("Time limit");
        timeLabel.setFont(new Font(defaultFont, Font.PLAIN, 14));

        JLabel lbl_time = new JLabel("Time limit");
        lbl_time.setFont(new Font(defaultFont, Font.PLAIN, 14));

        m_timeTextField = new JTextField("60",5);
        m_timeTextField.setFont(new Font("SansSerif", Font.PLAIN, 12));
        m_timeTextField.setHorizontalAlignment(SwingConstants.RIGHT);
        m_timeTextField.setColumns(5);
        m_timeTextField.setMaximumSize(new Dimension(50,26));
        m_timeTextField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent evt) {
                timeTextFieldKeyReleased(evt);
            }
        });

        m_updateButton = new JButton("Apply");
        m_updateButton.setPreferredSize(new Dimension(80, 24));
        m_updateButton.setEnabled(false);
        m_updateButton.addActionListener((evt) -> {
            updateButtonActionPerformed(evt);
        });

        JButton btn_cancel = new JButton("Cancel");
        btn_cancel.addActionListener((evt) -> {
            cancelButtonActionPerformed(evt);
        });

        JLabel lbl_seconds = new JLabel("seconds");
        lbl_seconds.setFont(new Font(defaultFont, Font.PLAIN, 12));

        /**** arrange the components *****/
        JPanel panel_main = new JPanel();
        panel_main.setBorder(BorderFactory.createEmptyBorder(20, 10, 10,10));
        panel_main.setLayout(new BorderLayout());

        /**** the title panel *****/
        JPanel panel_title = new JPanel();
        panel_title.setLayout(new BoxLayout(panel_title, BoxLayout.LINE_AXIS));
        panel_title.add(lbl_title);
        panel_main.add(panel_title, BorderLayout.NORTH);

        /**** the panel with the seconds input *****/
        JPanel panel_input = new JPanel();
        panel_input.setLayout(new BoxLayout(panel_input, BoxLayout.LINE_AXIS));
        panel_input.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_input.add(lbl_time);
        panel_input.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_input.add(m_timeTextField);
        panel_input.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_input.add(lbl_seconds);
        panel_main.add(panel_input, BorderLayout.CENTER);

        /**** the button panel *****/
        final JPanel panel_buttons = new JPanel();
        panel_buttons.setLayout(new BoxLayout(panel_buttons, BoxLayout.LINE_AXIS));
        panel_buttons.setBorder(BorderFactory.createEmptyBorder(0, 10, 10,10));
        panel_buttons.add(Box.createHorizontalGlue());
        panel_buttons.add(btn_cancel);
        panel_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_buttons.add(m_updateButton);
        panel_main.add(panel_buttons, BorderLayout.SOUTH);

        setContentPane(panel_main);

        pack();
        
        // set min and max dimensions, allowing extension only in width
        int currentHeight = getSize().height;
        setMinimumSize(new Dimension(400, currentHeight));
        setMaximumSize(new Dimension(getMaximumSize().width, currentHeight));

    }

    protected void updateButtonActionPerformed(ActionEvent evt)
    {}

    protected void cancelButtonActionPerformed(ActionEvent evt) {
        dispose();
    }

    protected void timeTextFieldKeyReleased(KeyEvent evt)
    {}

    protected JButton m_updateButton;
    protected JTextField m_timeTextField;

    public static void main(String args[]) {
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PSSelectionDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(() -> {
            LumiBlockIntervalDialog dialog = new LumiBlockIntervalDialog(new JFrame(), false);
            dialog.setVisible(true);
        });
    }


}
