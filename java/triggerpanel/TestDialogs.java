package triggerpanel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

public class TestDialogs extends JFrame {
    final private Font mainFont = new Font("Dialog",  Font.PLAIN, 18);
    final ArrayList<JLabel> labels = new ArrayList<JLabel>();
    final ArrayList<JButton> buttons = new ArrayList<JButton>();

    final private void addLine(String label, ActionListener listener) {
        final JLabel jLabel = new JLabel(label);
        jLabel.setFont(mainFont);
        labels.add(jLabel);
        final JButton jButton = new JButton("Open"); 
        jButton.addActionListener(listener);
        buttons.add(jButton);
    }

    final private void addLineToGrid(final JPanel panel, GridBagConstraints cbg, JLabel label, JButton button) {
        cbg.gridx = 0; cbg.weightx = 1;
        panel.add(label,cbg);
        cbg.gridx++; cbg.weightx = 0;
        // c.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(button,cbg);
        cbg.gridy++;
    }

    public void initialize() {

        addLine("Shifter panel", (e) -> {
            JFrame frame = new JFrame("Shifter Panel");
            frame.getContentPane().add(new ShifterPanel());
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.pack();
            frame.setLocation(600,250);
            frame.setVisible(true);
        });

        addLine("Shifter panel new", (e) -> {
            JFrame frame = new JFrame("Shifter Panel New");
            frame.getContentPane().add(new ShifterPanel());
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.pack();
            frame.setLocation(600,250);
            frame.setVisible(true);
        });

        addLine("Expert panel", (e) -> {
            JFrame frame = new JFrame("Expert Panel");
            frame.getContentPane().add(new ExpertPanel());
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.pack();
            frame.setLocation(600,250);
            frame.setVisible(true);
        });

        addLine("Full panel", (e) -> {
            JFrame frame = new JFrame("Full panel");
            frame.setSize(800,600);
            frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            frame.getContentPane().add(new TriggerPanelGUI());
            frame.setLocation(600,250);
            frame.setVisible(true);
        });

        addLine("Prescale selection", (e) -> {
            PSSelectionDialog dialog = new PSSelectionDialog(RunState.STANDBY, RunState.STANDBY, new JFrame(), false);
            dialog.setLocation(600,500);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.dropdown_L1.addItem(new KeyDescriptor(0,"1  Hello this is a very long super master key that should not fit, I will repeat, it is very very long",0,"",false));
            dialog.pack();
            dialog.setVisible(true);
        });

        addLine("Bunchgroup selection", (e) -> {
            BGSelectionDialog dialog = new BGSelectionDialog(new JFrame(), false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setLocation(600,300);
            dialog.setVisible(true);
        });

        addLine("SMK selection", (e) -> {
            SMKSelectionDialog dialog = new SMKSelectionDialog(new JFrame(), false);
            dialog.cob_smk.addItem(new KeyDescriptor(1,"Physics menu",1,"The physics menu",false));
            dialog.cob_smk.addItem(new KeyDescriptor(2,"Cosmics menu",1,"The cosmics menu",false));
            dialog.cob_smk.addItem(new KeyDescriptor(3,"ALFA menu",1,"The ALFA menu",false));
            dialog.cob_smk.addItem(new KeyDescriptor(4,"VdM menu",1,"The VdM menu",true));
            dialog.cob_smk.addItem(new KeyDescriptor(5,"VdM menu",2,"The updated VdM menu",false));
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setLocation(600,500);
            dialog.setVisible(true);
            System.out.println("Action was " + dialog.button_pressed() + "  ");
        });

        addLine("LB length selection", (e) -> {
            LumiBlockIntervalDialog dialog = new LumiBlockIntervalDialog(new JFrame(), false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setLocation(600,500);
            dialog.setVisible(true);
        });

        addLine("CTP deadtime selection", (e) -> {
            CTPDeadtimeDialog dialog = new CTPDeadtimeDialog(new JFrame(), false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setLocation(600,500);
            dialog.setVisible(true);
        });

        addLine("Alias selection", (e) -> {
            AliasSelectionDialog dialog = new AliasSelectionDialog(new JFrame(), false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.pack();
            dialog.setLocation(600,500);
            dialog.setVisible(true);
        });


        /************* create panel ***********/
        JPanel panel = new JPanel();
        GridBagLayout gbl = new GridBagLayout();
        panel.setLayout(gbl);
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets( 10, 0, 0, 0);
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.WEST;
        c.gridy = 0; // start in the first line
        c.ipadx = 40; // default height of elements
        c.ipady = 20; // default height of elements
        for(int i = 0; i<labels.size(); i++) {
            addLineToGrid(panel, c, labels.get(i), buttons.get(i));
        }
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.setOpaque(false);

        /************* buttons ************/
        JButton btnClose = new JButton("Close");
        btnClose.setFont(mainFont);
        btnClose.addActionListener((evt) -> {
            dispose();
            System.exit(0);
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel.add(btnClose);
        buttonsPanel.setOpaque(false);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setBackground(new Color(128,128,255));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPanel.add(panel, BorderLayout.CENTER);
        mainPanel.add(buttonsPanel, BorderLayout.SOUTH);

        add(mainPanel);

        pack();

        setTitle("Test Dialogs");
        setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args) {

        try {
            // Possible values for the look and feel: Metal, Nimbus, CDE/Motif, GTK+
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(TestDialogs.class.getName()).log(Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(() -> {
            TestDialogs myFrame = new TestDialogs();
            myFrame.initialize();
        });
    }
}
