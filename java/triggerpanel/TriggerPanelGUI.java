package triggerpanel;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.event.WindowEvent;

public class TriggerPanelGUI extends JPanel {

    public TriggerPanelGUI() {
        super(new BorderLayout());
        initComponents();
    }

    public ExpertPanel getAliasPanel() {
        return expertPanel;
    }

    public ShifterPanel getStandardPanel() {
        return standardPanel;
    }

    /******* protected methods ********/

    protected void showDialogNearMouse(JDialog dialog) {
        standardPanel.showDialogNearMouse(dialog);
    }

    protected ShifterPanel createStandardPanel() {
        return new ShifterPanel();
    }

    protected ExpertPanel createExpertPanel() {
        return new ExpertPanel();
    }

    /***** building the panel with tabs and scrollbar *****/
    private void initComponents() {

        final JScrollPane sp;

        standardPanel = createStandardPanel();
        boolean useTabs = false;
        if(useTabs) {
            expertPanel = createExpertPanel();
            tabbedPane = new JTabbedPane();
            tabbedPane.addTab("Standard", standardPanel);
            tabbedPane.addTab("Alias", expertPanel);
            tabbedPane.setPreferredSize(standardPanel.getPreferredSize());
            sp = new JScrollPane(tabbedPane); // make it scrollable
        } else {
            expertPanel = null;
            sp = new JScrollPane(standardPanel); // make it scrollable
        }
        sp.getVerticalScrollBar().setUnitIncrement(16); // make scrolling faster
        sp.getHorizontalScrollBar().setUnitIncrement(16); // make scrolling faster
        add(sp);
    }

    private ExpertPanel expertPanel;
    private ShifterPanel standardPanel;
    private JTabbedPane tabbedPane;

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(() -> {
            TriggerPanelGUI tpgui = new TriggerPanelGUI();
            tpgui.getStandardPanel().addTestActions();
            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.getContentPane().add(tpgui);
            frame.pack();
            frame.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {
                    System.exit(0);
                }
            });  
            frame.setVisible(true);
        });
    }

}
