package triggerpanel;

class HLTPSKVariable extends OKSVariable<Integer> {

    HLTPSKVariable() {
        super("HLTPSK");
    }


    @Override
    protected void writeToRDB(Integer value, config.Configuration rdb) {
        dal.TriggerConfiguration trigconf = TPGlobal.getConfigHelper().getTriggerConfiguration(rdb);
        if (trigconf==null) { return; }

        HLTPUDal.HLTImplementationDB dbtrigconf = null;
        try{
            dbtrigconf = HLTPUDal.HLTImplementationDB_Helper.cast(trigconf.get_hlt());
            if( dbtrigconf == null ) { TPL.error("Could not retrieve HLT TriggerConfiguration from OKS"); return; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve HLT TriggerConfiguration from OKS");
            return;
        }

        try{
            dbtrigconf.set_hltPrescaleKey( Integer.toString(value) );
        }
        catch(config.ConfigException ex){
            TPL.error("Could not set HLTPSK" + ex);
        }
    }

    @Override
    protected Integer readFromRDB(config.Configuration rdb) {
        dal.TriggerConfiguration trigconf = TPGlobal.getConfigHelper().getTriggerConfiguration(rdb);
        if (trigconf==null) { return 0; }

        HLTPUDal.HLTImplementationDB dbtrigconf = null;
        try{
            dbtrigconf = HLTPUDal.HLTImplementationDB_Helper.cast(trigconf.get_hlt());
            if( dbtrigconf == null ) { TPL.error("Could not retrieve HLT TriggerConfiguration from OKS"); return 0; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve HLT TriggerConfiguration from OKS");
            return 0;
        }

        Integer HLTPSK = 0;
        try{
            dbtrigconf.update();
            String ps = dbtrigconf.get_hltPrescaleKey();
            HLTPSK = Integer.parseInt(ps);
        }
        catch(config.ConfigException ex){
             TPL.error("Could not retrieve HLTPSK" + ex);
        }

        return HLTPSK;
    }

}
