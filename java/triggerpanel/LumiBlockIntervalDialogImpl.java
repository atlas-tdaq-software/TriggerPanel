package triggerpanel;

import Igui.IguiLogger;
import java.awt.Frame;
import java.awt.event.ActionEvent;


/**
 *
 * @author  markowen
 * @author  Joerg Stelzer 
 */
class LumiBlockIntervalDialogImpl extends LumiBlockIntervalDialog {

    int current_value = 120;

    /** Trigger Configuration panel */
    final private TriggerPanel triggerPanel;

    /** Creates new form lumiBlockIntervalDialog
     * @param parent
     * @param modal
     * @param thepanel */
    LumiBlockIntervalDialogImpl(Frame parent, boolean modal, TriggerPanel thepanel) {
        super(parent, modal);
        setLocationRelativeTo(parent);

        triggerPanel = thepanel;

        if(triggerPanel != null) {
            current_value = GUIState.getLBinterval();
        }

        m_timeTextField.setText(Integer.toString(current_value));
    }

    // Send the update to the run control
    @Override
    protected void updateButtonActionPerformed(ActionEvent evt) {
        //read the time units
        try {
            Integer time = Integer.parseInt(m_timeTextField.getText());
            triggerPanel.changeLumiBlockInterval(true,time);
        }
        catch (NumberFormatException ex) {
            IguiLogger.warning("Invalid number in the time field");
            return; //allow the string to be re-entered
        }
        dispose();
    }

    @Override
    protected void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
        dispose();
    }

    @Override
    protected void timeTextFieldKeyReleased(java.awt.event.KeyEvent evt) {
        try {
            int value = Integer.parseInt(m_timeTextField.getText());
            boolean unchanged = (current_value==value);
            m_updateButton.setEnabled(!unchanged);
        } catch (NumberFormatException ex) {
            m_updateButton.setEnabled(false);
        }
    }

}
