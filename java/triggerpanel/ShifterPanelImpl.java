/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

/**
 *
 * @author stelzer
 */
public class ShifterPanelImpl extends ShifterPanel {

    // constructor
    public ShifterPanelImpl() {
        super();
    }

    @Override
    public void setKeys(RunState readystate, LVL level, KeyDescriptor kd) {
        if (level == LVL.L1) {
            lbMap_l1psKeys.get(readystate).setText((kd.key>=0)?""+kd.key:"");
            lbMap_l1psNames.get(readystate).setText(kd.name);
            lbMap_l1psNames.get(readystate).setToolTipText(Utils.CreateToolTip(kd.comment));
        } else {
            lbMap_hltpsKeys.get(readystate).setText((kd.key>=0)?""+kd.key:"");
            lbMap_hltpsNames.get(readystate).setText(kd.name);
            lbMap_hltpsNames.get(readystate).setToolTipText(Utils.CreateToolTip(kd.comment));
        }
    }
}
