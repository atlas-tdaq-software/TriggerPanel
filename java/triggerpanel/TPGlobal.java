/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import Igui.Igui;
import config.Configuration;
import java.awt.Color;

/**
 *
 * @author stelzer
 */
class TPGlobal {

    final static Color activemodeclr   = new Color(86, 185, 18);
    final static Color inactivemodeclr = new Color(180, 180, 180);
    final static Color uncommittedclr  = new Color(247, 21, 218);
    final static Color committedclr  = new Color(5, 36, 246);
    final static Color black           = new Color(0, 0, 0);
    
    static TPGlobal instance = null;
    static int instanceCount = 0;
    
    static Igui igui = null;

    // the IS listeners
    static TPListenerHandler IS = null;
    
    static OKSHandler OKS = null;

    static DBKeysLoader dbKeysLoader = null;

    static private config.Configuration roDb = null;
    
    static private config.Configuration rwDb = null;

    static private ConfigHelper configHelper = null;
    
    static DBKeysLoader theDbKeysLoader() {
        return dbKeysLoader;
    }

    static void setRoDb(Configuration roDb) {
        TPGlobal.roDb = roDb;
    }

    static void setRwDb(Configuration rwDb) {
        TPGlobal.rwDb = rwDb;
    }

    static Configuration getRoDb() {
        return roDb;
    }

    static Configuration getRwDb() {
        return rwDb;
    }

    static ConfigHelper getConfigHelper() {
        return configHelper;
    }

    static void init(Igui igui) {
        if(instance==null) {
            instance = new TPGlobal(igui);
        }
        instanceCount++;
    }

    static void release() {
        instanceCount--;
        if(instanceCount==0) {
            instance.destroy();
        }
    }

    public TPGlobal(Igui igui) {
        // igui
        TPGlobal.igui = igui;
        
        // IS
        IS = new TPListenerHandler();
        
        OKS = new OKSHandler();
        
        // config helper
        configHelper = new ConfigHelper(igui.getPartition().getName());

        dbKeysLoader = new DBKeysLoader();
    }
    

    private void destroy() {
        IS.unsubscribe();
    }

    
}
