/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerpanel;

public class KeyDescriptor {

    static KeyDescriptor NoPSKforPartition = new KeyDescriptor(0, "No prescale key for this partition", 0, "No valid key for partition", false);
    static KeyDescriptor NotValidForSMK = new KeyDescriptor(0, "Not valid for this menu", 0, "No valid key for SMK", false);
    static KeyDescriptor NotInIS = new KeyDescriptor(-1, "Not found in IS", 0, "No key in IS", false);
    static KeyDescriptor NULL = new KeyDescriptor(0, "", 0, "", false);

    int    key = 0; // 0 means 'invalid key', -1 means 'not present in IS'; this can be used to indicate such a state, except when read from the database
    String name = "";
    int    version = 0;
    String comment = "";
    boolean hidden = false;

    public KeyDescriptor(final int key, final String name, final int version, final String comment, final boolean hidden) {
        this.key = key;
        this.name = name;
        this.version = version;
        this.comment = comment;
        this.hidden = hidden;
    }

    @Override
    public String toString() {
        // do not change this format, it is used in the dropdown boxes
        if(key > 0) {
            return key + ":  " + name + "  v." + version;
        } else {
            return this.name;
        }
    }
}
