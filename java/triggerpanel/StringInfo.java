package triggerpanel;

import is.NamedInfo;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author William
 */
public class StringInfo extends NamedInfo{
        public String variable;

    public StringInfo(ipc.Partition partition, String name) {
        super(partition, name, "String");
    }

    @Override
    public void publishGuts(is.Ostream out) {
        super.publishGuts(out);
        out.put(variable);
    }

    @Override
    public void refreshGuts(is.Istream in) {
        super.refreshGuts(in);
        variable = in.getString();
    }
}