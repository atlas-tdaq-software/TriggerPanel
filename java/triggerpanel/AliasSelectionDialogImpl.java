package triggerpanel;

import static triggerpanel.ExpertPanelImpl.NoAliasAvailable;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;

import Igui.IguiLogger;

import java.awt.event.*;

/**
 *
 * @author William, Joerg
 */
public class AliasSelectionDialogImpl extends AliasSelectionDialog {

    PrescaleSetAliasHandle aliasHandle = null;
    int smk = 0;
    RunState runMode = null;
    KeyDescriptor newl1 = null;
    KeyDescriptor newhlt = null;
    String newAlias = null;
    Boolean cancelled = false;
    Boolean fillingAliasComboBox = true;

    /**
     * Creates new form AliasSelectionDialog
     *
     * @param parent
     * @param modal
     * @param mode
     * @param smkey
     * @param currentAlias
     * @param currentL1PSK
     * @param currentHLTPSK
     * @throws java.sql.SQLException
     */
    public AliasSelectionDialogImpl(JFrame parent, boolean modal, 
        RunState mode, final int smkey, final String currentAlias, 
        final String currentL1PSK, final String currentHLTPSK) throws SQLException
    {
        super(parent, modal);
        smk = smkey;
        runMode = mode;
        titleLabel.setText("Select Alias for " + runMode);
        currentAliasNameLabel.setText(currentAlias);
        currentL1ValueLabel.setText(currentL1PSK);
        currentHLTValueLabel.setText(currentHLTPSK);
        SelectButton.setEnabled(false);
        matchWarningLabel.setForeground(TPGlobal.uncommittedclr);
        matchWarningLabel.setVisible(false);
        if (runMode == RunState.PHYSICS) {
            newPrescalesLabel.setVisible(false);
            newHLTLabel.setVisible(false);
            newHLTValueLabel.setVisible(false);
            newL1Label.setVisible(false);
            newL1ValueLabel.setVisible(false);
            overrideWarningLabel.setVisible(true);
            SelectButton.setText("Select");
        } else {
            SelectButton.setText("Apply");
            overrideWarningLabel.setVisible(false);
        }
        if (initPanel()) {
        }
    }

    private boolean initPanel() throws SQLException {
        TPGlobal.theDbKeysLoader().SetSMK(smk);
        aliasHandle = TPGlobal.theDbKeysLoader().GetAliases();
        if (aliasHandle == null) {
            IguiLogger.error("Aliases don't exist");
            return true;
        }
        IguiLogger.info("Will update the " + runMode + " comboboxes for SMK " + smk);
        aliasComboBox.removeAllItems();
        aliasHandle.getAliasComponents().stream()
                .forEach((comp) -> {
                    if (runMode == RunState.STANDBY) {
                        if (comp.getType().equals("STANDBY") || comp.getType().equals("COSMIC") || comp.getType().equals("OTHER")) {
                            IguiLogger.info("Adding standby/cosmic/other item " + comp.getName());
                            aliasComboBox.addItem(comp.getName());
                        }
                    } else if (runMode == RunState.EMITTANCE) {
                        if (comp.getType().equals("EMITTANCE")) {
                            IguiLogger.info("Adding emittance item " + comp.getName());
                            aliasComboBox.addItem(comp.getName());
                        }
                    } else if (runMode == RunState.PHYSICS) {
                        if (comp.getType().equals("PHYSICS")) {
                            IguiLogger.info("Adding physics item " + comp.getName());
                            aliasComboBox.addItem(comp.getName());
                        }
                    } else {
                        TPL.error("INVALID RUN MODE! " + runMode);
                    }
                });
        fillingAliasComboBox = false;

        aliasComboBox.setEnabled(aliasComboBox.getItemCount() > 0);
        aliasComboBox.setSelectedIndex(
                -1);
        if (aliasComboBox.getItemCount()
                == 0) {
            aliasComboBox.addItem(NoAliasAvailable);
        } else {
            IguiLogger.info("Will select alias item 0");
            aliasComboBox.setSelectedIndex(0);
        }
        boolean physicsCBHasContent
                = aliasComboBox.getItemCount() > 0
                && !aliasComboBox.getItemAt(0).equals(NoAliasAvailable);

        aliasComboBox.setEnabled(physicsCBHasContent);

        return false;
    }

    public KeyDescriptor getL1Key() {
        return newl1;
    }

    public KeyDescriptor getHLTKey() {
        return newhlt;
    }

    public String getAlias() {
        return newAlias;
    }

    public Boolean wasCancelled() {
        return cancelled;
    }

    @Override
    protected void aliasComboBoxAction(ActionEvent evt) {

        if (fillingAliasComboBox) {
            return;
        }
        ArrayList<String> types = new ArrayList<>();
        switch (runMode) {
            case STANDBY:
                types.add("STANDBY");
                types.add("COSMIC");
                types.add("OTHER");
                break;
            case EMITTANCE:
                types.add("EMITTANCE");
                break;
            case PHYSICS:
                types.add("PHYSICS");
                break;
            default:
                break;
        }

        PrescaleSetAliasHandle.AliasComponent selectedComp = null;
        for (String type : types) {
            PrescaleSetAliasHandle.AliasComponent comp;
            try {
                comp = TPGlobal.theDbKeysLoader().GetAliases().getAliasComponent(type, (String) aliasComboBox.getSelectedItem());
                if (comp != null) {
                    selectedComp = comp;
                }
            } catch (SQLException ex) {
                TPL.error("SQL exception extracting alias list: " + ex.getMessage(), true);
            }

        }
        if (selectedComp != null) {
            if (selectedComp.getName().equals(currentAliasNameLabel.getText())) {
                matchWarningLabel.setVisible(true);
                SelectButton.setEnabled(false);
            } else {
                matchWarningLabel.setVisible(false);
                SelectButton.setEnabled(true);
            }
            if (runMode != RunState.PHYSICS) {
                newL1ValueLabel.setText(Integer.toString(selectedComp.getLumis().get(0).l1psk));
                newHLTValueLabel.setText(Integer.toString(selectedComp.getLumis().get(0).hltpsk));
            }
        } else {
            TPL.warning("Could not find any aliases for type: " + types, true);
        }
    }

    @Override
    protected void cancelButtonAction(ActionEvent evt) {
        cancelled = true;
        dispose();
    }

    @Override
    protected void selectButtonAction(ActionEvent evt) {
        if (runMode != RunState.PHYSICS) {
            newl1 = new KeyDescriptor(Integer.parseInt(newL1ValueLabel.getText()), "0", 0, "0", false);
            newhlt = new KeyDescriptor(Integer.parseInt(newHLTValueLabel.getText()), "0", 0, "0", false);
        }
        newAlias = (String) aliasComboBox.getSelectedItem();
        dispose();
    }

}
