package triggerpanel;

import Igui.IguiConstants;
import Igui.IguiLogger;
import daq.rc.RCException;

import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

/**
 *
 * @author stelzer
 */
final class Utils {
    
    /**
     * Turn string into html string with line breaks (for tool tip)
     * @param original
     * @return String that can be used for tooltip
     **/
    static String CreateToolTip(final String original) {
        if( original.isEmpty() ) {
            return "<html><b>No comment</b></html>";
        }
        String work_copy = "<b>Comment:</b> " + original.trim();
        String tooltip = "<html>";
        boolean done  = false;
        while(!done) {
            if(work_copy.length()<80) { 
                tooltip += work_copy;
                done = true;
                continue;
            }
            int splitpos = work_copy.lastIndexOf(' ',80);
            if (splitpos == -1) { splitpos = work_copy.indexOf(' ',80); }
            if (splitpos == -1) {
                tooltip += work_copy;
                done = true;
            } else {
                tooltip += work_copy.substring(0,splitpos).trim();
                tooltip += "<br>";
                work_copy = work_copy.substring(splitpos).trim();
            }
        }
        tooltip += "</html>";
        return tooltip;
    }

    /**
    * Function to send command via the Commander
    * @param controller - name of controller to send command to
    * @param command - the command (e.g. "USER")
    * @param arguments - arguments for the command
    * @throws RCException
    */
    static void sendUserCommand(String controller, String command, String[] arguments) throws RCException 
    {
        TPL.info("Send run-control command \"" + command + "\" with arguments \"" + Arrays.toString(arguments) + "\" to controller \"" + controller + "\"");
        
        daq.rc.CommandSender cs;
        cs = new daq.rc.CommandSender(TPGlobal.igui.getPartition().getName(), "TriggerPanel");
        cs.userCommand(controller, command, arguments);
    }

    static void logException(Exception ex) {
        String msg = "Exception caught: " + ex + "\n";
        for (StackTraceElement ste : ex.getStackTrace() ) {
            msg += "    at " + ste.toString() + "\n";
        }
        IguiLogger.error(msg);
    }
    
    /**
     * Small helper function to attach a comment to the end of name to get
     * name  //  comment.
     * If comment is long, changes it to comm....
     * @param name
     * @param comment
     * @return
     */
    static String attachCommentToName(String name, String comment) {

        if(name!=null) {
            String combname = name;
            // attach comment to the name
            if(comment!=null) {
                String shortcomment = comment;
                if(comment.length() > 50) {
                    shortcomment = comment.substring(0,50);
                    shortcomment += "....";
                }
                combname += "  //  ";
                combname += shortcomment;
            }
            return combname;
        } //name !null
        else {
            return null;
        }
    }

    static void assertEDT() {
        if( ! SwingUtilities.isEventDispatchThread() ) {
            String msg = "TriggerPanel issue: code not in EDT, TriggerPanel might not perform correctly\n";
            msg += Thread.currentThread().getStackTrace()[2].toString();
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.WARNING,msg);
            IguiLogger.warning(msg);
        }
    }

    static String getStackTrace() {
        String msg = "Calling stack of current thread " + Thread.currentThread().getId() + "\n";
        for (StackTraceElement ste : Thread.currentThread().getStackTrace() ) {
            msg += "    at " + ste.toString() + "\n";
        }
        return msg;
    }

    static int getRunnumber() {
        // get the run number from IS
        int run_number;
        try {
            rc.RunParamsNamed rp = new rc.RunParamsNamed(TPGlobal.igui.getPartition(), "RunParams.RunParams");
            rp.checkout();
            //System.out.print(rp);
            run_number = rp.run_number;
            IguiLogger.info("Got run number " + run_number + " from IS.");
        } catch (is.RepositoryNotFoundException | is.InfoNotFoundException | is.InfoNotCompatibleException e) {
            IguiLogger.error("IS server not found, can not request LB increase ", e);
            return -1;
        }
        return run_number;
    }

    static String getBuiltDate() {
        
        String className = Utils.class.getSimpleName() + ".class";
        String classPath = Utils.class.getResource(className).toString();
        System.out.println(classPath);
        if (!classPath.startsWith("jar")) {
            return "Unknown";
        }
        try {
            URL url = new URL(classPath);
            JarURLConnection jarConnection = (JarURLConnection) url.openConnection();
            for(Object key : jarConnection.getMainAttributes().keySet() ) {
                System.out.println("Key " + key.toString());
            }
            long seconds = jarConnection.getLastModified();
            Date date = new Date(seconds);
            SimpleDateFormat sdf = new SimpleDateFormat("EE,MMMM d,yyyy H:mm", Locale.ENGLISH);
            sdf.setTimeZone(TimeZone.getTimeZone("CEST"));
            String formattedDate = sdf.format(date);
            return formattedDate;
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Unknown";
    }
    
    private Utils() {}

}
