package triggerpanel;

class L1PSKVariable extends OKSVariable<Integer> {

    L1PSKVariable() {
        super("L1PSK");
    }

    @Override
    protected void writeToRDB(Integer value, config.Configuration rdb) {
    
        dal.L1TriggerConfiguration l1conf = TPGlobal.getConfigHelper().getL1TriggerConfiguration(rdb);
        if (l1conf==null) return;

        try{
            l1conf.set_Lvl1PrescaleKey(value);
        }
        catch(config.ConfigException ex){
            TPL.error("Could not set L1PSK" + ex);
        }

    }

    @Override
    protected Integer readFromRDB(config.Configuration rdb) {
    
        dal.L1TriggerConfiguration l1conf = TPGlobal.getConfigHelper().getL1TriggerConfiguration(rdb);
        if (l1conf==null) return 0;

        Integer L1PSK = 0;
        try{
            l1conf.update();
            L1PSK = l1conf.get_Lvl1PrescaleKey();
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve L1PSK" + ex);
        }
        return L1PSK;

    }

}
