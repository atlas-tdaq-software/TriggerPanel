package triggerpanel;

import Igui.IguiLogger;

class SMKVariable extends OKSVariable<Integer> {

    public SMKVariable() { 
        super("SMK");
    }

    @Override
    protected void writeToRDB(Integer value, config.Configuration rdb) {
        IguiLogger.info("Writing " + value + " to configuration DB");
        dal.TriggerDBConnection dbconn = TPGlobal.getConfigHelper().getTriggerDBConnection(rdb);
        if (dbconn==null) {
            IguiLogger.warning("Could not update SMK in RDB server, no TriggerDBConnection object found");
            return;
        }
        try{
            dbconn.set_SuperMasterKey(value);
        }
        catch(config.ConfigException ex){
            IguiLogger.error("Could not set SMK. Reason: " + ex);
        }
    }

    @Override
    protected Integer readFromRDB(config.Configuration rdb) {
        dal.TriggerDBConnection dbconn = TPGlobal.getConfigHelper().getTriggerDBConnection(rdb);
        if (dbconn==null) {
            return 0;
        }

        Integer SMK = 0;
        try {
            dbconn.update();
            SMK = dbconn.get_SuperMasterKey();
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve SMK" + ex);
        }
        return SMK;
    }

}
