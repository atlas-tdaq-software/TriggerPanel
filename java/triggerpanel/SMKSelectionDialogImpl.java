/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import java.awt.event.ActionEvent;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingUtilities;

import Igui.IguiLogger;

class SMKSelectionDialogImpl extends SMKSelectionDialog {

    //private TriggerPanel triggerPanel = null;
    private int currentSMK = 0;
    private int selectedSMK = 0;

    public int getSelectedSMK() {
        return selectedSMK;
    }

    SMKSelectionDialogImpl(boolean modal, final int currentSMK) {
        super(null, modal);

        this.currentSMK = currentSMK;
        (new SMKSelectionDialogImpl.smLoader()).execute();
        lb_current_smk.setText(Integer.toString(this.currentSMK));
    }
    /**
     * Small utility class to load super masters from db on a worker thread.
     * Once loaded
     */
    private class smLoader extends javax.swing.SwingWorker<Void, Void> {

        @Override
            public Void doInBackground() {
            TPGlobal.theDbKeysLoader().UpdateMenus();
            return null;
        }

        @Override
            public void done() {
            try{
                this.get();
            } catch(InterruptedException | ExecutionException ex) {
                IguiLogger.warning("Problem in smLoader: " + ex.getMessage());
                ex.printStackTrace();
            }
            fillDropDownBox();
            setVisible(true);
        }

    }

    private void fillDropDownBox() {
        cob_smk.removeAllItems();
        for(KeyDescriptor kd : TPGlobal.theDbKeysLoader().GetMenus()) {
            if(!chb_showHidden.isSelected() && kd.hidden) {
                continue;
            }
            //String el = kd.key + ":  " + kd.name + "  v." + kd.version;
            cob_smk.addItem(kd);
            if(kd.key==currentSMK) {
                cob_smk.setSelectedItem(kd);
            }
        }
    }

    @Override
    final protected void showHiddenAction(ActionEvent evt) {
        SwingUtilities.invokeLater( new Runnable() {
            @Override
            public void run() {
                fillDropDownBox();
            }
        });
    }
    
    @Override
    final protected void selectButtonAction(ActionEvent evt) {
        IguiLogger.info("Applying SMK selected from dropdown: " + selectedSMK + ", was " + currentSMK);
        super.selectButtonAction(evt);
    }

    @Override
    final protected void cancelButtonAction(ActionEvent evt){
        selectedSMK = currentSMK;
        super.cancelButtonAction(evt);
    }

    @Override
    final protected void smkSelectionChanged(ActionEvent evt) {                                            
        SwingUtilities.invokeLater(() -> {
            selectedSMK = ((KeyDescriptor)cob_smk.getSelectedItem()).key;
            boolean keyChanged = (selectedSMK != currentSMK);
            btn_select.setText(keyChanged?"Apply":"Close");
            btn_cancel.setEnabled(keyChanged);
        });
    }
}
