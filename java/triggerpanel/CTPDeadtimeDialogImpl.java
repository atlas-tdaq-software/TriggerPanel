package triggerpanel;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JFrame;
import Igui.IguiLogger;
import config.ConfigObject;

public class CTPDeadtimeDialogImpl extends CTPDeadtimeDialog {

    public CTPDeadtimeDialogImpl(JFrame parent, boolean modal) {
        super(parent, modal);
    }

    @Override
    protected ArrayList<String> getAllTCTPDeadtimeConfigObjects() {
        ArrayList<String> ctp_cfg_uids = new ArrayList<>();
        for(ConfigObject obj: TPGlobal.getConfigHelper().getAllTCTPDeadtimeConfigObjects(TPGlobal.getRoDb())) {
            ctp_cfg_uids.add(obj.UID());
        }
        return ctp_cfg_uids;
    }

    @Override
    protected void applyActionPerformed(ActionEvent evt) {
        String selection = getSelection();    
        if(!selection.equals("")){
            IguiLogger.info("triggerPanel setting deadtime configuration to " + selection);
            TPGlobal.OKS.getCTPDeadtimeVar().setValue(selection);
        }
        dispose();
    }
}
