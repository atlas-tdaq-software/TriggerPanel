/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import static ers.Logger.error;
import static triggerpanel.TPGlobal.OKS;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;

import javax.swing.*;
import javax.swing.border.TitledBorder;

import Igui.*;
import Igui.RunControlFSM.State;
import TRIGGER.CommandExecutionFailed;
import is.InfoNotCompatibleException;
import is.RepositoryNotFoundException;

public class TriggerPanelGUIImpl extends TriggerPanelGUI {

    private final TriggerPanel triggerPanel;

    final private Color activemodeclr   = new Color(86, 185, 18);
    final private Color inactivemodeclr = new Color(180, 180, 180);
    final private Color uncommittedclr  = new Color(247, 21, 218);
    final private Color black           = new Color(0, 0, 0);

    // constructor
    TriggerPanelGUIImpl(TriggerPanel tp) {

        triggerPanel = tp;

        /***** adding button listeners for standard panel *****/
        // Trigger Menu
        getStandardPanel().btn_changeSMK.addActionListener((evt) -> {
            SMKSelectionDialogImpl dialog = new SMKSelectionDialogImpl(true, triggerPanel.getCurrentSMK());
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if(! dialog.is_cancelled()) {
                int newsmk = dialog.getSelectedSMK();
                new smOksWriteWorker(newsmk).execute();
            }
        });

        // Prescale sets change buttons
        for(RunState state: RunState.values()) {
            getStandardPanel().btnMap_pskChange.get(state).addActionListener((evt) -> {
                prescaleKeysSelectionAction(state);
            });
        }

        // bunchgroup set change
        getStandardPanel().btn_changeBGS.addActionListener((evt) -> {
            BGSelectionDialogImpl dialog = new BGSelectionDialogImpl(true,triggerPanel.getCurrentBG());
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
            if(!dialog.isCancelled()) {
                int bgsk = dialog.getSelectedBGSK().key;
                new bgWriterWorker(bgsk).execute();
            }
        });

        // LB length change
        getStandardPanel().btn_changeLBLength.addActionListener((evt)-> {
            LumiBlockIntervalDialogImpl dialog = new LumiBlockIntervalDialogImpl(null, true, triggerPanel);
            dialog.setLocationRelativeTo(this);
            dialog.setVisible(true);
        });

        // change DT
        getStandardPanel().btn_changeDT.addActionListener((ActionEvent e)-> {
            CTPDeadtimeDialog dialog = new CTPDeadtimeDialogImpl(null, true);
            showDialogNearMouse(dialog);
            UpdateLabelCTPDeadtimeConfig();
            UpdateLabelColor();
        });

        // bunchgroup set creation
        getStandardPanel().btn_createBGS.addActionListener((evt)-> {
            try {
                createBGSButtonActionPerformed();
            } catch (SQLException ex) {
                Utils.logException(ex);
            }
        });

        // new LB
        getStandardPanel().btn_startNewLB.addActionListener((ActionEvent evt) -> {
            getStandardPanel().btn_startNewLB.setEnabled(false);
            Thread worker = new Thread() {
                @Override
                public void run() {
                    triggerPanel.increaseLB();
                    SwingUtilities.invokeLater(() -> {
                        getStandardPanel().btn_startNewLB.setEnabled(true);
                    });
                }
            };
            worker.start();
        });
        /***** done with button listeners *****/

        if(getAliasPanel() != null) {
            getAliasPanel().setTriggerPanel(tp);
        }
        
        // bind to IS changes
        TPGlobal.IS.CurrentL1Psk.bind(this::UpdateLabelCurrentL1PSK);

        TPGlobal.IS.CurrentHLTPsk.bind(this::UpdateLabelCurrentHLTPSK);

        TPGlobal.IS.CurrentBGSk.bind(this::UpdateLabelCurrentL1BGKey);

        TPGlobal.IS.StandbyL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys( LVL.L1, RunState.STANDBY );
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.PhysicsL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.L1, RunState.PHYSICS);
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.EmittanceL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.L1, RunState.EMITTANCE);
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.StandbyHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.STANDBY);
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.PhysicsHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.PHYSICS);
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.EmittanceHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.EMITTANCE);
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.CurrentLBInt.bind(() -> UpdateLabelLumiInterval());
        TPGlobal.IS.CurrentReady4Physics.bind(() -> UpdatePanelPhysicsReady());
        TPGlobal.IS.CurrentEmittance.bind(() -> UpdatePanelPhysicsReady());
        
    }

    @Override
    protected ShifterPanel createStandardPanel() {
        return new ShifterPanelImpl();
    }

    @Override
    protected ExpertPanel createExpertPanel() {
        return new ExpertPanel();
    }

    /**
     * Called by TriggerPanel during panelInit
     */
    void Initialize() {
        if(getAliasPanel() != null) {
            getAliasPanel().Initialize();
        }
    }
    
    /***** Section with button actions *****/
    public void createBGSButtonActionPerformed() throws SQLException {
        boolean runupdate=true;

        // first check if we are on LHC clock
        String clock = TPGlobal.IS.clockStatusISRec.getCurrentClock();
        if(clock==null) {// IS failed somehow
            String message = "Warning: Unable to determine clock type from IS.\n";
            message += "IS Clock type = null (see Igui log for error)";
            message += "\nDo you wish to continue to upload Bunch Groups?";
            int result = JOptionPane.showConfirmDialog(null, message, "Warning for Clock Type", JOptionPane.YES_NO_OPTION);
            IguiLogger.info("Did not recognise clock type null");
            if(result == JOptionPane.NO_OPTION) {
                runupdate = false;
            }
        } else {
            if(clock.equalsIgnoreCase("internal")) { //internal clock
                IguiLogger.info("On internal clock, blocking bunch group upload");
                TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION,"Using Internal clock, cannot upload new bunch grouPanelState.");
                runupdate=false;
            } else {
                if( ! (clock.equalsIgnoreCase("BC1") || clock.equalsIgnoreCase("BC2")) ) { // unknown clock
                    String message = "Warning: Unable to determine clock type from IS.\n";
                    message += "IS Clock type = ";
                    message += clock;
                    message += "\nDo you wish to continue to upload Bunch Groups?";
                    int result = JOptionPane.showConfirmDialog(null, message, "Warning for Clock Type", JOptionPane.YES_NO_OPTION);
                    IguiLogger.info("Did not recognise clock type " + clock);
                    if(result == JOptionPane.NO_OPTION) {
                        runupdate = false;
                    }
                }
            }//not internal
        }//not null

        if(!runupdate) {
            IguiLogger.info("Not updating bunch groups due to clock setting");
            return;
        }

        BunchGroupUploadDialog dialog = new BunchGroupUploadDialog(getStandardPanel().getParentFrame(this),false);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
    }

    private void prescaleKeysSelectionAction(RunState state) {
        final RunState currentState = GUIState.getCurrentRunState();
        PSSelectionDialogImpl dialog = new PSSelectionDialogImpl(null, true, this.triggerPanel, state, currentState);
        getStandardPanel().addOpenPsDialog(dialog);
        dialog.setLocationRelativeTo(this);
        dialog.setVisible(true);
        getStandardPanel().removeOpenPsDialog(dialog);
        if(dialog.isCancelled()) {
            return;
        }
        // analyse the selection made by the user
        KeyDescriptor newl1  = dialog.selectedL1PS();
        KeyDescriptor newhlt = dialog.selectedHLTPS();
        KeyDescriptor newbgs = dialog.selectedBGS();
        boolean wasBgsSelected = dialog.isCurrentState() && dialog.isBGSSelectionActive();


        if(newl1==null || newhlt==null) {
            IguiLogger.error("One of the prescales is null, not applying");
            return;
        }
        if(wasBgsSelected && newbgs==null) {
            IguiLogger.error("Bunchgroup set selection was active but no bunchgroup was selected, not applying");
            return;
        }

        if(wasBgsSelected) {
            // L1 psk, HLT psk and BGS key were selected
            IguiLogger.info("Sending " + state + " L1 psk " + newl1.key + ", HLT psk " + newhlt.key + " and BGS key " + newbgs.key + " back to the TrigPanel");
            ChangePrescales(state, newl1, newhlt, newbgs);
        } else {
            // only L1 psk and HLT psk were selected 
            IguiLogger.info("Sending " + state + " L1 psk " + newl1.key + " and HLT psk " + newhlt.key + " back to the TrigPanel");
            ChangePrescales(state, newl1, newhlt, null);
        }

    }

    /**
     * Function to change prescales for STANDBY, PHYSICS or EMITTANCE and possibly the bunchgroupset key if provided
     * 
     * @param runState: RunState state for which to do the update (STANDBY, PHYSICS, EMITTANCE)
     * @param newl1: int the L1 prescale key
     * @param newhlt: int the HLT prescale key
     * @param newbgs: int the bunchgroupset key (can be null)
     * 
     * 1) First the prescale keys for the given run state a written to IS (RunParams.<runState>.L1PsKey and RunParams.<state>.HltPsKey)
     * 2) If the current state of ATLAS data taking matches the paramenter runState, then the prescales are also send to the CTP to be used
     *     a) if the ATLAS RC is NONE (<INITIAL) and the update is for STANDBY, then the L1 and HLT PSKs are written to OKS
     *     b) if the ATLAS RC is >=INITIAL then the L1 and HLT prescale keys are send to the CTP
     * 3) If the bunchgroupset key is provided (not null) then it is also send to the CTP (together with the prescales)
     *    This is possible if ATLAS RC is >=INITIAL for operation simplicity 
     */
    public void ChangePrescales(final RunState runState, final KeyDescriptor newl1, final KeyDescriptor newhlt, final KeyDescriptor newbgs) {

        if(TPGlobal.igui.getAccessLevel()!=IguiConstants.AccessControlStatus.CONTROL) {
            IguiLogger.warning("Somehow you are trying to change prescales and bunchgroup without access rights, aborting");
            return;
        }

        // first send the prescale to IS
        new SendPrescalesToISWorker(newl1, newhlt, runState).execute();
        
        // get state of run control (INITIAL, CONNECTED, ...)
        State rcState = TPGlobal.igui.getRCState();

        // update OKS (RDB) when provided keys are STANDBY keys and RC state is <INITIAL
        if( runState==RunState.STANDBY && State.INITIAL.follows(rcState) ) {
            new psOksWriter( newl1.key, newhlt.key).execute();
        }

        // Send the keys to CTP (prescale keys and if provided bunchgroupset key) if RC state >=INITIAL
        if (! State.INITIAL.follows(rcState)) {
            if( runState == GUIState.getCurrentRunState() ) {
                new SendPrescalesToMasterTriggerWorker(newl1, newhlt, newbgs).execute();
            }
        }
    }

    /**
     * Utility to write PS to OKS on another thread
     */
    private class psOksWriter extends SwingWorker<Void, Void> {
        private int l1ps=-1;
        private int hltps=-1;
        public psOksWriter(final int l1ps, final int hltps) {
            super();
            this.l1ps  = l1ps;
            this.hltps = hltps;
        }
        @Override
        public Void doInBackground() {
            TPGlobal.OKS.getL1PSKVar().setValue(l1ps);
            OKS.getHLTPSKVar().setValue(hltps);
            return null;
        }
        @Override
        protected void done() {
            UpdateLabelColor();
        }
        
    }

    class SendPrescalesToISWorker extends SwingWorker<Void, Void> {

        private final KeyDescriptor l1ps;
        private final KeyDescriptor hltps;
        private final RunState readyState;

        public SendPrescalesToISWorker(final KeyDescriptor l1ps, final KeyDescriptor hltps, final RunState readyState) {
            this.l1ps = l1ps;
            this.hltps = hltps;
            this.readyState = readyState;
        }

        @Override
        protected Void doInBackground() throws Exception {
            // always write to IS
            try {
                triggerPanel.ChangePrescaleInIS(LVL.L1, readyState, l1ps);
                triggerPanel.ChangePrescaleInIS(LVL.HLT, readyState, hltps);
            } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                IguiLogger.error("Prescale keys or bunchgroup key can not be written to IS, will not send them in the CTP");
            }
            return null;

        }
    }
    
    class SendPrescalesToMasterTriggerWorker extends SwingWorker<Void,Void> {
        private final KeyDescriptor l1ps;
        private final KeyDescriptor hltps;
        private final KeyDescriptor bgsk;
        public SendPrescalesToMasterTriggerWorker(final KeyDescriptor l1ps, final KeyDescriptor hltps, final KeyDescriptor bgsk) {
            this.l1ps = l1ps;
            this.hltps = hltps;
            this.bgsk = bgsk;
        }
        @Override
        protected Void doInBackground() throws Exception {
            triggerPanel.sendPrescalesToMasterTrigger(l1ps, hltps, bgsk);
            return null;
        }
    }

    private class smOksWriteWorker extends SwingWorker<Void,Void> {
        private final int smk;
        smOksWriteWorker(final int smk) {
            this.smk = smk;
        }
        @Override
        public Void doInBackground() throws SQLException {

            // write to OKS
            OKS.getSMKVar().setValue(smk);

            // update prescales
            TPGlobal.theDbKeysLoader().UpdatePrescales( OKS.getSMKVar().value() );

            // check if keys are still valid for current smk. If not, set to 0 in IS
            if( triggerPanel.getKeyDescriptor( GUIState.getStandbyl1psk(), LVL.L1) == KeyDescriptor.NotValidForSMK ) {
                try {
                    triggerPanel.ChangePrescaleInIS( LVL.L1,  RunState.STANDBY, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset L1 standby keys in IS", ex);
                }
                OKS.getL1PSKVar().setValue(0);
            }
            if( triggerPanel.getKeyDescriptor( GUIState.getStandbyhltpsk(), LVL.HLT) == KeyDescriptor.NotValidForSMK ) {
                try {
                    triggerPanel.ChangePrescaleInIS( LVL.HLT, RunState.STANDBY, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset HLT standby keys in IS", ex);
                }
                OKS.getHLTPSKVar().setValue(0);
            }
            if( triggerPanel.getKeyDescriptor( GUIState.getPhysicsl1psk(), LVL.L1) == KeyDescriptor.NotValidForSMK ) {
                try {
                    triggerPanel.ChangePrescaleInIS( LVL.L1,  RunState.PHYSICS, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset L1 physics keys in IS", ex);
                }
            }
            if( triggerPanel.getKeyDescriptor( GUIState.getPhysicshltpsk(), LVL.HLT) == KeyDescriptor.NotValidForSMK ) {
                try {
                    triggerPanel.ChangePrescaleInIS( LVL.HLT, RunState.PHYSICS, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset HLT physics keys in IS", ex);
                }
            }
            // check if keys are still valid for current smk. If not, set to 0 in IS
            if( triggerPanel.getKeyDescriptor( GUIState.getEmittancel1psk(), LVL.L1) == KeyDescriptor.NotValidForSMK ) {
                try {
                    triggerPanel.ChangePrescaleInIS( LVL.L1,  RunState.EMITTANCE, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset L1 standby keys in IS", ex);
                }
            }
            // check if keys are still valid for current smk. If not, set to 0 in IS
            if (triggerPanel.getKeyDescriptor(GUIState.getEmittancehltpsk(), LVL.HLT) == KeyDescriptor.NotValidForSMK) {
                try {
                    triggerPanel.ChangePrescaleInIS(LVL.HLT, RunState.EMITTANCE, KeyDescriptor.NotValidForSMK);
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("Can't reset HLT standby keys in IS", ex);
                }
            }

            
            return null;
        }

        @Override
        public void done() {
            UpdateLabelColor();
            try {
                this.get();
            } catch(InterruptedException | ExecutionException ex) {
                error(new TriggerPanelIssue("Error when trying to write to OKS", ex));
                Utils.logException(ex);
            }
            // update panel
            UpdateLabelCurrentSMK();
            // triggerPanel.setBusy(false);
        }
    }

    private class bgWriterWorker extends SwingWorker<Void,Void> {
        private final int bgsk;
        bgWriterWorker(final int bgsk) {
            this.bgsk = bgsk;
        }
        @Override
        public Void doInBackground() {
            RunControlFSM.State state = TPGlobal.igui.getRCState();
            // >= INITIAL: send update to L1
            if( ! RunControlFSM.State.INITIAL.follows(state) )  {
                try {
                    TPL.info( "Sending bunch group " + bgsk + " to TriggerCommander.", true );
                    triggerPanel.triggerCommander.setBunchGroup(bgsk);
                    if(state==RunControlFSM.State.RUNNING) {
                        triggerPanel.increaseLB();
                    }
                } catch (CommandExecutionFailed ex) {
                    TPL.error( "Could not send new bunch group " + bgsk + " to CTP \n" + ex.toString(), true );
                }
            }
            // < INITIAL: write to OKS
            if( RunControlFSM.State.INITIAL.follows(state) )  {
                OKS.getBGSKVar().setValue(bgsk); // setting the value automatically writes to RDB server
            }
            return null;
        }

        @Override
        protected void done() {
            RunControlFSM.State state = TPGlobal.igui.getRCState();
            // < INITIAL: write to OKS
            if( RunControlFSM.State.INITIAL.follows(state) )  {
                UpdateLabelCurrentL1BGKey();
                UpdateLabelColor();
            }
        }
    }

    void UpdateLabelsAfterOKSRead() {
        UpdateLabelCurrentSMK();
        UpdateLabelCurrentL1PSK();
        UpdateLabelCurrentHLTPSK();
        UpdateLabelCurrentL1BGKey();
        UpdateLabelCTPDeadtimeConfig();
        UpdateLabelColor();
    }

    /**
     * Change appropriate text when SM key changes
     */
    private void UpdateLabelCurrentSMK() {
        SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
            KeyDescriptor kd;
            @Override
            protected void done() {
                getStandardPanel().lbl_curMenu.setText(kd.name);
                getStandardPanel().lbl_curMenu.setToolTipText( Utils.CreateToolTip(kd.comment) );
                getStandardPanel().lbl_curSmk.setText(""+kd.key);
                UpdateLabelColor();
            }
            @Override
            protected Void doInBackground() throws Exception {
                kd = triggerPanel.getKeyDescriptor(triggerPanel.getCurrentSMK(), LVL.MENU);
                if(kd==null) {
                    kd = new KeyDescriptor(0, "not available", 0, "needs to be set", false);
                }
                return null;
            }
        };
        swingWorker.execute();
    }

    protected void UpdateLabelCurrentL1PSK() {
        SwingUtilities.invokeLater(() -> {
            getStandardPanel().lbl_curL1Psk.setText(""+triggerPanel.getCurrentL1PSK());
            getStandardPanel().lbl_curL1Psk.setToolTipText( triggerPanel.getCurrentL1PSKInfo() );
            UpdateLabelPrescaleErrorMsg();
        });
    }

    protected void UpdateLabelCurrentHLTPSK() {
        SwingUtilities.invokeLater(() -> {
            getStandardPanel().lbl_curHltPsk.setText(""+triggerPanel.getCurrentHLTPSK());
            getStandardPanel().lbl_curHltPsk.setToolTipText( triggerPanel.getCurrentHLTPSKInfo() );
            UpdateLabelPrescaleErrorMsg();
        });
    }

    protected void UpdateLabelCurrentL1BGKey() {
        SwingUtilities.invokeLater(() -> {
            final int currentBGSKey = triggerPanel.getCurrentBG();
            final KeyDescriptor currBgskey = TPGlobal.theDbKeysLoader().getKeyDescriptor(currentBGSKey, LVL.BGS);
            getStandardPanel().lbMap_ctpValues.get("BG").setText(""+currentBGSKey);
            getStandardPanel().lbMap_ctpValues.get("BG").setToolTipText( currBgskey.comment );
        });
    }
    
    protected void UpdateLabelCTPDeadtimeConfig() {
        SwingUtilities.invokeLater(() -> {
            getStandardPanel().lbMap_ctpValues.get("DT").setText(""+triggerPanel.getCurrentDeadtimeConfig());
        });
    }

    protected void UpdateLabelLumiInterval() {
        SwingUtilities.invokeLater(() -> {

            String lbText = "";
            String tooltip = "";
            String infoSource = "";

            if( RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()) ) {
                // <=INITIAL, so we take it from OKS
                final int lblength = OKS.getLBLengthVar().value();
                if(lblength>0) {
                    lbText = lblength + " seconds";
                    tooltip = TPGlobal.IS.CurrentLBInt.infoProvider();
                } else {
                    lbText = "Not in OKS";
                    tooltip = "";
                }
                infoSource = "oks";
            } else {
                // >INITIAL, so we take it from IS
                final int lblength = GUIState.getLBinterval();
                if(lblength>0) {
                    lbText = lblength + " seconds";
                    tooltip = TPGlobal.IS.CurrentLBInt.infoProvider();
                } else {
                    lbText = "Info not found";
                    tooltip = TPGlobal.IS.CurrentLBInt.stateInfo();
                }
                infoSource = "ctp";
            }

            JLabel lb_lbLength = getStandardPanel().lbMap_ctpValues.get("LB");
            lb_lbLength.setText(lbText);
            lb_lbLength.setToolTipText(tooltip);
            ((TitledBorder)getStandardPanel().panel_lbLength.getBorder()).setTitle("LB length (" + infoSource + ")");
        });
    }

    void UpdateLabelColor() {
        if(GUIState.isControl()) {
            IguiLogger.info("Uncommited DB changes:"+( OKS.uncommitedDbChanges()?"TRUE":"FALSE"));
            getStandardPanel().setCommitMessage(OKS.uncommitedDbChanges()?"Please 'Commit & Reload' before leaving this panel":"");
            getStandardPanel().lbl_curSmk.setForeground( OKS.getSMKVar().isCommitted()?black:uncommittedclr);
            getStandardPanel().lbMap_l1psKeys.get(RunState.STANDBY).setForeground( OKS.getL1PSKVar().isCommitted()?black:uncommittedclr);
            getStandardPanel().lbMap_hltpsKeys.get(RunState.STANDBY).setForeground( OKS.getHLTPSKVar().isCommitted()?black:uncommittedclr);
            getStandardPanel().lbMap_ctpValues.get("BG").setForeground( OKS.getBGSKVar().isCommitted()?black:uncommittedclr);
            getStandardPanel().lbMap_ctpValues.get("DT").setForeground( OKS.getCTPDeadtimeVar().isCommitted()?black:uncommittedclr);
        } else {
            getStandardPanel().setCommitMessage("");
            getStandardPanel().lbl_curSmk.setForeground(black);
            getStandardPanel().lbMap_l1psKeys.get(RunState.STANDBY).setForeground(black);
            getStandardPanel().lbMap_hltpsKeys.get(RunState.STANDBY).setForeground(black);
            getStandardPanel().lbMap_ctpValues.get("BG").setForeground(black);
            getStandardPanel().lbMap_ctpValues.get("DT").setForeground(black);
        }
    }


    final class ErrMsg {
        String msg = "";
        int severity = 0;
        public ErrMsg(){}
        public ErrMsg(String msg, int severity){
            this.msg = msg;
            this.severity = severity;
        }
    }

    private void UpdateLabelPrescaleErrorMsg() {
        SwingUtilities.invokeLater(() -> {
            final ErrMsg errorMsg = GetPrescaleErrorMsg();
            if(! errorMsg.msg.isEmpty()) {
                IguiLogger.info(errorMsg.msg);
            }
            JLabel lbl_pskError = getStandardPanel().lbl_pskError; 
            lbl_pskError.setText(errorMsg.msg);
            if(errorMsg.severity==0) {
                lbl_pskError.setForeground(Color.GRAY);
                lbl_pskError.setFont(lbl_pskError.getFont().deriveFont(Font.ITALIC));
            } else if(errorMsg.severity==1) {
                lbl_pskError.setForeground(Color.RED.darker());
                lbl_pskError.setFont(lbl_pskError.getFont().deriveFont(Font.BOLD|Font.ITALIC));

            }
        });
    }

    private ErrMsg GetPrescaleErrorMsg() {

        // if we are not running, don't complain
        if(State.RUNNING.follows(GUIState.currentState)) {
            return new ErrMsg();
        }
        
        KeyDescriptor currentL1psKD = triggerPanel.getKeyDescriptor(triggerPanel.getCurrentL1PSK(), LVL.L1);
        if(currentL1psKD == null) {
            return new ErrMsg();
        }
        
        if(currentL1psKD.name.startsWith("AUTO_")) {
            return new ErrMsg("L1 prescales are auto generated by the AutoPrescaler",0);
        }

        final RunState state = GUIState.getCurrentRunState();
        int l1psk = 0;
        int hltpsk = 0;
        switch(state) {
            case STANDBY:
                l1psk = GUIState.getStandbyl1psk();
                hltpsk = GUIState.getStandbyhltpsk();
                break;
            case EMITTANCE:
                l1psk = GUIState.getEmittancel1psk();
                hltpsk = GUIState.getEmittancehltpsk();
                break;
            case PHYSICS:
                l1psk = GUIState.getPhysicsl1psk();
                hltpsk = GUIState.getPhysicshltpsk();
                break;
        }

        if ((triggerPanel.getCurrentL1PSK() == l1psk) &&
        (triggerPanel.getCurrentHLTPSK() == hltpsk))
        {
            // prescales match
            return new ErrMsg();
        }

        return new ErrMsg("Current prescales unexpected for state " + state, 1);
    }

    void UpdatePrescaleKeys(final LVL level, final RunState readystate) throws SQLException {
        SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
            KeyDescriptor kd;
            @Override
            protected Void doInBackground() throws Exception {
                kd = triggerPanel.getKeyDescriptor(level, readystate);
                return null;
            }
            @Override
            protected void done() {
                getStandardPanel().setKeys(readystate, level, kd);
                UpdateLabelPrescaleErrorMsg();
            }
        };
        swingWorker.execute();
    }

    void UpdatePanelPhysicsReady() {
        SwingUtilities.invokeLater(() -> { // execute im EventDispatchThread
            final RunState currentState = GUIState.getCurrentRunState();
            for(final RunState state : RunState.values()) {
                getStandardPanel().lbMap_runModes.get(state).setForeground( currentState == state ? activemodeclr : inactivemodeclr );
                for(PSSelectionDialog dialog : getStandardPanel().getOpenPsDialogs()) {
                    dialog.notifyOfCurrentState(currentState);
                }
            }
        });
    }
    
    /**
     * Function sets up the status of all the buttons depending
     * on the run state & access control level.
     */
    void UpdatePanelForStateAndAccessControl() {

        final RunControlFSM.State state = GUIState.currentState;
        final IguiConstants.AccessControlStatus a = GUIState.currentAccess;
        
        IguiLogger.info("Updating panel: state="+state.toString()+", access control="+a.toString());

        SwingUtilities.invokeLater(() -> {
            ShifterPanel shifterPanel = getStandardPanel();
            
            if(a==IguiConstants.AccessControlStatus.DISPLAY) {
                
                // DISPLAY
                shifterPanel.btn_changeSMK.setEnabled(false);
                for(RunState runState : RunState.values()) { // loop over STANDBY, PHYSICS, EMITTANCE
                    shifterPanel.btnMap_pskChange.get(runState).setEnabled(false);
                }
                shifterPanel.btn_changeBGS.setEnabled(false);
                shifterPanel.btn_changeLBLength.setEnabled(false);
                shifterPanel.btn_startNewLB.setEnabled(false);
                shifterPanel.btn_createBGS.setEnabled(true);
                shifterPanel.btn_changeDT.setEnabled(false);
            } else {
                
                // CONTROL
                shifterPanel.btn_changeSMK.setEnabled(state == State.NONE );
                for(RunState runState : RunState.values()) { // loop over STANDBY, PHYSICS, EMITTANCE
                    shifterPanel.btnMap_pskChange.get(runState).setEnabled(state==State.NONE || state==State.RUNNING);
                }
                shifterPanel.btn_changeBGS.setEnabled(state==State.NONE || state==State.RUNNING);
                shifterPanel.btn_startNewLB.setEnabled(state.equals(State.RUNNING));
                shifterPanel.btn_createBGS.setEnabled(true);
                shifterPanel.btn_changeDT.setEnabled(state==State.NONE);
                shifterPanel.btn_changeLBLength.setEnabled(! RunControlFSM.State.CONFIGURED.follows(TPGlobal.igui.getRCState()));
            }
            
            if( State.INITIAL.follows(state) ) {  // < INITIAL
                ((TitledBorder)shifterPanel.panel_currentPS.getBorder()).setTitle("Current prescales (oks)");
            }
            else {
                ((TitledBorder)shifterPanel.panel_currentPS.getBorder()).setTitle("Current prescales (ctp)");
            }
            shifterPanel.panel_currentPS.repaint();
            
            UpdateLabelCurrentL1PSK();
            UpdateLabelCurrentHLTPSK();
            UpdateLabelLumiInterval();
            UpdateLabelPrescaleErrorMsg();
            UpdateLabelColor();
        });
        if(getAliasPanel() != null) {
            getAliasPanel().UpdateStateAccessControl();
        }

    }
}
