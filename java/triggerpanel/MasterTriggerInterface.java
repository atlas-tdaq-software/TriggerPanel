package triggerpanel;

import Igui.IguiConstants;
import Igui.IguiLogger;
import TRIGGER.CommandExecutionFailed;
import TRIGGER.TriggerCommander;
import dal.MasterTrigger;
import dal.RunControlApplicationBase;


/**
 * @author stelzer
 * @brief Simplify interaction with CtpController (this is a singleton)
 */
final class MasterTriggerInterface {

    void setMasterTrigger(config.Configuration db) {
        if(masterTrigger != null) {
            return;
        }
        try {
            dal.Partition dalPartition = dal.Partition_Helper.get(db, TPGlobal.igui.getPartition().getName());

            final MasterTrigger mt = dalPartition.get_MasterTrigger();
            if (mt != null) {
                final RunControlApplicationBase rc = mt.get_Controller();
                if (rc != null) {
                    String mtControllerName = rc.UID();
                    IguiLogger.info("The partition Master Trigger is " + mtControllerName);
                    masterTrigger = new TriggerCommander(TPGlobal.igui.getPartition(), mtControllerName);
                } else {
                    IguiLogger.warning("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger");
                }
            } else {
                IguiLogger.warning("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger");
            }
        } catch (final Exception ex) {
            IguiLogger.error("Error looking for the partition Master Trigger: " + ex, ex);
        }
    }

    void increaseLB() {
        int run = Utils.getRunnumber();
        if (run > 0) {
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION, "Calling increaseLumiBlock of trigger commander for run " + run);
            IguiLogger.info("Calling TriggerMaster increaseLumiBlock with run number " + run);
            try {
                masterTrigger.increaseLumiBlock(run);
            } catch (TRIGGER.CommandExecutionFailed ex) {
                final String errMsg = "Failed to execute LB increase: " + ex;
                IguiLogger.error(errMsg, ex);
                TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, errMsg);
            }
        } else {
            IguiLogger.error("Can't increase LB since no run number found");
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, "Cannot increase LB since no run number found");
        }
    }

    boolean setL1Prescales(KeyDescriptor l1psk) {
        try {
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION, "Sending L1 PSK " + l1psk.key + " to MasterTrigger");
            IguiLogger.info("Sending L1 PSK " + l1psk.key + " to MasterTrigger");
            masterTrigger.setL1Prescales(l1psk.key);
        } catch (CommandExecutionFailed ex) {
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, "Error when sending L1 PSK " + l1psk.key + " to MasterTrigger (" + ex.toString() + ")");
            IguiLogger.error("Error when sending L1 PSK " + l1psk.key + " to CTP. (" + ex.toString() + ")");
            return false;
        }
        return true;
    }

    boolean sendBunchGroupSetToTrigger(KeyDescriptor bgsk) {
        try {
            TPL.info( "Sending bunch group " + bgsk + " to MasterTrigger", true );
            masterTrigger.setBunchGroup(bgsk.key);
        } catch (CommandExecutionFailed ex) {
            TPL.error( "Could not send new bunch group " + bgsk + " to CTP \n" + ex.toString(), true );
            return false;
        }
        return true;
    }
    
    static private MasterTriggerInterface theInstance = null;
    
    static void destroy() {
        theInstance = null;
    }
    
    static MasterTriggerInterface instance() {
        if(theInstance == null) {
            theInstance = new MasterTriggerInterface();
        }
        return theInstance;
    }

    private TriggerCommander masterTrigger = null;

    private MasterTriggerInterface() {
        masterTrigger = null;
    }
    
}