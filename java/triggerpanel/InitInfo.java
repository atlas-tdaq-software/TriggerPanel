package triggerpanel;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Hold the login information. Class to contain the init information collected
 * in the InitDialog interface displayed at program start.
 *
 * @author Simon Head
 * @author Thorsten Wengler
 */
public class InitInfo {

    /**
     * Read only access, can view whole configurations only (SMT).
     */
    ///Exit Mode.
    public static final int EXIT = -1;
    
    protected UserMode userMode;
    
    protected DBTechnology technology;

    /**
     *
     */
    public InitInfo() {
        this.technology = DBTechnology.UNKNOWN;
        this.userMode = UserMode.UNKNOWN;
    }

    /**
     * @return the userMode
     */
    public UserMode getUserMode() {
        return userMode;
    }

    /**
     * @param userMode the userMode to set
     */
    public void setUserMode(UserMode userMode) {
        this.userMode = userMode;
    }

    /**
     * @return the technology
     */
    public DBTechnology getTechnology() {
        return technology;
    }

    /**
     * @param technology the technology to set
     */
    public void setTechnology(DBTechnology technology) {
        this.technology = technology;
    }
    
    /**
     * Username.
     */
    protected String userName = "";

    /**
     * Password.
     */
    protected String passWord = "";

    /**
     * URL of the db server
     */
    protected String dbServer = "";  // atonr1.cern.ch ..

    /**
     * URL of the db host, has special meaning for Oracle.
     */

    protected String dbHost = ""; // ATLAS_CONFIG ...
    /**
     * MySQL DB name, or Oracle when schema != user.
     */

    /**
     * Used for MySQL as the database name. Also used for Oracle, when the user
     * name doesn't match the name of the schema.
     */
    protected String schemaName = "";

    ///Is this a good connection?
    protected boolean goodInfo = false;
    ///Password for atonr users.
    protected String atonrPassword = "";
    ///Password for atlr R.
    protected String atlrRPassword = "";
    ///Password for atonr R.
    protected String atonrRPassword = "";
    ///Password for atonr W.
    protected String atonrWPassword = "";
    ///Shift user, needs write access.
    protected boolean isOnline = false;
    protected boolean isOnlineDB = false;
    ///Running mode
    protected boolean isCommandLine;
    ///Minimum DB schema version
    protected int minimumDbSchemaVersion;
    /**
     * Does this DB require the schema to be inserted?
     */
    //protected boolean insertSchema = false;
    /**
     * Not the DB username, but the name of the current user.
     */
    protected String systemUsername = "";
    /**
     * Message Log.
     */
    protected static final Logger logger = Logger.getLogger("TriggerDb");

    @Override
    public String toString() {
        String s = "";
        s += "technology: " + getTechnology() + "\n";
        s += "schema    : " + schemaName + "\n";
        s += "user name : " + userName + "\n";
        s += "server    : " + dbServer + "\n";
        s += "host      : " + dbHost + "\n";
        s += "mode      : " + getUserMode() + "\n";
        s += "dblookup  : " + DBConnections.getDBLookupPath() + "\n";
        s += "dbauth    : " + DBConnections.getDBAuthPath() + "\n";
        s += "online    : " + getOnline() + "\n";
        s += "onlineDB  : " + getOnlineDB() + "\n";
//         s += "pw        : " + getPassWord() + "\n";
//         s += "atlr r    : " + getAtlrRPassword() + "\n";
//         s += "atonr r   : " + getAtonrRPassword() + "\n";
//         s += "atonr w   : " + getAtonrWPassword() + "\n";
        return s;
    }

    /**
     * Set to
     * <code>true</code> if running online TT at P1.
     *
     * @param online
     */
    public void setOnline(boolean online) {
        isOnline = online;
    }

    /**
     * Get the isOnline flag.
     *
     * @return
     * <code>true</code> if running at P1.
     */
    public boolean getOnline() {
        return isOnline;
    }

    ///set and get isonline

    /**
     *
     * @param onlinedb
     */
    public void setOnlineDB(boolean onlinedb) {
        isOnlineDB = onlinedb;
    }

    /**
     *
     * @return
     */
    public boolean getOnlineDB() {
        return isOnlineDB;
    }

    ///Set Good Info.

    /**
     *
     * @param goodInfoInput
     */
    public void setgoodInfo(boolean goodInfoInput) {
        goodInfo = goodInfoInput;
    }

    ///Set the user name for DB logon.

    /**
     *
     * @param userNameInput
     */
    public void setUserName(final String userNameInput) {
        userName = userNameInput;
    }

    ///Set the DB password.

    /**
     *
     * @param passWordInput
     */
    public void setPassWord(final String passWordInput) {
        passWord = passWordInput;
    }

    /**
     * Set the DB technology: Oracle.
     * @param dbTechnologyInput
     */
    public void setTechnologyFromString(final String dbTechnologyInput) {
//        dbTechnology = dbTechnologyInput;
        String tech = dbTechnologyInput.toLowerCase();
        switch (tech) {
            case "oracle":
                technology = DBTechnology.ORACLE;
                break;
            default:
                technology = DBTechnology.UNKNOWN;
                logger.log(Level.SEVERE, "Unknown database technology {0} specified", dbTechnologyInput);
        }
    }

    /**
     *
     * @param host
     */
    public void setdbHost(final String host) {
        dbHost = host;
        setdbServer();
    }

    /**
     * Set the DB server. TODO move this to DBConnections. TODO "-" causes
     * problems in the aliases.
     */
    protected void setdbServer() {
        //if (dbTechnology.toLowerCase().equals("oracle")) {
        if (technology == DBTechnology.ORACLE) {
            dbServer = DBConnections.getDBstring(dbHost);
            if (dbServer == null) {
                logger.log(Level.SEVERE, "Can''t set dbServer for host {0}", dbHost);
                System.exit(-1);
            }
        } else {
            dbServer = dbHost.toLowerCase();
        }
    }

    ///Set the table name for Oracle or MySQL.

    /**
     *
     * @param tablename
     */
    public void setSchemaName(final String tablename) {
        schemaName = tablename;
    }


    protected String[] getConnUserPwFromSvcName(final String svcName) throws Exception {

        DocumentBuilder parser = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setIgnoringComments(true);
            factory.setCoalescing(true);
            factory.setNamespaceAware(false);
            factory.setValidating(false);
            parser = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            //System.out.println("Problems building document parser " + e.getMessage());
            throw new Exception();
        }

        String username = null;
        String password = null;

        logger.log(Level.FINE, "TriggerTool is using authentication on connection {0} to find account name and password", svcName);
        String coral_auth_path = DBConnections.getDBAuthPath();
        ArrayList<File> dbAuthfiles = new ArrayList<>();
        File f = new File(coral_auth_path + "/authentication.xml");
        if (f.exists()) {
            dbAuthfiles.add(f);
        }
        if (dbAuthfiles.isEmpty()) {
            logger.log(Level.SEVERE, "No authentication file, neither locally nor in ''{0}''", coral_auth_path);
            throw new Exception();
        }

        // look in each file (one or two) to find the first
        // occurance of the alias and take the first instance of
        // service (this is also the behavior of CORAL, so we do
        // the same here)
        Element authConn = null;
        Iterator<File> fIt = dbAuthfiles.iterator();
        while (fIt.hasNext()) {
            f = fIt.next();
            try {
                // first get the xml document from the file
                Document document = parser.parse(f);
                // then get all the connections
                NodeList connections = document.getElementsByTagName("connection");
                // and find ours
                for (int i = 0; i < connections.getLength(); i++) {
                    if (!((Element) connections.item(i)).getAttribute("name").equals(svcName)) {
                        continue;
                    }
                    logger.log(Level.FINE, "Found connection for {0}", svcName);
                    authConn = (Element) connections.item(i);
                    NodeList params = authConn.getElementsByTagName("parameter");
                    for (int j = 0; j < params.getLength(); j++) {
                        Element param = (Element) params.item(j);
                        if (param.getAttribute("name").equals("user")) {
                            username = param.getAttribute("value");
                        }
                        if (param.getAttribute("name").equals("password")) {
                            password = param.getAttribute("value");
                        }
                    }
                    if (username == null || password == null) {
                        logger.log(Level.SEVERE, "In authentication file {0}/{1} connection {2} has no user or no password", new Object[]{f.getPath(), f.getName(), svcName});
                        throw new Exception();
                    }
                    break;
                }
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Problems in file {0}/{1}: {2}", new Object[]{f.getPath(), f.getName(), e.getMessage()});
            }
            if (authConn != null) {
                break;
            }
        }
        if (authConn == null) {
            logger.log(Level.SEVERE, "Could not find connection {0} in any of the authentication files!", svcName);
            throw new Exception();
        }
        logger.log(Level.FINE, "Found user ''{0}'' and password.", username);
        String[] retVal = new String[2];
        retVal[0] = username;
        retVal[1] = password;
        return retVal;
    }

    /**
     * Interprets the dbconn string
     *
     * db connection = "ALIAS" (alias can be anything without a ':', use
     * dblookup file) db connection = "type:..." (type has to be oracle
     * db connection = "oracle://ATLAS_CONF/ATLAS_CONF_TRIGGER"
     * (use authentication file) db connection =
     * "oracle://ATLAS_CONF/ATLAS_CONF_TRIGGER;username=ALTLAS_CONF_TRIGGER_R;password=..."
     * (if user or password are omitted they are looked up in the authentication
     * file)
     * @param dbConn
     * @throws java.lang.Exception
     */
    public void setConnectionFromCommandLine(final String dbConn) throws Exception {

        String techno;
        String host = "";
        String schema = "";
        String username = "";
        String password = "";

        String svcName = dbConn;
        if (!dbConn.contains(":")) { // connection name from alias
            svcName = DBConnections.getSvcNameFromDbConn(dbConn);
        }

        techno = svcName.split(":")[0];

        String[] parts = svcName.split(";");
        Pattern p = Pattern.compile("(\\w+)://(\\w+)/(\\w+)");
        Matcher m = p.matcher(parts[0]);
        if (m.find()) {
            techno = m.group(1);
            host = m.group(2);
            schema = m.group(3);
        }
        if (parts.length == 1) {
            // username and password from connection name
            String[] userpw = getConnUserPwFromSvcName(parts[0]);
            username = userpw[0];
            password = userpw[1];
        } else {
            String usernameLabel = "username";
            String pwdLabel = "password";
            String schemaLabel = "schema";
            for (String part : parts) {
                if (part.contains(usernameLabel)) {
                    p = Pattern.compile(usernameLabel + "=(\\w+)");
                    m = p.matcher(part);
                    if (m.find()) {
                        username = m.group(1);
                    }
                } else if (part.contains(pwdLabel)) {
                    p = Pattern.compile(pwdLabel + "=(\\w+)");
                    m = p.matcher(part);
                    if (m.find()) {
                        password = m.group(1);
                    }
                } else if (part.contains(schemaLabel)) {
                    p = Pattern.compile(schemaLabel + "=(\\w+)");
                    m = p.matcher(part);
                    if (m.find()) {
                        schema = m.group(1);
                    }
                }
            }
        }
        
        setTechnologyFromString(techno); // always set technology before setting host!
        setdbHost(host);
        setSchemaName(schema);
        setUserName(username);
        setPassWord(password);

    }

    /**
     *
     * @return
     */
    public String getURL() {
        return getdbServer();
    }

    /**
     * Get the value of good info.
     * @return 
     */
    public boolean getgoodInfo() {
        return goodInfo;
    }


    /**
     * Get the DB user name.
     * @return 
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Get the DB password. Not sure this is a good idea.
     * @return 
     */
    public String getPassWord() {
        return passWord;
    }

    /**
     * Get the DB host without converting to server name!
     * @return 
     */
    public String getdbHost() {
        return dbHost;
    }

    /**
     * Get the DB server. Hard coded for common ones.
     * @return 
     */
    public String getdbServer() {
        return dbServer;
    }

    /**
     * Get the table name.
     * @return 
     */
    public String getSchemaName() {
        return schemaName;
    }

    /**
     * Get the table name.
     * @return 
     */
    public String getSchemaModifier() {
        return this.getSchemaName().toUpperCase() + ".";
    }

    /**
     * Reset all fields to the default settings.
     */
    public void resetAll() {
        userMode = UserMode.UNKNOWN;
        userName = "";
        passWord = "";
        technology = DBTechnology.UNKNOWN;
        dbServer = "";
    }

    /**
     * Set the system user name.
     *
     * @param inp the UserName.
     */
    public void setSystemUsername(final String inp) {
        systemUsername = inp;
    }

    /**
     * Get the system username.
     * @return 
     */
    public String getSystemUsername() {
        return systemUsername;
    }

    /**
     * Get the home directory of the user.
     * @return 
     */
    public static String getSystemHome() {
        return System.getProperty("user.home");
    }

    ///Set the insert schema variable.
    //void insertSchema(boolean b) {
    //    insertSchema = b;
    //}
    ///Get the value of insert schema.
    //boolean getInsertSchema() {
    //    return insertSchema;
    //}
    /**
     * Set the value of atonr user password.
     * @param pass
     */
    public void setOnlineUserPassword(final String pass) {
        atonrPassword = pass;
    }

    /**
     * Get the value of atonr user password. Bad Idea to have this.
     * @return 
     */
    public String getOnlineUserPassword() {
        return atonrPassword;
    }

    /**
     * Set the value of atonr R password.
     * @param pass
     */
    public void setAtlrRPassword(final String pass) {
        atlrRPassword = pass;
    }

    /**
     * Get the value of atonr R password. Bad Idea to have this.
     * @return 
     */
    public String getAtlrRPassword() {
        return atlrRPassword;
    }

    /**
     * Set the value of atonr R password.
     * @param pass
     */
    public void setAtonrRPassword(final String pass) {
        atonrRPassword = pass;
    }

    /**
     * Get the value of atonr R password. Bad Idea to have this.
     * @return 
     */
    public String getAtonrRPassword() {
        return atonrRPassword;
    }

    /**
     * Set the value of atonr W password.
     * @param pass
     */
    public void setAtonrWPassword(final String pass) {
        atonrWPassword = pass;
    }

    /**
     * Get the value of atonr W password. Bad Idea to have this.
     * @return 
     */
    public String getAtonrWPassword() {
        return atonrWPassword;
    }

    /**
     * Encrypt the atonr password and return the result.
     * @return 
     */
    public String encryptAtonrPassword() {
        return encrypt(atonrPassword);
    }

    /**
     * Method to encrypt the atonr password using SHA1.
     *
     * @param str the String to encrypt.
     * @return 
     */
    public String encrypt(final String str) {
        try {
            ////System.out.println("Encoding " + atonrPassword);
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.reset();
            md.update(str.getBytes());
            Encoder encoder = Base64.getEncoder();
            String encoded = encoder.encodeToString(md.digest());
            ////System.out.println("Result " + encoded);
            return encoded;
        } catch (NoSuchAlgorithmException ex) {
            logger.log(Level.SEVERE, "Exception: {0}", ex);
        }
        return null;
    }

    /**
     * Method return true if the system user is an online expert, works online
     * at P1
     *
     * @return 
     */
    public boolean isOnlineExpert() {
        return DBConnections.isOnlineExpert(this.systemUsername);
    }

    /**
     * Set the mode integer from a string.
     *
     * @param level a String representing one of the different levels on the
     * trigger users.
     */
    public void setModeString(final String level) {
        String lvl = level.toUpperCase();
        switch (lvl) {
            case "USER":
                this.userMode = UserMode.USER;
                break;
            case "SHIFTER":
                this.userMode = UserMode.SHIFTER;
                break;
            case "EXPERT":
                this.userMode = UserMode.EXPERT;
                break;
            case "DBA":
                this.userMode = UserMode.DBA;
                break;
            case "L1EXPERT":
                this.userMode = UserMode.L1EXPERT;
                break;
            case "HLTEXPERT":
                this.userMode = UserMode.HLTEXPERT;
                break;
        }
    }
    
    /**
     *
     * @return
     */
    public boolean IsCommandLine()
    {
        return isCommandLine;
    }
    
    /**
     *
     * @param isCommandLine
     */
    public void SetIsCommandLine(boolean isCommandLine)
    {
        this.isCommandLine = isCommandLine;
    }
        
    /**
     *
     * @return
     */
    
    //Caution: The following methods are named using 'Minimun' instead of 'Minimum'
    public int getDbMinimunSchema()
    {
        return minimumDbSchemaVersion;
    }
    
    /**
     *
     * @param schemaVersion
     */
    public void setDbMinimunSchema(int schemaVersion)
    {
        minimumDbSchemaVersion = schemaVersion;
    }
}
