package triggerpanel;

import Igui.IguiLogger;
import TTCInfo.BaseClassReservation;
import TTCInfo.OCLumi;
import TTCInfo.LBSettings;
import TTCInfo.TrigConfL1BgKeyNamed;
import TTCInfo.TrigConfL1PsKeyNamed;
import is.Info;

import java.util.ArrayList;

class TPListenerHandler {

    private static final ArrayList<TPISListener<is.Info>> allListeners = new ArrayList<>();

    final TPISListener<TrigConfL1PsKeyNamed> CurrentL1Psk;
    final TPISListener<TrigConfL1PsKeyNamed> StandbyL1Psk;
    final TPISListener<TrigConfL1PsKeyNamed> PhysicsL1Psk;
    final TPISListener<TrigConfL1PsKeyNamed> EmittanceL1Psk;

    final TPISListener<TrigConfHltPsKeyNamed> CurrentHLTPsk;
    final TPISListener<TrigConfHltPsKeyNamed> StandbyHltPsk;
    final TPISListener<TrigConfHltPsKeyNamed> PhysicsHltPsk;
    final TPISListener<TrigConfHltPsKeyNamed> EmittanceHltPsk;

    final TPISListener<TrigConfL1BgKeyNamed> CurrentBGSk;
    // public final TPISListener<ISCTPCOREBGKeyNamed> ScheduledBGSk;
    final TPISListener<TTCInfo.LBSettings> CurrentLBInt;
    final TPISListener<rc.Ready4PhysicsInfo> CurrentReady4Physics;
    final TPISListener<BooleanInfo> CurrentEmittance;
    final TPISListener<StringInfo> CurrentStandbyAlias;
    final TPISListener<StringInfo> CurrentEmittanceAlias;
    final TPISListener<StringInfo> CurrentPhysicsAlias;
    // final TPISListener<StringInfo> CurrentPhysicsAliasRow;
    final TPISListener<StringInfo> CurrentPhysicsAliasOverrideRow;
    final TPISListener<StringInfo> CurrentSuggestedPhysicsAliasRow;

    final TPISListener<TTCInfo.LumiBlock> RunParamsInfo;
    final ClockStatusISReceiver clockStatusISRec;
    final TPISListener<BaseClassReservation> CtpConfiguration;
    final TPISListener<OCLumi> LuminosityInfo;

    public TPListenerHandler() {

        CurrentL1Psk = new TPISListener<>("RunParams.TrigConfL1PsKey", TrigConfL1PsKeyNamed.class, null);

        StandbyL1Psk = new TPISListener<>("RunParams.Standby.L1PsKey", TrigConfL1PsKeyNamed.class,
                (final TrigConfL1PsKeyNamed l1) -> {
                    GUIState.setStandbyl1psk(l1 == null ? -1 : l1.L1PrescaleKey);
                });
        StandbyHltPsk = new TPISListener<>("RunParams.Standby.HltPsKey", TrigConfHltPsKeyNamed.class,
                (final TrigConfHltPsKeyNamed hlt) -> {
                    GUIState.setStandbyhltpsk( hlt == null ? -1 : hlt.HltPrescaleKey );
                });
        PhysicsL1Psk = new TPISListener<>("RunParams.Physics.L1PsKey", TrigConfL1PsKeyNamed.class,
                (final TrigConfL1PsKeyNamed l1) -> {
                    GUIState.setPhysicsl1psk(l1 == null ? -1 : l1.L1PrescaleKey);
                });
        PhysicsHltPsk = new TPISListener<>("RunParams.Physics.HltPsKey", TrigConfHltPsKeyNamed.class,
                (final TrigConfHltPsKeyNamed hlt) -> {
                    GUIState.setPhysicshltpsk(hlt == null ? -1 : hlt.HltPrescaleKey);
                });
        EmittanceL1Psk = new TPISListener<>("RunParams.Emittance.L1PsKey", TrigConfL1PsKeyNamed.class,
                (final TrigConfL1PsKeyNamed l1) -> {
                    GUIState.setEmittancel1psk(l1 == null ? -1 : l1.L1PrescaleKey);
                });
        EmittanceHltPsk = new TPISListener<>("RunParams.Emittance.HltPsKey", TrigConfHltPsKeyNamed.class,
                (final TrigConfHltPsKeyNamed hlt) -> {
                    GUIState.setEmittancehltpsk(hlt == null ? -1 : hlt.HltPrescaleKey);
                });

        CurrentHLTPsk = new TPISListener<>("RunParams.TrigConfHltPsKey", TrigConfHltPsKeyNamed.class, null);

        CurrentBGSk = new TPISListener<>("RunParams.TrigConfL1BgKey", TrigConfL1BgKeyNamed.class, null);
        // ScheduledBGSk = new TPISListener<> ("L1CT.ISCTPCOREBGKey",
        // ISCTPCOREBGKeyNamed.class, null);

        CurrentLBInt = new TPISListener<>("RunParams.LBSettings", TTCInfo.LBSettings.class,
                (final TTCInfo.LBSettings lbsettings) -> {
                    LBSettings.switchingMode SwitchingMode = lbsettings.SwitchingMode;
                    if (!(SwitchingMode == LBSettings.switchingMode.TIME
                            || SwitchingMode == LBSettings.switchingMode.EVENTS)) {
                        IguiLogger.warning("Did not recognise type " + SwitchingMode + ", cannot diplay lumi info");
                    }
                    GUIState.setLBinterval(lbsettings.Interval);
                });

        CurrentReady4Physics = new TPISListener<>("RunParams.Ready4Physics", rc.Ready4PhysicsInfo.class,
                (final rc.Ready4PhysicsInfo info) -> {
                    Boolean isReadyForPhysics = info.ready4physics;
                    GUIState.setStandbyOrPhysics(isReadyForPhysics ? RunState.PHYSICS : RunState.STANDBY);
                });

        CurrentEmittance = new TPISListener<>("initial", "LHC.EmittanceScanOngoing", BooleanInfo.class,
                (final BooleanInfo info) -> {
                    GUIState.setEmittanceState(info.variable);
                });

        CurrentStandbyAlias = new TPISListener<>("RunParams.CurrentStandbyAlias", StringInfo.class,
                (final StringInfo info) -> {
                });

        CurrentEmittanceAlias = new TPISListener<>("RunParams.CurrentEmittanceAlias", StringInfo.class,
                (final StringInfo info) -> {
                });

        CurrentPhysicsAlias = new TPISListener<>("RunParams.CurrentPhysicsAlias", StringInfo.class,
                (final StringInfo info) -> {
                });

        /*
         * CurrentPhysicsAliasRow = new TPISListener<>
         * ("RunParams.CurrentPhysicsAliasRow", StringInfo.class, (final StringInfo
         * info) -> {
         * });
         */
        CurrentPhysicsAliasOverrideRow = new TPISListener<>("RunParams.CurrentPhysicsAliasOverrideRow",
                StringInfo.class, (final StringInfo info) -> {
                });

        CurrentSuggestedPhysicsAliasRow = new TPISListener<>("RunParams.CurrentPhysicsAliasSuggestedRow",
                StringInfo.class, (final StringInfo info) -> {
                });

        RunParamsInfo = new TPISListener<>("RunParams.LumiBlock", TTCInfo.LumiBlock.class,
                (final TTCInfo.LumiBlock lumiblock) -> {
                    GUIState.lbNumber = lumiblock.LumiBlockNumber;
                    GUIState.runNumber = lumiblock.RunNumber;
                });

        clockStatusISRec = new ClockStatusISReceiver();

        CtpConfiguration = new TPISListener<>("initialL1CT", "L1CT.Reservation", TTCInfo.BaseClassReservation.class,
                null);

        LuminosityInfo = new TPISListener<>("OLC", "OLC.OLCApp/ATLAS_PREFERRED_Inst", OCLumi.class, null);
        // LuminosityInfo = new TPISListener<>
        // ("OLC","OLC.OLCApp.ATLAS_PREFERRED_LBAv_PHYS", OCLumi.class, null );
    }

    static void addListener(TPISListener<Info> listener) {
        allListeners.add(listener);
    }

    /** subscribe to IS */
    public void subscribe(boolean silent) {
        IguiLogger.info("Subscribing to IS");
        for (TPISListener<Info> l : allListeners) {
            l.subscribe(silent);
        }
    }

    /** unsubscribe from IS */
    public void unsubscribe() {
        IguiLogger.info("Unsubscribing from IS");
        for (TPISListener<Info> l : allListeners) {
            l.unsubscribe();
        }
    }

}
