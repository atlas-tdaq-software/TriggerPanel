/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import java.util.ArrayList;

import com.jidesoft.utils.Q;

import HLTPUDal.HLTImplementationDB;
import HLTPUDal.HLTImplementationDB_Helper;
import config.ConfigObject;
import config.NotFoundException;
import config.Query;
import config.SystemException;
import dal.TriggerConfiguration;


final class ConfigHelper {

    private final String partitionName;

    public ConfigHelper(final String partitionName) {
        this.partitionName = partitionName;
    }

    /**********
     *
     * helper functions to traverse the configuration
     *
     * @param rdb
     * @return 
     */

    // helper function to get OKS trigger configuration object
    dal.TriggerConfiguration getTriggerConfiguration(config.Configuration rdb) {

        if (rdb==null) {
            throw new RuntimeException("RDB server is null");
        }
        
        dal.Partition p = null;
        try{
            p = dal.Partition_Helper.get(rdb, this.partitionName);
            if (p==null) {
                TPL.error("Could not retrieve partition from OKS");
                return null;
            }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve partition from OKS" + ex);
            return null;
        }

        dal.TriggerConfiguration trigconf = null;
        try{
            trigconf = p.get_TriggerConfiguration();
            if (trigconf==null) {
                TPL.error("Could not retrieve TriggerConfiguration from OKS");
                return null;
            }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve TriggerConfiguration from OKS" + ex);
        }

        return trigconf;

    }

    // helper function to get OKS CTP deadtime config object
    ConfigObject getCTPDeadtimeConfig(config.Configuration rdb) {

        if (rdb==null) { throw new RuntimeException("RDB server is null"); }
        
        dal.Partition p = null;
        try{
            p = dal.Partition_Helper.get(rdb, this.partitionName);
            if (p==null) {
                TPL.error("Could not retrieve partition from OKS");
                return null;
            }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve partition from OKS" + ex);
            return null;
        }
        
        ConfigObject ctpDeadtimeConfig = null;
        try{
            if(p.get_MasterTrigger().get_Controller().UID().equals("HLTSV")) {
                // The HLTSV does not configure the deadtime
                return null;
            }
            ctpDeadtimeConfig = p.get_MasterTrigger().get_Controller().config_object().get_object("Deadtime").get_object("CtpDeadtimeConfig");
            if (ctpDeadtimeConfig==null) { 
                TPL.error("Could not retrieve CTP Deadtime Configuration from OKS");
                return null;
            }
        }
        catch(config.ConfigException ex) {
            TPL.error("Could not retrieve CTP Deadtime configuration from OKS" + ex);
            return null;
        }

        return ctpDeadtimeConfig;
    }

    // helper function to set OKS CTP configuration object
    Boolean setCTPDeadtimeConfig(config.Configuration rdb, String value) {

        if (rdb==null) { throw new RuntimeException("RDB server is null"); }
        
        try{
            rdb.get_object( "CtpDeadtimeConfigFrame", "CtpDeadtime").set_object("CtpDeadtimeConfig", rdb.get_object("CtpDeadtimeConfig",value)) ;
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve CTP Deadtime Configuration from OKS" + ex);
        }

        return true;

    }

    ArrayList<ConfigObject> getAllTCTPDeadtimeConfigObjects(config.Configuration rdb) {

        if (rdb==null) {
            throw new RuntimeException("RDB server is null");
        }
        
        ArrayList<ConfigObject> ctp_deadtimes = new ArrayList<>();

        ConfigObject[] all_ctp_deadtimes = null;
        try {
            all_ctp_deadtimes = rdb.get_objects("CtpDeadtimeConfig", new Query());
        } catch (SystemException | NotFoundException e) {
            TPL.error("Could not retrieve list of CTP Deadtime Configuration objects (type CtpDeadtimeConfig) from OKS (" + e + ")");
            return ctp_deadtimes;
        }

        for(ConfigObject obj: all_ctp_deadtimes) {
            try {
                if(obj.get_bool("ShifterSelectable")) {
                    ctp_deadtimes.add(obj);
                }
            } catch (SystemException | NotFoundException e) {
                TPL.warning("CtpDeadtimeConfig '" + obj.UID() + "' does not have boolean field 'ShifterSelectable' (" + e + ")");
            }
        }
        return ctp_deadtimes;
    }

    // helper function to get OKS CTP configuration object
    ConfigObject getCTPLBLength(config.Configuration rdb) {

        if (rdb==null) {
            throw new RuntimeException("RDB server is null");
        }
        
        dal.Partition p = null;
        try{
            p = dal.Partition_Helper.get(rdb, this.partitionName);
            if (p==null) {
                TPL.error("Got empty oks partition helper for partition " + this.partitionName);
                return null;
            }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve oks partition helper for partition " + this.partitionName + ":" + ex);
            return null;
        }
        
        ConfigObject ctpLBLengthConfig = null;
        try{
            if(p.get_MasterTrigger().get_Controller().UID().equals("HLTSV")) {
                // The HLTSV does not configure the deadtime
                return null;
            }
            ctpLBLengthConfig = p.get_MasterTrigger().get_Controller().config_object().get_object("LumiBlock");
            if (ctpLBLengthConfig==null) { 
                TPL.error("Could not retrieve CTP LB-length configuration from OKS");
                return null;
            }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve CTP LB-length configuration from OKS:" + ex);
            return null;
        }

        return ctpLBLengthConfig;
    }

    // helper function to get OKS L1 trigger db connectionn object
    dal.L1TriggerConfiguration getCTPConfiguration(config.Configuration rdb) {
    
        dal.TriggerConfiguration trigconf = getTriggerConfiguration(rdb);
        if (trigconf==null) { return null; }

        dal.L1TriggerConfiguration l1trigconf = null;
        try{
            l1trigconf = trigconf.get_l1();
            if (l1trigconf==null) { TPL.error("Could not retrieve L1 TriggerConfiguration from OKS"); return null; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve L1 TriggerConfiguration from OKS" + ex);
        }
    
        return l1trigconf;
    }

    // helper function to get OKS trigger db connectionn object
    dal.TriggerDBConnection getTriggerDBConnection(config.Configuration rdb) {
    
        dal.TriggerConfiguration trigconf = getTriggerConfiguration(rdb);
        if (trigconf==null) { return null; }

        dal.TriggerDBConnection dbconn = null;
        try{
            dbconn = trigconf.get_TriggerDBConnection();
            if (dbconn==null) { TPL.error("Could not retrieve TriggerDBConnection from OKS"); return null; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve TriggerDBConnection from OKS" + ex);
        }

        return dbconn;

    }


    // helper function to get OKS L1 trigger db connectionn object
    dal.L1TriggerConfiguration getL1TriggerConfiguration(config.Configuration rdb) {
    
        dal.TriggerConfiguration trigconf = getTriggerConfiguration(rdb);
        if (trigconf==null) { return null; }

        dal.L1TriggerConfiguration l1trigconf = null;
        try{
            l1trigconf = trigconf.get_l1();
            if (l1trigconf==null) { TPL.error("Could not retrieve L1 TriggerConfiguration from OKS"); return null; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve L1 TriggerConfiguration from OKS" + ex);
        }

        return l1trigconf;
    }

    /**
     *
     * @param rdb the value of rdb
     * @param lvl the value of lvl
     * @return hlt configuration object
     */
    HLTImplementationDB getHLTImplementationDB(config.Configuration rdb, int lvl) {
        TriggerConfiguration trigconf = getTriggerConfiguration(rdb);
        if (trigconf == null) {
            return null;
        }
        HLTImplementationDB dbtrigconf = null;
        try{
            dbtrigconf = HLTImplementationDB_Helper.cast(trigconf.get_hlt());
            if (dbtrigconf == null) { TPL.error("Could not retrieve DB HLT TriggerConfiguration from OKS"); return null; }
        }
        catch(config.ConfigException ex){
            TPL.error("Could not retrieve DB HLT TriggerConfiguration from OKS" + ex);
        }

        return dbtrigconf;
    }
    
}
