package triggerpanel;

public enum DBTechnology {
    ORACLE("oracle"),
    DBLOOKUP("dblookup"),
    UNKNOWN("unknown");
    private final String rep;
    private DBTechnology(final String rep) {
        this.rep = rep;
    }
    @Override public String toString(){
        return rep;
    }
}
