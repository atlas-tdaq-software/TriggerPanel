package triggerpanel;

import TTCInfo.LBSettings;
import TTCInfo.TrigConfL1BunchGroups;
import TTCInfo.TrigConfL1PsKeyNamed;
import is.InfoNotCompatibleException;
import is.NamedInfo;
import is.RepositoryNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


class IS_send {

    private final Scanner in = new Scanner(System.in);

    final ipc.Partition partition;

    final is.Repository repository;

    public IS_send(final String partitionName) {
        partition = new ipc.Partition(partitionName);
        repository = new is.Repository(partition);
    }


    private boolean send() {
        System.out.println("\n  1 - Lumiblock length");
        System.out.println("  2 - Standby prescales");
        System.out.println("  3 - Physics prescales");
        System.out.println("  4 - Go Standby");
        System.out.println("  5 - Go Physics");
        System.out.println("  6 - Bunchgroup Set");
        System.out.println("  7 - Current L1 PSK");
        System.out.println("  0 - Exit");
        int idx = in.nextInt();
        try {
            switch(idx) {
                case 1: sendLBlength(); return true;
                case 2: sendPrescales("Standby"); return true;
                case 3: sendPrescales("Physics"); return true;
                case 4: changeReadyForPhysicsModeIS(false); return true;
                case 5: changeReadyForPhysicsModeIS(true); return true;
                case 6: setBGS(); return true;
                case 7: setL1PSK(); return true;
            }
        } catch (RepositoryNotFoundException ex) {
            Logger.getLogger(IS_send.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InfoNotCompatibleException ex) {
            Logger.getLogger(IS_send.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    private void setBGS() {
        TrigConfL1BunchGroups bgs = new TrigConfL1BunchGroups();
        bgs.BunchGroupSet = "Test_Joerg_01";
        int[] BCID_Group_0 = { 1,2,3,4,5,6,7,8,9 };
        int[] BCID_Group_1 = { 1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010 };
        int[] BCID_Group_2 = { };
        int[] BCID_Group_3 = { 99,100 };
        int[] BCID_Group_4 = { };
        int[] BCID_Group_5 = { };
        int[] BCID_Group_6 = { 17,18,19 };
        int[] BCID_Group_7 = { 3000,3001,3002};
        bgs.BCID_Group_0 = BCID_Group_0;
        bgs.BCID_Group_1 = BCID_Group_1;
        bgs.BCID_Group_2 = BCID_Group_2;
        bgs.BCID_Group_3 = BCID_Group_3;
        bgs.BCID_Group_4 = BCID_Group_4;
        bgs.BCID_Group_5 = BCID_Group_5;
        bgs.BCID_Group_6 = BCID_Group_6;
        bgs.BCID_Group_7 = BCID_Group_7;

        bgs.Name_Group_1 = "Paired";
        bgs.Name_Group_3 = "Empty";
        bgs.Name_Group_7 = "Special";

        try {
            repository.checkin("Monitoring.BunchGrouperApp/BunchGroups", bgs);
        } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
            Logger.getLogger(IS_send.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void sendLBlength() throws RepositoryNotFoundException, InfoNotCompatibleException {
        System.out.println("SwitchingMode:");
        System.out.println("  1 - Time");
        System.out.println("  2 - Events");
        System.out.println("  0 - Cancel");
        int mode = in.nextInt();
        if( mode!=1 && mode!=2 ) return;
        System.out.print("Interval: "); int interval = in.nextInt();
        LBSettings lbs = new LBSettings();
        lbs.SwitchingMode = (mode==1)? LBSettings.switchingMode.TIME : LBSettings.switchingMode.EVENTS;
        //lbs.SwitchingMode = (mode==1)? "TIME":"EVENTS";
        lbs.Interval = interval;
        repository.checkin("RunParams.LBSettings", lbs);
    }


    private void sendPrescales(final String state) throws RepositoryNotFoundException, InfoNotCompatibleException {
        System.out.print("L1 " +state + " prescale: ");  int l1psk = in.nextInt();
        System.out.print("HLT " +state + " prescale: "); int hltpsk = in.nextInt();
        TrigConfL1PsKeyNamed  isl1psk  = new TrigConfL1PsKeyNamed( partition, "RunParams."+state+".L1PsKey"); isl1psk.L1PrescaleKey = l1psk;
        TrigConfHltPsKeyNamed ishltpsk = new TrigConfHltPsKeyNamed( partition, "RunParams."+state+".HltPsKey"); ishltpsk.HltPrescaleKey = hltpsk;
        isl1psk.checkin();
        ishltpsk.checkin();
    }

    private void setL1PSK() throws RepositoryNotFoundException, InfoNotCompatibleException {
        System.out.print("L1 prescale: ");  int l1psk = in.nextInt();
        TrigConfL1PsKeyNamed  isl1psk  = new TrigConfL1PsKeyNamed( partition, "RunParams.TrigConfL1PsKey"); isl1psk.L1PrescaleKey = l1psk;
        isl1psk.checkin();
    }


    class BooleanInfo extends NamedInfo {

        public boolean Ready4Physics;

        public BooleanInfo( ipc.Partition partition, String name ) {
            super( partition, name, "Boolean" );
        }

        @Override
        public void publishGuts( is.Ostream out ){
            super.publishGuts( out );
            out.put( Ready4Physics );
        }

        @Override
        public void refreshGuts( is.Istream in ){
            super.refreshGuts( in );
            Ready4Physics = in.getBoolean(  );
        }
    }

    private void changeReadyForPhysicsModeIS(final Boolean physics) throws RepositoryNotFoundException, InfoNotCompatibleException {
        BooleanInfo bi = new BooleanInfo(partition, "RunParams.Ready4Physics");
        bi.Ready4Physics = physics;
        bi.checkin();
    }



    public static void main(String[] args) {
        String partition = "None";
        String previous = "";
        for ( String arg: args ) {
            if ( previous.equals("-p")) partition = arg;
            previous = arg;
        }
        IS_send iss = new IS_send(partition);
        while(iss.send()) {}
    }

}
