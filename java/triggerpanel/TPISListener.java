package triggerpanel;

import Igui.IguiConstants;
import Igui.IguiConstants.MessageSeverity;
import Igui.IguiLogger;
import is.AlreadySubscribedException;
import is.InfoEvent;
import is.InfoListener;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.Repository;
import is.RepositoryNotFoundException;
import is.SubscriptionNotFoundException;
import is.ValueReadingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.lang.reflect.Constructor;

/**
 * IS receiver for trigger configuration information.
 *
 * @author Joerg Stelzer
 * @param <T> an IS info type
 */
public class TPISListener<T extends is.Info> implements InfoListener {
    private final Class<T> infoType;

    /** IS info name: server.provider */
    protected final String infoname;  // e.g. 'RunParams.RunParams', or 'RunParams.Ready4Physics', or 'RunParams.LuminosityInfo'

    /** IS repository */
    private final Repository isRepository;

    /** Are we subscribed? */
    private boolean subscribed;

    /** State information */
    private String stateinfo;

    /** update function pointer */
    private final UpdateInfo<T> updateinterface;
    
    /** the info object itself */
    private T info = null;

    /** list of functions to be called when IS info is updated */
    private final ArrayList<TPCallable> toCall;
    
    /** the partition */
    private String partition;
    
    /**
     * Constructor.
     * @param infoname
     * @param infoType
     * @param update
     */
    @SuppressWarnings("unchecked")
    public TPISListener(final String infoname, 
                        final Class<T> infoType,
                        final UpdateInfo<T> update) {
        this.toCall = new ArrayList<>();
        
        this.infoType   = infoType; // the class
        this.infoname   = infoname;
        this.subscribed = false;
        this.partition = TPGlobal.igui.getPartition().getName();
        this.isRepository = new Repository( new ipc.Partition( partition ) );
        this.updateinterface = update;
        
        this.info = createInstance();
        // register with the TPListenerHandler
        TPListenerHandler.addListener((TPISListener<is.Info>) this);
    }

    @SuppressWarnings("unchecked")
    public TPISListener(final String partition,
                        final String infoname, 
                        final Class<T> infoType,
                        final UpdateInfo<T> update) {
        this.toCall = new ArrayList<>();
        
        this.infoType   = infoType; // the class
        this.infoname   = infoname;
        this.subscribed = false;
        this.partition = partition;
        this.isRepository = new Repository( new ipc.Partition( partition ) );
        this.updateinterface = update;
        
        this.info = createInstance();
        // register with the TPListenerHandler
        TPListenerHandler.addListener((TPISListener<is.Info>) this);
    }

    void bind(TPCallable c) {
        toCall.add(c);
    }
    
    /**
     * Function to get the current settings from the IS repository
     * @return 
     */
    public final T getinfo() {
        return this.info;
    }

    public boolean isSubscribed() {
        return subscribed;
    }

    public String infoProvider() {
        return infoname;
    }

    public String stateInfo() {
        return stateinfo;
    }

    /**
     * @brief Wraps around the update function 
     */
    private void updatePanel() {
        if(updateinterface == null) {
            return;
        }
        try {
            updateinterface.updateInfo( this.info );
        }
        catch(Exception ex) {
            TPGlobal.igui.internalMessage( MessageSeverity.WARNING, "Could not update panel with  " + infoname + ": " + ex);
        }
    }


    /**
     * @param silent no message is printed when IS warning is received
     * @brief Subscribe to the IS server
     */
    public void subscribe(boolean silent) {
        
        if( !subscribed ) {
            try {
                isRepository.subscribe(infoname,this);
                stateinfo = "Subscribed to " + infoname;
                subscribed = true;
            }
            catch(RepositoryNotFoundException ex) {
                if(! silent) {
                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR,"RepositoryNotFoundException when subscribing to IS service " + infoname);
                }
                stateinfo = "Repository " + infoname + " not found";
                subscribed = false;
            } catch (AlreadySubscribedException ex) {
            }
        }
        // immediately update with current state
        readInfo();
        updatePanel();
        toCall.stream().forEach((c) -> {
            c.call();
        });
    } 


    /**
     * Un-subscribe to the IS server
     */
    public void unsubscribe() {
        try{
            isRepository.unsubscribe(infoname);
        }
        catch(RepositoryNotFoundException | SubscriptionNotFoundException ex) {
            IguiLogger.warning("RepositoryNotFoundException when unsubscribing to IS service " + infoname);
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR,"SubscriptionNotFoundException when unsubscribing to IS service RunParams with subscription LBSettings");
        }
        stateinfo = "Unsubscribed from repository " + infoname;
        subscribed=false;
    }


    /**
     * Function to get the current settings from the IS repository
     * @return the IS info 
     */
    private void readInfo() {
        try {
            T readInfo = this.info;
            isRepository.getValue(infoname, this.info);
            this.info = readInfo; // only assign if successful
            stateinfo = "Info read from repository " + infoname;
            IguiLogger.info("Read IS object " + infoname);
        }
        catch (final RepositoryNotFoundException | InfoNotFoundException | InfoNotCompatibleException ex) {
            stateinfo = "When reading " + infoname + ", exception occured:" + ex;
            IguiLogger.warning(ex.toString() + ": " + infoname, ex);
        }
    }

    /**
     * @param infoevent
     */
    @Override
    public void infoCreated(InfoEvent infoevent) {
        try {
            infoevent.getValue(this.info);
            updatePanel();
            toCall.stream().forEach((TPCallable c) -> {
                c.call();
            });
        }
        catch(final InfoNotCompatibleException | ValueReadingException ex) {
            final String msg = "Failed to update info \"" + infoevent.getName() + "\": " + ex
                + ". Information may not be correctly updated";
            IguiLogger.error(msg, ex);
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
        }
    }


    @Override
    public void infoUpdated(InfoEvent infoevent) {
        IguiLogger.debug("IS object " + this.infoname + " has been updated with " + this.info);
        try {
            infoevent.getValue(this.info);
            updatePanel();
            toCall.stream().forEach((c) -> {
                c.call();
            });
        }
        catch(final InfoNotCompatibleException | ValueReadingException ex) {
            final String msg = "Failed to update info \"" + infoevent.getName() + "\": " + ex
                + ". Information may not be correctly updated";
            IguiLogger.error(msg, ex);
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR, msg);
        }
    }


    @Override
    public void infoDeleted(InfoEvent arg0)
    {}


    /**
     * @brief creates new instance of the info object
     */
    private T createInstance() {
        
        try { // is.Info objects have a default constructor
            final Constructor<T> constr = this.infoType.getConstructor(new Class[] {});
            
            Object[] args = new Object[] {};
            
            this.info = (T)constr.newInstance( args );
            
        }
        catch ( NoSuchMethodException ex ) { // no default constructor - as for named IS info
            
            try {

                final Constructor<T> constr = this.infoType.getConstructor(new Class[] {ipc.Partition.class, String.class});

                Object[] args = new Object[] { new ipc.Partition( this.partition ), this.infoname };
                
                this.info = (T)constr.newInstance( args );

            } 
            catch ( NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex2 ) {
                TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR,"Could not instantiate class for " + infoname + ": " + ex2);
            }

        }
        catch ( SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex ) {
            TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.ERROR,"Could not instantiate class for " + infoname + ": " + ex);
        }

        return this.info;
    }

}
