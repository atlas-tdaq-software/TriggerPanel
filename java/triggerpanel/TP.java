package triggerpanel;

enum LVL {
    L1, HLT, MENU, BGS;
}

enum RunState {
    STANDBY, PHYSICS, EMITTANCE;
}
