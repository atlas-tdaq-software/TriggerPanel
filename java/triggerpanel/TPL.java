package triggerpanel;

import Igui.IguiConstants.MessageSeverity;
import Igui.IguiLogger;


class TPL {

    //private static final TPL instance = new TPL();
/*
    private static Igui igui() {
        return TrigPanelCore.getIgui();
    }

    
    public static void ersInfo(final String msg) {
    ers.Logger.info(new TriggerPanelIssue(msg));
    }
    
    public static void ersWarning(final String msg) {
    ers.Logger.warning(new TriggerPanelIssue(msg));
    }
    
    public static void ersError(final String msg) {
    ers.Logger.error(new TriggerPanelIssue(msg));
    }
    
    public static void ersFatal(final String msg) {
    ers.Logger.fatal(new TriggerPanelIssue(msg));
    }
    
    public static void ersDebug(final String msg, final int n) {
    ers.Logger.debug(n, new TriggerPanelIssue(msg));
    }*/
    static void info(final String msg, final boolean publish) { 
        IguiLogger.info(msg);
        if(publish) {
            TPGlobal.igui.internalMessage(MessageSeverity.INFORMATION, msg);
        } 
    }
    
    static void warning(final String msg, final boolean publish) {
        IguiLogger.warning(msg); 
        if(publish) {
            TPGlobal.igui.internalMessage(MessageSeverity.WARNING, msg);
        }
    }

    static void error(final String msg, final boolean publish) {
        IguiLogger.error(msg);
        if(publish) {
            TPGlobal.igui.internalMessage(MessageSeverity.ERROR, msg);
        }
    }
    
    static void fatal(final String msg, final boolean publish) {
        IguiLogger.error(msg);
        if(publish) {
            TPGlobal.igui.internalMessage(MessageSeverity.FATAL, msg);
        }
    }
    /*
    * 
    */
    static void debug(final String msg, final boolean publish) {
        IguiLogger.debug(msg);
        if(publish) {
            TPGlobal.igui.internalMessage(MessageSeverity.DEBUG, msg);
        }
    }
    
    static void info(final String msg) { 
        info(msg, false);
    }
    
    static void warning(final String msg) {
        warning(msg, true);
    }
    
    static void error(final String msg) {
        error(msg, true);
    }
    
    static void fatal(final String msg) {
        fatal(msg, true);
    }
    
    static void debug(final String msg) {
        debug(msg, false);
    }
    
    private TPL() {}


}
