/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import java.awt.event.*;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.SwingWorker;




public class BGSelectionDialogImpl extends BGSelectionDialog {

    private int currentBGSK = 0;
    private KeyDescriptor selectedBGSK = null;
    private boolean cancelled = false;

    public boolean isCancelled() {
        return cancelled;
    }

    public KeyDescriptor getSelectedBGSK() {
        return selectedBGSK;
    }

    public BGSelectionDialogImpl(boolean modal, final int currentBGSK) {
        super(null, modal);
        this.currentBGSK = currentBGSK;
        lb_current_bgk.setText(Integer.toString(this.currentBGSK));
        (new BGSKLoader()).execute();
        
        btn_apply.addActionListener((evt) -> {
            dispose();
        });

        btn_cancel.addActionListener((evt) -> {
            cancelled = true;
            dispose();
        });
    
        comboL1BunchGroupKey.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                if(evt.getStateChange()==ItemEvent.SELECTED) {
                    selectedBGSK = (KeyDescriptor) evt.getItem();
                    setButtonStatus();
                }
            }
        });
        
        selectedBGSK = (KeyDescriptor) comboL1BunchGroupKey.getSelectedItem();
    
    }

    private class BGSKLoader extends SwingWorker<Void, Void> {

        @Override
        public Void doInBackground() {
            TPGlobal.theDbKeysLoader().UpdateBunchgroups();
            return null;
        }

        @Override
        public void done() {
            fillDropDownBox();
            setVisible(true);
        }
    }
    
    private void setButtonStatus() {
        if( selectedBGSK.key != currentBGSK ) {
            btn_apply.setEnabled(true);
            btn_cancel.setEnabled(true);
        } else {
            btn_apply.setEnabled(false);
            btn_cancel.setEnabled(true);
        }
    }

    @Override
    protected void actionShowHidden(final ActionEvent evt) {
        fillDropDownBox();
    }


    private void fillDropDownBox() {
        fillDropDownBox(comboL1BunchGroupKey, TPGlobal.theDbKeysLoader().GetBunchgroups(), this.currentBGSK, showHidden.isSelected());
    }


    private void fillDropDownBox(final JComboBox<KeyDescriptor> ddBox, final Vector<KeyDescriptor> dbKeys, final int selectedKey, final boolean showHiddenKeys) {
        ddBox.removeAllItems();
        for (KeyDescriptor kd : dbKeys) {
            if (!showHiddenKeys && kd.hidden) {
                continue;
            }
            ddBox.addItem(kd);
            if (kd.key == selectedKey) { // mark current key as selected
                ddBox.setSelectedItem(kd);
            }
        }
    }
    
}
