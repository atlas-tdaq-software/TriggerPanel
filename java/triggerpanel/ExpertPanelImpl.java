/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import static java.lang.Math.round;
import static triggerpanel.TPGlobal.*;
import static triggerpanel.Utils.assertEDT;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import Igui.*;
import is.*;
import triggerpanel.PrescaleSetAliasHandle.AliasComponent;

public final class ExpertPanelImpl extends ExpertPanel {

    final static String NoAliasAvailable = "No alias";

    int currentLumiIndex = -1;
    int suggestedLumiIndex = -1;
    int userSelectedLumiIndex = -1;
    int potentialLumiIndex = -1;
    int lumiBlockCounter = -1;
    String currentPhysicsAlias = "";

    String noMatch = "SELECTED KEYS HAVE NO MATCHING ALIAS";

    private boolean startupInProgress = false;

    TriggerPanel triggerpanel;

    private class ColorRenderer extends DefaultTableCellRenderer {

        Color suggestedAliasColor = Color.YELLOW;
        Color currentAliasColor = Color.BLUE;
        Color overrideAliasColor = Color.RED;

        Color defaultBackgroundColor = null;

        Border unselectedBorder = null;
        Border currentAliasBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                currentAliasColor);
        Border suggestedAliasBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                suggestedAliasColor);
        Border overrideAliasBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                overrideAliasColor);

        public ColorRenderer(boolean isBordered) {
            setOpaque(true); //MUST do this for background to show up.
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus,
                int row, int column) {

            JComponent c = (JComponent) super.getTableCellRendererComponent(table, value,
                    isSelected, hasFocus, row, column);

            if (defaultBackgroundColor == null) {
                defaultBackgroundColor = c.getBackground();
            }

            if (unselectedBorder == null) {
                unselectedBorder = BorderFactory.createMatteBorder(2, 5, 2, 5,
                        table.getBackground());
            }

            //set border current has to take precendence
            if (row == currentLumiIndex) {
                c.setBorder(currentAliasBorder);
            } else if (row == suggestedLumiIndex) {
                c.setBorder(suggestedAliasBorder);
            } else if (row == userSelectedLumiIndex) {
                c.setBorder(overrideAliasBorder);
            } else {
                c.setBorder(unselectedBorder);
            }
            //now set background
            if (row == userSelectedLumiIndex) {
                c.setBackground(overrideAliasColor);
            } else if (row == suggestedLumiIndex) {
                c.setBackground(suggestedAliasColor);
            } else {
                c.setBackground(defaultBackgroundColor);
            }

            //setToolTipText("RGB value: " );
            return c;
        }

    }

    final ColorRenderer cr = new ColorRenderer(true);

    public ExpertPanelImpl() {
        super();

        startupInProgress = true;

        IguiLogger.info("Creating ExpertPanel");

        selectSMKButton.addActionListener((ActionEvent e) -> {
            SMKSelectionDialogImpl dialog = new SMKSelectionDialogImpl(true, OKS.getSMKVar().value());
            dialog.setVisible(true);
            if (dialog.is_cancelled()) {
                return;
            }
            new SMKUpdateWorker(dialog.getSelectedSMK(), false).execute();
        });

        selectBGKButton.addActionListener((ActionEvent e) -> {
            BGSelectionDialogImpl dialog = null;
            if (GUIState.currentState == RunControlFSM.State.RUNNING) {
                dialog = new BGSelectionDialogImpl(true, TPGlobal.IS.CurrentBGSk.getinfo().L1BunchGroupKey);
            } else {
                dialog = new BGSelectionDialogImpl(true, TPGlobal.OKS.getBGSKVar().value());
            }
            dialog.setVisible(true);
            if (dialog.isCancelled()) {
                return;
            }
            int bgsk = dialog.getSelectedBGSK().key;
            new BGKWriterWorker(bgsk).execute();
        });

        createBGSButton.addActionListener((ActionEvent e) -> {
            try {
                triggerpanel.tpgui.createBGSButtonActionPerformed();
            } catch (SQLException ex) {
                Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

        applyPhysicsPrescaleButton.addActionListener((ActionEvent e) -> {

            if (userSelectedLumiIndex != -1) {
                currentLumiIndex = userSelectedLumiIndex;
            } else {
                currentLumiIndex = suggestedLumiIndex;
            }

            DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
            tModel.fireTableDataChanged();

            Integer l1psk = (Integer) tModel.getValueAt(currentLumiIndex, 1);
            Integer hltpsk = (Integer) tModel.getValueAt(currentLumiIndex, 2);
            KeyDescriptor newl1 = new KeyDescriptor(l1psk, "0", 0, "0", false);
            KeyDescriptor newhlt = new KeyDescriptor(hltpsk, "0", 0, "0", false);

            IguiLogger.info("Sending PHYSICS" + " L1PS = " + newl1.key + " HLTPS = " + newhlt.key + " back to the TrigPanel");

            triggerpanel.getTPgui().ChangePrescales(RunState.PHYSICS, newl1, newhlt, null);
        });

        changeStandbyAliasButton.addActionListener((ActionEvent e) -> {

            try {
                AliasSelectionDialogImpl aliasSel = new AliasSelectionDialogImpl(null, true, 
                    RunState.STANDBY, Integer.parseInt(currentSMLabel.getText()), 
                    standbyCurrentAliasValueLabel.getText(), standbyL1PSK.getText(), standbyHLTPSK.getText());
                Point loc = MouseInfo.getPointerInfo().getLocation();
                loc.setLocation(loc.x - 100, loc.y - 100);
                aliasSel.setLocation(loc);
                aliasSel.setVisible(true);

                if (aliasSel.wasCancelled()) {
                    IguiLogger.info("User cancelled alias selection");
                    return;
                }

                KeyDescriptor newl1 = aliasSel.getL1Key();
                KeyDescriptor newhlt = aliasSel.getHLTKey();
                if (newl1 == null || newhlt == null) {
                    IguiLogger.error("One of the prescales is null, not applying");
                    return;
                }

                triggerpanel.getTPgui().ChangePrescales(RunState.STANDBY, newl1, newhlt, null);
            } catch (SQLException ex) {
                TPL.error("SQL exception opening alias selector: " + ex.getMessage(), true);
            }
            try {
                updateAliasLabel("STANDBY");
            } catch (SQLException ex) {
                Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            UpdateCommitted();
        });

        changeEmittanceAliasButton.addActionListener((ActionEvent e) -> {

            try {
                AliasSelectionDialogImpl aliasSel = new AliasSelectionDialogImpl(null, true, 
                    RunState.EMITTANCE, Integer.parseInt(currentSMLabel.getText()),
                    emittanceCurrentAliasValueLabel.getText(), emittanceL1PSK.getText(), emittanceHLTPSK.getText());
                Point loc = MouseInfo.getPointerInfo().getLocation();
                loc.setLocation(loc.x - 100, loc.y - 100);
                aliasSel.setLocation(MouseInfo.getPointerInfo().getLocation());
                aliasSel.setVisible(true);

                if (aliasSel.wasCancelled()) {
                    IguiLogger.info("User cancelled alias selection");
                    return;
                }

                KeyDescriptor newl1 = aliasSel.getL1Key();
                KeyDescriptor newhlt = aliasSel.getHLTKey();
                if (newl1 == null || newhlt == null) {
                    IguiLogger.error("One of the prescales is null, not applying");
                    return;
                }

                IguiLogger.info("Sending EMITTANCE L1PS = " + newl1.key + " HLTPS = " + newhlt.key + " back to the TrigPanel");
                triggerpanel.getTPgui().ChangePrescales(RunState.EMITTANCE, newl1, newhlt, null);
            } catch (SQLException ex) {
                TPL.error("SQL exception opening alias selector: " + ex.getMessage(), true);
            }

            try {
                updateAliasLabel("EMITTANCE");
            } catch (SQLException ex) {
                Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            UpdateCommitted();
        });

        changePhysicsAliasButton.addActionListener((ActionEvent e) -> {

            try {
                AliasSelectionDialogImpl aliasSel = new AliasSelectionDialogImpl(null, true, 
                    RunState.PHYSICS, Integer.parseInt(currentSMLabel.getText()),
                    physicsCurrentAliasValueLabel.getText(), physicsL1PSK.getText(), physicsHLTPSK.getText());
                Point loc = MouseInfo.getPointerInfo().getLocation();
                loc.setLocation(loc.x - 100, loc.y - 100);
                aliasSel.setLocation(MouseInfo.getPointerInfo().getLocation());
                aliasSel.setVisible(true);

                if (aliasSel.wasCancelled()) {
                    IguiLogger.info("User cancelled alias selection");
                    return;
                }

                /*KeyDescriptor newl1 = aliasSel.getL1Key();
                KeyDescriptor newhlt = aliasSel.getHLTKey();
                if (newl1 == null || newhlt == null) {
                    IguiLogger.error("One of the prescales is null, not applying");
                    return;
                }*/
                String newAlias = aliasSel.getAlias();
                if (newAlias == null) {
                    IguiLogger.error("Alias is null, not applying");
                    return;
                }

                physicsCurrentAliasValueLabel.setText(newAlias);
                new PrescaleLumiTableUpdateWorker("PHYSICS", newAlias).execute();
                //IguiLogger.info("Sending PHYSICS L1PS = " + newl1.key + " HLTPS = " + newhlt.key + " back to the TrigPanel");
                //triggerpanel.getPanel().ChangePrescales(RunState.PHYSICS, newl1, newhlt);
            } catch (SQLException ex) {
                TPL.error("SQL exception opening alias selector: " + ex.getMessage(), true);
            }

            UpdateCommitted();
        });

        aliasTable.addMouseListener(new AliasTableMouseListener());

        for (int ci = 0; ci < this.aliasTable.getColumnModel().getColumnCount(); ++ci) {
            this.aliasTable.getColumnModel().getColumn(ci).setCellRenderer(cr);
        }

        lumiLabel.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
            }
        });

        TPGlobal.IS.LuminosityInfo.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                assertEDT();
                Long lumi = round(TPGlobal.IS.LuminosityInfo.getinfo().CalibLumi);
                this.lumiLabel.setText(lumi.toString());
                if ((GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                    suggestedLumiIndex = getLumiIndex(TPGlobal.IS.LuminosityInfo.getinfo().CalibLumi);
                    //this.lumiLabel.setText(Double.toString(TPGlobal.IS.LuminosityInfo.getinfo().RawLumi));
                    //suggestedLumiIndex = getLumiIndex(TPGlobal.IS.LuminosityInfo.getinfo().RawLumi);
                    DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
                    tModel.fireTableDataChanged();
                    writeSuggestedLumiPointToIS();
                }
            });
        });

        TPGlobal.IS.CurrentStandbyAlias.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                assertEDT();
                //if(!(GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)){
                if (TPGlobal.IS.CurrentStandbyAlias.getinfo().variable != null) {
                    standbyCurrentAliasValueLabel.setText(TPGlobal.IS.CurrentStandbyAlias.getinfo().variable);
                }
                //}
            });
        });

        TPGlobal.IS.CurrentEmittanceAlias.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                assertEDT();
                //if(!(GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)){
                if (TPGlobal.IS.CurrentEmittanceAlias.getinfo().variable != null) {
                    emittanceCurrentAliasValueLabel.setText(TPGlobal.IS.CurrentEmittanceAlias.getinfo().variable);
                }
                //}
            });
        });

        TPGlobal.IS.CurrentPhysicsAlias.bind(() -> {
            updatePhysicsAliasFromIS(false);
        });

        TPGlobal.IS.CurrentPhysicsAliasOverrideRow.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                assertEDT();
                //if(!(GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)){
                if (TPGlobal.IS.CurrentPhysicsAliasOverrideRow.getinfo().variable != null) {
                    userSelectedLumiIndex = Integer.parseInt(TPGlobal.IS.CurrentPhysicsAliasOverrideRow.getinfo().variable);
                }
                //writeOverrideToIS();
                ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
                //}
            });
        });

        TPGlobal.IS.CurrentSuggestedPhysicsAliasRow.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                assertEDT();
                if (!(GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                    if (TPGlobal.IS.CurrentSuggestedPhysicsAliasRow.getinfo().variable != null) {
                        suggestedLumiIndex = Integer.parseInt(TPGlobal.IS.CurrentSuggestedPhysicsAliasRow.getinfo().variable);
                    }
                    ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
                }
            });
        });

        TPGlobal.IS.CurrentL1Psk.bind(() -> {
            updateCurrentL1PSK();
        });

        TPGlobal.IS.CurrentHLTPsk.bind(() -> {
            updateCurrentHLTPSK();
        });

        TPGlobal.IS.CurrentBGSk.bind(() -> {
            updateCurrentBGSK();
        });

        TPGlobal.IS.CurrentReady4Physics.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                TPL.info("updatePanelPhysicsReady with ReadyForPhysics: " + GUIState.getStandbyOrPhysics());
                if (GUIState.isInStateEmittance()) {
                    emittanceLabel.setForeground(activemodeclr);
                    standbyLabel.setForeground(inactivemodeclr);
                    physicsLabel.setForeground(inactivemodeclr);
                } else {
                    emittanceLabel.setForeground(inactivemodeclr);
                    standbyLabel.setForeground(GUIState.getStandbyOrPhysics() == RunState.STANDBY ? activemodeclr : inactivemodeclr);
                    physicsLabel.setForeground(GUIState.getStandbyOrPhysics() == RunState.PHYSICS ? activemodeclr : inactivemodeclr);
                }
            });
        });

        TPGlobal.IS.CurrentEmittance.bind(() -> {
            SwingUtilities.invokeLater(() -> {
                TPL.info("updatePanelPhysicsReady with Emittance: " + GUIState.isInStateEmittance());
                if (GUIState.isInStateEmittance()) {
                    emittanceLabel.setForeground(activemodeclr);
                    standbyLabel.setForeground(inactivemodeclr);
                    physicsLabel.setForeground(inactivemodeclr);
                } else {
                    emittanceLabel.setForeground(inactivemodeclr);
                    standbyLabel.setForeground(GUIState.getStandbyOrPhysics() == RunState.STANDBY ? activemodeclr : inactivemodeclr);
                    physicsLabel.setForeground(GUIState.getStandbyOrPhysics() == RunState.PHYSICS ? activemodeclr : inactivemodeclr);
                }
            });
        });

        TPGlobal.IS.StandbyL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.L1, RunState.STANDBY);
                updateAliasLabel("STANDBY");
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.PhysicsL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.L1, RunState.PHYSICS);
                updateCurrentLumiIndex();
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.StandbyHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.STANDBY);
                updateAliasLabel("STANDBY");

            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.PhysicsHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.PHYSICS);
                updateCurrentLumiIndex();
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });

        TPGlobal.IS.EmittanceL1Psk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.L1, RunState.EMITTANCE);
                updateAliasLabel("EMITTANCE");
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });
        TPGlobal.IS.EmittanceHltPsk.bind(() -> {
            try {
                UpdatePrescaleKeys(LVL.HLT, RunState.EMITTANCE);
                updateAliasLabel("EMITTANCE");
            } catch (SQLException ex) {
                IguiLogger.error("", ex);
            }
        });

        TPGlobal.theDbKeysLoader().SetSMK(OKS.getSMKVar().value());
        System.out.println("Setting SMK to " + OKS.getSMKVar().value());
        aliasTable.setToolTipText("<html>Current alias highlighted in <span style=\"background-color: #0000ff\">BLUE</span><br> Suggested alias highlighted in <span style=\"background-color: #ffff00\">YELLOW</span> <br> User override prescale highlighted in <span style=\"background-color: #ff0000\">RED</span><br>");
        String storedSuggestedIndex = readISString("RunParams.CurrentPhysicsAliasSuggestedRow", TPGlobal.igui.getPartition(), "0");
        if (storedSuggestedIndex != null) {
            //System.out.println("suggested lumi index read from IS as " + storedSuggestedIndex);
            suggestedLumiIndex = Integer.parseInt(storedSuggestedIndex);
        } else {
            //System.out.println("current lumi null");
            suggestedLumiIndex = -1;
        }
        updateCurrentLumiIndex();
        ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
        physicsInfoLabel.setText("Selecting 'Apply' will move to lumi-driven alias.");
        try {
            updateAliasLabel("STANDBY");
            updateAliasLabel("EMITTANCE");
        } catch (SQLException ex) {
            Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        physicsL1PSK.setForeground(committedclr);
        physicsHLTPSK.setForeground(committedclr);
        errorLabel.setText("");
        updateCurrentL1PSK();
        updateCurrentHLTPSK();
        updateCurrentBGSK();

        updatePhysicsAliasFromIS(true);
        startupInProgress = false;

    }

    private String readISString(String varToRead, ipc.Partition partition, String defaultValue) {
        StringInfo info = new StringInfo(partition, varToRead);
        try {
            info.checkout();
        } catch (RepositoryNotFoundException | InfoNotFoundException | InfoNotCompatibleException ex) {
            if(defaultValue == null) {
                Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
            } else {
                return defaultValue;
            }
        }
        String result = info.variable;
        return result;
    }

    private void updatePhysicsAliasFromIS(Boolean startup) throws NumberFormatException {
        //                    TPGlobal.igui.internalMessage(IguiConstants.MessageSeverity.INFORMATION,"Updating Physics Alias to " + TPGlobal.IS.CurrentPhysicsAlias.getinfo().variable);
        SwingUtilities.invokeLater(() -> {
            assertEDT();

            if (TPGlobal.IS.CurrentPhysicsAlias.getinfo().variable != null) {
                //System.out.println("Syncing Physics Alias Combo Box to " + TPGlobal.IS.CurrentPhysicsAlias.getinfo().variable);
                String newAlias = TPGlobal.IS.CurrentPhysicsAlias.getinfo().variable;
                System.out.println("newAlias " + newAlias + " stored " + physicsCurrentAliasValueLabel.getText() + " startup? " + startup);
                if (!physicsCurrentAliasValueLabel.getText().equals(newAlias) || startup == true) {
                    System.out.println("Running update worker!");
                    physicsCurrentAliasValueLabel.setText(newAlias);
                    new PrescaleLumiTableUpdateWorker("PHYSICS", newAlias).execute();
                }

            } else {
                String currentAlias = readISString("RunParams.CurrentPhysicsAlias", TPGlobal.igui.getPartition(), "0");
                //System.out.println("Setting Physics Alias Combo Box to " + currentAlias);
                System.out.println("currentAlias " + currentAlias + " stored " + physicsCurrentAliasValueLabel.getText() + " startup? " + startup);
                if (!physicsCurrentAliasValueLabel.getText().equals(currentAlias) || startup == true) {
                    System.out.println("Running update worker123!");
                    physicsCurrentAliasValueLabel.setText(currentAlias);
                    new PrescaleLumiTableUpdateWorker("PHYSICS", currentAlias).execute();
                }
            }
            updateCurrentLumiIndex();
            if (TPGlobal.IS.CurrentPhysicsAliasOverrideRow.getinfo().variable != null) {
                userSelectedLumiIndex = Integer.parseInt(TPGlobal.IS.CurrentPhysicsAliasOverrideRow.getinfo().variable);
            } else {
                String currentOverride = readISString("RunParams.CurrentPhysicsAliasOverrideRow", TPGlobal.igui.getPartition(), "0");
                userSelectedLumiIndex = Integer.parseInt(currentOverride);
            }
            physicsCurrentAliasValueLabel.repaint();
            //writeOverrideToIS();
            ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
        });
    }

    private void updateCurrentBGSK() {
        SwingUtilities.invokeLater(() -> {
            assertEDT();
            this.bgKeyLabel.setText(Integer.toString(triggerpanel.getCurrentBG()));
            this.bgKeyLabel.setToolTipText(triggerpanel.getCurrentBGInfo());
        });
    }

    private void updateCurrentHLTPSK() {
        SwingUtilities.invokeLater(() -> {
            assertEDT();
            hltpsLabel.setText(Integer.toString(triggerpanel.getCurrentHLTPSK()));
            hltpsLabel.setToolTipText(triggerpanel.getCurrentHLTPSKInfo());
            UpdateLabelError();
        });
    }

    private void updateCurrentL1PSK() {
        SwingUtilities.invokeLater(() -> {
            assertEDT();
            this.l1psLabel.setText(Integer.toString(triggerpanel.getCurrentL1PSK()));
            this.l1psLabel.setToolTipText(triggerpanel.getCurrentL1PSKInfo());
            UpdateLabelError();
        });
    }

    /**
     * Called during TriggerPanel.panelInit
     */
    void Initialize() {
        IguiLogger.info("Initializing ExpertPanel");
        UpdateFromOKS();
        UpdateStateAccessControl();
        UpdateCommitted();

        //new SMKUpdateWorker(OKS.getSMKVar().value(), true).execute();
        //new PrescaleLumiTableUpdateWorker("PHYSICS", "").execute();
        //updatePhysicsAliasFromIS(true);
        UpdatePanelForState();
        updateCurrentL1PSK();
        updateCurrentHLTPSK();
        updateCurrentBGSK();
    }

    void setTriggerPanel(TriggerPanel tp) {
        triggerpanel = tp;
    }

    private class BGKWriterWorker extends SwingWorker<Void, Void> {

        private final int bgsk;

        BGKWriterWorker(final int bgsk) {
            this.bgsk = bgsk;
        }

        @Override
        public Void doInBackground() {
            RunControlFSM.State state = TPGlobal.igui.getRCState();
            // >= INITIAL: send update to L1
            if (!RunControlFSM.State.INITIAL.follows(state)) {
                try {
                    TPL.info("Sending bunch group " + bgsk + " to TriggerCommander.", true);
                    triggerpanel.setBGSK(bgsk);
                    if (state == RunControlFSM.State.RUNNING) {
                        triggerpanel.increaseLB();
                    }
                } catch (TRIGGER.CommandExecutionFailed ex) {
                    TPL.error("Could not send new bunch group " + bgsk + " to CTP \n" + ex.toString(), true);
                }
            }
            // < INITIAL: write to OKS
            if (RunControlFSM.State.INITIAL.follows(state)) {
                bgKeyLabel.setText(Integer.toString(bgsk));
                OKS.getBGSKVar().setValue(bgsk); // setting the value automatically writes to RDB server
                UpdateCommitted();

                //UpdateLabelColor();
                //UpdateLabelCurrentL1BGKey();
            }
            return null;
        }

    }

    void updateCurrentLumiIndex() {
        if (!physicsL1PSK.getText().equals("PSK") && (!physicsHLTPSK.getText().equals("PSK"))) {
            currentLumiIndex = getPSIndex(Integer.parseInt(physicsL1PSK.getText()), Integer.parseInt(physicsHLTPSK.getText()));
            DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
            tModel.fireTableDataChanged();
        }
    }

    void UpdatePrescaleKeys(final LVL level, final RunState runstate) throws SQLException {
        SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
            KeyDescriptor kd;

            @Override
            protected Void doInBackground() throws Exception {
                kd = triggerpanel.getKeyDescriptor(level, runstate);
                return null;
            }

            @Override
            protected void done() {
                setKeys(runstate, level, kd);
                UpdateLabelError();
                if (runstate == RunState.STANDBY) {
                    try {
                        updateAliasLabel("STANDBY");
                    } catch (SQLException ex) {
                        Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (runstate == RunState.EMITTANCE) {
                    try {
                        updateAliasLabel("EMITTANCE");
                    } catch (SQLException ex) {
                        Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    updateCurrentLumiIndex();
                }
            }
        };
        swingWorker.execute();
    }

    void setKeys(RunState readystate, LVL level, KeyDescriptor kd) {
        if (readystate == RunState.STANDBY) {
            if (level == LVL.L1) {
                standbyL1PSK.setText((kd.key >= 0) ? "" + kd.key : "");
                standbyL1PSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            } else {
                standbyHLTPSK.setText((kd.key >= 0) ? "" + kd.key : "");
                standbyHLTPSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            }
        } else if (readystate == RunState.EMITTANCE) {
            if (level == LVL.L1) {
                //System.out.println("Setting displayed emittance L1 key to " + ((kd.key>=0)?""+kd.key:""));
                emittanceL1PSK.setText((kd.key >= 0) ? "" + kd.key : "");
                emittanceL1PSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            } else {
                //System.out.println("Setting displayed emittance HLT key to " + ((kd.key>=0)?""+kd.key:""));
                emittanceHLTPSK.setText((kd.key >= 0) ? "" + kd.key : "");
                emittanceHLTPSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            }
        } else {
            if (level == LVL.L1) {
                physicsL1PSK.setText((kd.key >= 0) ? "" + kd.key : "");
                physicsL1PSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            } else {
                physicsHLTPSK.setText((kd.key >= 0) ? "" + kd.key : "");
                physicsHLTPSK.setToolTipText(Utils.CreateToolTip(" Name = " + kd.name/* + ": " + kd.comment*/));
            }
        }

    }

    private void UpdateLabelError() {
        /// Change text - should be done in EDT
        final String errormsg = GetUpdatedLabelError();
        SwingUtilities.invokeLater(() -> {
            errorLabel.setText(errormsg);
        });
    }

    private String GetUpdatedLabelError() {
        // if we are not running, don't complain
        String msg = "";

        if (RunControlFSM.State.RUNNING.follows(GUIState.currentState)) {
            return msg;
        }

        KeyDescriptor currentL1psKD = triggerpanel.getKeyDescriptor(triggerpanel.getCurrentL1PSK(), LVL.L1);
        if (currentL1psKD == null) {
            return msg;
        }
        if (currentL1psKD.name.startsWith("AUTO_")) {
            msg = "<html>L1 prescale is auto generated</br> (hot items)</html>";
        } else {
            int error = 0;
            if (GUIState.isInStateEmittance()) {
                if (!l1psLabel.getText().equals(emittanceL1PSK.getText())) {
                    error += 1;
                }
                if (!hltpsLabel.getText().equals(emittanceHLTPSK.getText())) {
                    error += 2;
                }
            } else if (GUIState.getStandbyOrPhysics() == RunState.PHYSICS) {
                if (!l1psLabel.getText().equals(physicsL1PSK.getText())) {
                    error += 1;
                }
                if (!hltpsLabel.getText().equals(physicsHLTPSK.getText())) {
                    error += 2;
                }
            } else {
                if (!l1psLabel.getText().equals(standbyL1PSK.getText())) {
                    error += 1;
                }
                if (!hltpsLabel.getText().equals(standbyHLTPSK.getText())) {
                    error += 2;
                }
            }
            if (error == 0) {
                IguiLogger.info("Current keys are consistent with state");
                return "";
            }
            if (GUIState.isInStateEmittance()) {
                msg = "<html>Current prescales </br> do not match current state EMITTANCE </html>";
            } else {
                msg = "<html>Current prescales </br> do not match current state " + GUIState.getStandbyOrPhysics() + " </html>";
            }
        }
        IguiLogger.info(msg);
        return msg;
    }

    void UpdateFromOKS() {
        int smkVar = OKS.getSMKVar().value();
        currentSMLabel.setText(Integer.toString(smkVar));

        int l1pskVar = OKS.getL1PSKVar().value();
        l1psLabel.setText(Integer.toString(l1pskVar));

        int hltpskVar = OKS.getHLTPSKVar().value();
        hltpsLabel.setText(Integer.toString(hltpskVar));

        int bgkVar = OKS.getBGSKVar().value();
        bgKeyLabel.setText(Integer.toString(bgkVar));
    }

    void UpdateCommitted() {
        final boolean SMKcommitted = OKS.getSMKVar().isCommitted();
        final boolean L1PSKcommitted = OKS.getL1PSKVar().isCommitted();
        final boolean HLTPSKcommitted = OKS.getHLTPSKVar().isCommitted();
        final boolean BGSKcommitted = OKS.getBGSKVar().isCommitted();

        final boolean uncommitted = !(SMKcommitted && L1PSKcommitted && BGSKcommitted && HLTPSKcommitted);

        SwingUtilities.invokeLater(() -> {
            currentSMLabel.setForeground(SMKcommitted ? black : uncommittedclr);
            standbyL1PSK.setForeground(L1PSKcommitted ? black : uncommittedclr);
            standbyHLTPSK.setForeground(HLTPSKcommitted ? black : uncommittedclr);
            bgKeyLabel.setForeground(BGSKcommitted ? black : uncommittedclr);
            if (!L1PSKcommitted || !HLTPSKcommitted) {
                standbyCurrentAliasValueLabel.setForeground(uncommittedclr);
            } else {
                standbyCurrentAliasValueLabel.setForeground(black);
            }
            commitMsg.setVisible(uncommitted);
            try {
                updateAliasLabel("STANDBY");
                updateAliasLabel("EMITTANCE");
            } catch (SQLException ex) {
                Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

        });
    }
    
    @Override
    void UpdateStateAccessControl() {
        SwingUtilities.invokeLater(() -> {
            boolean controlEnabled = (GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL);
            boolean OKSCommitAllowed = !GUIState.currentState.follows(RunControlFSM.State.NONE);
            //boolean isConfigured = GUIState.currentState.follows(RunControlFSM.State.RUNNING);
            boolean isNone = GUIState.currentState.equals(RunControlFSM.State.NONE);
            boolean isRunning = GUIState.currentState.equals(RunControlFSM.State.RUNNING);

            selectSMKButton.setEnabled(controlEnabled && OKSCommitAllowed);
            applyPhysicsPrescaleButton.setEnabled(controlEnabled && (isNone || isRunning));
            changeStandbyAliasButton.setEnabled(controlEnabled && (isNone || isRunning));
            changeEmittanceAliasButton.setEnabled(controlEnabled && (isNone || isRunning));
            changePhysicsAliasButton.setEnabled(controlEnabled && (isNone || isRunning));
            selectBGKButton.setEnabled(controlEnabled && (isNone || isRunning));
            createBGSButton.setEnabled(!controlEnabled);

        });
    }

    void updateAliasLabel(String runMode) throws SQLException {
        if (currentSMLabel.getText().equals("SMK") || currentSMLabel.getText().equals("0")) {
            return;
        }
        TPGlobal.theDbKeysLoader().SetSMK(Integer.parseInt(currentSMLabel.getText()));
        PrescaleSetAliasHandle aliasHandle = TPGlobal.theDbKeysLoader().GetAliases();
        if (aliasHandle == null) {
            IguiLogger.error("Aliases don't exist");
            return;
        }
        if (runMode.equals("STANDBY")) {
            if (standbyL1PSK.getText().equals("PSK") || standbyHLTPSK.getText().equals("PSK")) {
                return;
            }
        } else if (runMode.equals("EMITTANCE")) {
            if (emittanceL1PSK.getText().equals("PSK") || emittanceHLTPSK.getText().equals("PSK")) {
                return;
            }
        }
        Boolean changed = false;

        for (AliasComponent comp : aliasHandle.getAliasComponents()) {
            if (runMode.equals("STANDBY")) {
                if (comp.getType().equals("STANDBY") || comp.getType().equals("COSMIC") || comp.getType().equals("OTHER")) {
                    if (Integer.parseInt(standbyL1PSK.getText()) == comp.getLumis().get(0).l1psk && Integer.parseInt(standbyHLTPSK.getText()) == comp.getLumis().get(0).hltpsk) {
                        standbyCurrentAliasValueLabel.setText(comp.getName());
                        StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentStandbyAlias");
                        info.variable = comp.getName();
                        try {
                            info.checkin();
                        } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                            Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        changed = true;
                        break;
                    }
                }
            } else if (comp.getType().equals("EMITTANCE")) {
                if (Integer.parseInt(emittanceL1PSK.getText()) == comp.getLumis().get(0).l1psk && Integer.parseInt(emittanceHLTPSK.getText()) == comp.getLumis().get(0).hltpsk) {
                    emittanceCurrentAliasValueLabel.setText(comp.getName());
                    StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentEmittanceAlias");
                    info.variable = comp.getName();
                    try {
                        info.checkin();
                    } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                        Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    changed = true;
                    break;
                }
            }
        }

        if (!changed) {
            if (runMode.equals("STANDBY")) {
                standbyCurrentAliasValueLabel.setText(noMatch);
                StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentStandbyAlias");
                info.variable = noMatch;
                try {
                    info.checkin();
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (runMode.equals("EMITTANCE")) {
                emittanceCurrentAliasValueLabel.setText(noMatch);
                StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentEmittanceAlias");
                info.variable = noMatch;
                try {
                    info.checkin();
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public final void UpdatePanelForState() {

        final RunControlFSM.State state = GUIState.currentState;
        final IguiConstants.AccessControlStatus a = GUIState.currentAccess;

        IguiLogger.info("Updating panel: state=" + state.toString() + ", access control=" + a.toString());

        if (RunControlFSM.State.INITIAL.follows(state)) {  // < INITIAL
            ((TitledBorder) prescalePanel.getBorder()).setTitle("Current Prescales (oks)");
        } else {
            ((TitledBorder) prescalePanel.getBorder()).setTitle("Current Prescales (ctp)");
        }
        prescalePanel.updateUI();
        updateCurrentBGSK();
        updateCurrentL1PSK();
        updateCurrentHLTPSK();
        UpdateStateAccessControl();
    }

    private class SMKUpdateWorker extends javax.swing.SwingWorker<Void, Void> {

        private final int smk;
        private final Boolean read;

        SMKUpdateWorker(final int smk, Boolean read) {
            super();
            this.smk = smk;
            this.read = read;
        }

        @Override
        public Void doInBackground() throws SQLException {

            // write to OKS
            if (!read) {
                TPGlobal.OKS.getSMKVar().setValue(smk);
            }
            // KeyDescriptor kdSMK = TPGlobal.getDbKeysLoader().getKeyDescriptor(smk,
            // LVL.MENU);

            // update prescales
            TPGlobal.theDbKeysLoader().UpdateL1Prescales(OKS.getSMKVar().value());
            TPGlobal.theDbKeysLoader().UpdateHLTPrescales(OKS.getSMKVar().value());

            if (!read) {
                // check if l1 psk is still valid for current smk. If not, set to 0
                final int l1psk = OKS.getL1PSKVar().value();
                KeyDescriptor kdL1PSK = TPGlobal.theDbKeysLoader().getKeyDescriptor(l1psk, LVL.L1);
                if (kdL1PSK == KeyDescriptor.NotValidForSMK) {
                    OKS.getL1PSKVar().setValue(0);
                }

                // check if hlt psk is still valid for current smk. If not, set to 0
                final int hltpsk = OKS.getHLTPSKVar().value();
                KeyDescriptor kdHLTPSK = TPGlobal.theDbKeysLoader().getKeyDescriptor(hltpsk, LVL.HLT);
                if (kdHLTPSK == KeyDescriptor.NotValidForSMK) {
                    OKS.getHLTPSKVar().setValue(0);
                }
            }
            UpdateCommitted();
            return null;
        }

        @Override
        public void done() {
            IguiLogger.info("SMKVar OKS " + OKS.getSMKVar().oksvalue() + " RDB " + OKS.getSMKVar().rdbvalue());
            // update panel
            UpdateFromOKS();
            UpdateCommitted();

            System.out.println("Clearing aliases");
            physicsCurrentAliasValueLabel.setText("NO ALIAS SELECTED");
            new PrescaleLumiTableUpdateWorker("PHYSICS", "").execute();

        }
    }

    private class PrescaleLumiTableUpdateWorker extends SwingWorker<Void, Void> {

        String type = "";
        String name = "";
        AliasComponent aliasComp = null;

        public PrescaleLumiTableUpdateWorker(final String type, final String name) {
            this.type = type;
            this.name = name;
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                System.out.println("Updating aliases to " + this.name);
                if (!this.name.equals("")) {
                    this.aliasComp = TPGlobal.theDbKeysLoader().GetAliases().getAliasComponent(this.type, this.name);
                }
            } catch (SQLException ex) {
                IguiLogger.error("Could not load prescale set alias information", ex);
            }
            return null;
        }

        @Override
        protected void done() {

            DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
            tModel.setRowCount(0);

            if (this.aliasComp == null) {
                //if( ! this.name.equals(NoAliasAvailable) ) {
                IguiLogger.info("No alias component available for type " + this.type + " and name " + this.name);
                //}
            } else {
                IguiLogger.info("Now will update the lumi table with\n" + this.aliasComp.toString());

                this.aliasComp.getLumis().stream().forEach(lr -> {
                    Object[] entry = {Double.toString(round(lr.lumiMin / 1e30)) + " -- " + Double.toString(round(lr.lumiMax / 1e30)), lr.l1psk, lr.hltpsk, lr.comment};
                    tModel.addRow(entry);
                });
                if ((GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                    suggestedLumiIndex = getLumiIndex(TPGlobal.IS.LuminosityInfo.getinfo().CalibLumi);
                    writeSuggestedLumiPointToIS();
                }
                if (!startupInProgress) {
                    if ((GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                        userSelectedLumiIndex = -1;
                        writeOverrideToIS();
                    }
                }
                physicsInfoLabel.setText("Selecting 'Apply' will move to suggested lumi row.");
                updateCurrentLumiIndex();
                tModel.fireTableDataChanged();
            }

            if ((GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentPhysicsAlias");
                info.variable = name;
                try {
                    info.checkin();
                } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private Integer getPSIndex(int l1psk, int hltpsk) {
        DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
        int selectedRow = -1;
        for (int row = 0; row < tModel.getRowCount(); row++) {
            int l1candidate = (Integer) tModel.getValueAt(row, 1);
            int hltcandidate = (Integer) tModel.getValueAt(row, 2);
            if (l1psk == l1candidate && hltpsk == hltcandidate) {
                selectedRow = row;
                break;
            }
        }
        return selectedRow;
    }

    private Integer getLumiIndex(Double lumi) {
        //System.out.println("getLumiIndex: Starting with lumi value " + lumi + " startup mode? " + startupInProgress);
        DefaultTableModel tModel = (DefaultTableModel) aliasTable.getModel();
        int index = -1;
        Double lowestLumi = 1000000.0;
        Double highestLumi = 0.0;
        for (int row = 0; row < tModel.getRowCount(); row++) {
            //System.out.println("getLumiIndex: Checking Lumi Row " + row);
            String text = tModel.getValueAt(row, 3).toString().toLowerCase();
            if (GUIState.getStandbyOrPhysics() == RunState.STANDBY || GUIState.isInStateEmittance()) {
                if (text.contains("default")) {
                    //System.out.println("getLumiIndex: Returning Default : " + row);
                    return row;
                }
                else{
                    continue;
                }
            }

            String entry = tModel.getValueAt(row, 0).toString();
            String[] data = entry.split("--");
            if (data.length != 2) {
                TPL.warning("Unable to extract lumi string to populate alias table", true);
                //System.out.println("getLumiIndex: Unable to extract lumi string");
                break;
            }
            Double lowlumi = Double.parseDouble(data[0]);///1e30;
            if (lowlumi < lowestLumi) {
                lowestLumi = lowlumi;
            }
            Double highlumi = Double.parseDouble(data[1]);///1e30;
            if (highlumi > highestLumi) {
                highestLumi = highlumi;
            }
            //System.out.println("getLumiIndex: LowLumi : " + lowlumi + " LowestLumi: " + lowestLumi + " HighLumi " + highlumi + " HighestLumi " + highestLumi);

            if (lowlumi <= lumi && lumi <= highlumi) {
                //System.out.println("setting index to " + row);
                index = row;
                break;
            }
        }

        //System.out.println("getLumiIndex: Calculated Index " + index);
        if (GUIState.getStandbyOrPhysics() == RunState.STANDBY || GUIState.isInStateEmittance()) {
            //System.out.println("getLumiIndex: Returning as standby");
            return 0;
        }

        if (index == -1) {
            if (lumi > highestLumi) {
                //System.out.println("getLumiIndex: lumi gt highest lumi");
                index = 0;
            } else if (lumi < lowestLumi) {
                //System.out.println("getLumiIndex: lumi lt lowest lumi");
                index = tModel.getRowCount() - 1;
            }

        }
        //System.out.println("getLumiIndex: Index after check " + index + " potential index " + potentialLumiIndex + " lb number " + GUIState.lbNumber + " comparator " + (lumiBlockCounter + 2));

        if ((index == potentialLumiIndex && GUIState.lbNumber >= lumiBlockCounter + 1) || startupInProgress) {
            /*if(startupInProgress){
                System.out.println("Startup bypass: " + startupInProgress);
            }
            System.out.println("getLumiIndex: Found new lumi point, index " + index + " lb " + GUIState.lbNumber + " lbcounter " + lumiBlockCounter);
             */
            TPL.info("Found new recommended lumi point", true);
            potentialLumiIndex = -1;
            lumiBlockCounter = -1;
            return index;
        } else {
            if (potentialLumiIndex != index) {
                potentialLumiIndex = index;
                lumiBlockCounter = GUIState.lbNumber;
                //System.out.println("getLumiIndex: Potential new lumi range, index " + index + " lb " + GUIState.lbNumber + " current index " + currentLumiIndex + " suggested index " + suggestedLumiIndex);
            }/*
            else{
                System.out.println("getLumiIndex: Still in same potential range, index " + index + " lb " + GUIState.lbNumber + " current index " + currentLumiIndex + " suggested index " + suggestedLumiIndex);
            }*/
            return suggestedLumiIndex;
        }
    }

    private void writeOverrideToIS() {
        StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentPhysicsAliasOverrideRow");
        info.variable = Integer.toString(userSelectedLumiIndex);
        try {
            info.checkin();
        } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
            Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void writeSuggestedLumiPointToIS() {
        StringInfo info = new StringInfo(TPGlobal.igui.getPartition(), "RunParams.CurrentPhysicsAliasSuggestedRow");
        info.variable = Integer.toString(suggestedLumiIndex);
        try {
            info.checkin();
        } catch (RepositoryNotFoundException | InfoNotCompatibleException ex) {
            Logger.getLogger(ExpertPanelImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    class AliasTableMouseListener extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            if (!(GUIState.currentAccess == IguiConstants.AccessControlStatus.CONTROL)) {
                return;
            }
            if (SwingUtilities.isRightMouseButton(evt)) {

                final JPopupMenu popupMenu = new JPopupMenu();
                JMenuItem selectItem = new JMenuItem("Override And Select This Row");
                JMenuItem resetItem = new JMenuItem("Reset Override");
                popupMenu.add(selectItem);
                popupMenu.add(resetItem);
                selectItem.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        System.out.println("Setting override " + aliasTable.rowAtPoint(evt.getPoint()));
                        userSelectedLumiIndex = aliasTable.rowAtPoint(evt.getPoint());
                        writeOverrideToIS();
                        physicsInfoLabel.setText("Selecting 'Apply' will change to user override row.");
                        popupMenu.setVisible(false);
                        ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
                    }
                });
                resetItem.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent e) {
                        userSelectedLumiIndex = -1;
                        writeOverrideToIS();
                        physicsInfoLabel.setText("Selecting 'Apply' will change to lumi-driven alias.");
                        popupMenu.setVisible(false);
                        ((DefaultTableModel) aliasTable.getModel()).fireTableDataChanged();
                    }
                });
                popupMenu.show(aliasTable, evt.getPoint().x, evt.getPoint().y);
            }
        }
    }

}
