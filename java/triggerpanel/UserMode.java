/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

/**
 * Try to move to enumerations to avoid string comparison and upper/lower case issues
 */
public enum UserMode {

    /**
     *
     */
    UNKNOWN("Unknown"),

    /**
     *
     */
    EXIT("EXIT"),

    /**
     *
     */
    USER("User"),

    /**
     *
     */
    SHIFTER("Shifter"),

    /**
     *
     */
    L1EXPERT("L1 Expert"),

    /**
     *
     */
    HLTEXPERT("HLT Expert"),

    /**
     *
     */
    EXPERT("Expert"),

    /**
     *
     */
    DBA("Database Admin"),

    /**
     *
     */
    TTDEVELOPER("TriggerTool Developer");
    
    final private String representation;
    
    private UserMode(final String representation) {
        this.representation = representation;
    }

    @Override
    public String toString() {
        return this.representation;
    }
    
}
