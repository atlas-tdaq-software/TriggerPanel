/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package triggerpanel;

import Igui.IguiLogger;
import TTCInfo.RF2TTC_DCS;
import is.AlreadySubscribedException;
import is.InfoEvent;
import is.InfoListener;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.InvalidCriteriaException;
import is.Repository;
import is.RepositoryNotFoundException;
import is.SubscriptionNotFoundException;
import java.util.regex.Pattern;

/**
 * Class to get Clock Status from IS
 * @author markowen
 */
public class ClockStatusISReceiver implements InfoListener {

    /** IS repository */
    private Repository isRepository = null;

    /** partition name */
    protected String partitionName = "initial";

    /** Are we subscribed? */
    private boolean subscribed;

    /** IS server name */
    private String serverName = "LHC";

    /** IS object name */
    private String objectName = "RF2TTCApp-DCS";

    public ClockStatusISReceiver() {
        subscribed=false;
        // connect to IS repository
        isRepository = new Repository(new ipc.Partition(partitionName));
    }

    /**
     * Return a string to show the partition name & serve.object used to contact IS.
     * @return
     */
    public String getConnString() {
        return "Partition: " + partitionName + ", Object: " + serverName + "." + objectName;
    }

    /** 
     * This function contacts the IS server & tries to get the current clock status.
     * 
     * @return the clock status (as a string)
     */
    public String getCurrentClock() {
        try {
            RF2TTC_DCS isinfo = new RF2TTC_DCS();
            isRepository.getValue(serverName + "." + objectName, isinfo);
	    
            String status = isinfo.BCmainSelection;
            IguiLogger.info("Clock status from IS: " + status);
            return status;
        } catch (RepositoryNotFoundException ex) {
            IguiLogger.error("RepositoryNotFoundException for " + serverName + "." + objectName);
        } catch (InfoNotFoundException ex) {
            IguiLogger.error("InfoNotFoundException for " + serverName + "." + objectName);
        } catch (InfoNotCompatibleException ex) {
            IguiLogger.error("InfoNotCompatibleException for " + serverName + "." + objectName);
            //Logger.getLogger(ClockStatusISReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }



    /**
     * Subscribe to the IS server
     */
    public void subscribe(boolean silent) {
        if(!subscribed) {
            try {
                isRepository.subscribe( serverName, new is.Criteria( Pattern.compile(objectName) ), this);
                subscribed = true;
            } catch(RepositoryNotFoundException ex) {
                IguiLogger.warning("RepositoryNotFoundException when subscribing to IS server " + serverName + " for object " + objectName);
                subscribed = false;
            } catch(InvalidCriteriaException ex) {
                IguiLogger.warning("InvalidCriteriaException when subscribing to IS server " + serverName + " for object " + objectName);
                subscribed = false;
            } catch (AlreadySubscribedException ex) {
            }
        }
    }//subscribe()

    /**
     * Subscribe to the IS server
     */
    public void unsubscribe() {
        if(subscribed) {
            try {
                isRepository.unsubscribe( serverName, new is.Criteria( Pattern.compile(objectName) ) );
                subscribed = false;
            } catch(RepositoryNotFoundException | SubscriptionNotFoundException ex) {
                
            }
        }
    }//unsubscribe()

    ////// -----------------------------------------------------
    ///////// Methods below are from InfoListener interface
    //////
    ////// Currently we don't do anything when the info changes.
    ////// -----------------------------------------------------

    @Override
    public void infoCreated(InfoEvent infoevent) {
    }

    @Override
    public void infoUpdated(InfoEvent infoevent) {
    }

    @Override
    public void infoDeleted(InfoEvent infoevent) {
    }
}
