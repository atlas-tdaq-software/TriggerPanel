package triggerpanel;

import static java.lang.Math.max;

import java.sql.*;
import oracle.jdbc.OracleConnection;
import Igui.IguiLogger;

/**
 * Handles the connection to the oracle Trigger Database on ATONR
 */
public class ConnectionManager {

    /**
     * db schema version
     * -1: schema has not yet been read from the database
     *  0: attempt has been made to read the schema from the DB, but it failed or was not available
     * >0: schema version after successful read from the database
     */
    private int dbSchemaVersion = -1;
    /**
     * Singleton pointer.
     */
    private static ConnectionManager connMgrInstance = null;
    /**
     * We need to keep a copy of the InitInfo.
     */
    private InitInfo savedInitInfo = null;
    /**
     * Connection is set up when the user logs in. The connection is reused in
     * any successive SQL query.
     */
    private Connection dbConnection = null;
    
    /**
     * This registers the additional JAR packages we need to talk to the
     * database(s) using Oracle.
     */
    private ConnectionManager()
    {}

    /**
     * @return true if the connection is open
     * @throws SQLException
     */
    public boolean isConnectionOpenAndValid() throws SQLException {
        return dbConnection != null && !dbConnection.isClosed() && dbConnection.isValid(30);
    }

    /**
     * Form a connection to the relevant database. At the moment this can
     * connect to Oracle only. It also stores a copy of the initialisation
     * information in savedInitInfo
     *
     * Does a lot of tricky things on atonr, including looking up a user's
     * access level and signing in as either the read only or write account
     * based on its findings.
     *
     * @param initInfo Object that contains the start-up information
     * @return true if the connection was made ok
     * @throws SQLException if something goes wrong connecting to DB.
     */
    public boolean connect(final InitInfo initInfo) throws SQLException {

        savedInitInfo = initInfo;
        final String url = initInfo.getURL();
        final String username = initInfo.getUserName();
        final String password = initInfo.getPassWord();
        final boolean online = initInfo.getOnline();
        final boolean onlineDB = initInfo.getOnlineDB();

        String msg = "\nConnecting to " + url;
        msg += "\n   System user " + initInfo.getSystemUsername();
        msg += "\n   DB username " + username;
        msg += "\n   Role        " + initInfo.getUserMode();
        msg += "\n   online      " + (online ? "Yes" : "No");
        msg += "\n   onlineDB    " + (onlineDB ? "Yes" : "No");
        IguiLogger.info(msg);
        try {
            dbConnection = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.err.println("Failed to connect to the database " + url + " as " + username + " with " + password);
            IguiLogger.error("Failed to connect to the database " + url + " as " + username + " with " + password);
            return false;
        }

        if (initInfo.getTechnology() == DBTechnology.ORACLE) {
            ((OracleConnection) dbConnection).setStatementCacheSize(100);
            ((OracleConnection) dbConnection).setImplicitCachingEnabled(true);
        }

        // gets driver info:
        final DatabaseMetaData meta = dbConnection.getMetaData();
        System.out.println("JDBC driver version is " + meta.getDriverVersion());

        return true;
    }

    /**
     * Closes the current connection, if any, and tries to remove any existing
     * write lock.
     *
     * @throws SQLException if problems with the db communication.
     */
    public void disconnect() throws SQLException {
        if (dbConnection == null) {
            return;
        }
        IguiLogger.info("Closing db connection.");
        try {
            dbConnection.close();
        }
        catch(final Exception exc) {}
        dbConnection = null;
    }

    /**
     * Get the init info, which contains important information about the user
     * and the database connection parameters.
     *
     * @return The Init Info object.
     */
    public InitInfo getInitInfo() {
        return this.savedInitInfo;
    }

    /**
     * Use this method when referring to the connection, so that we can swap
     * between connection to read from and the conncetion to write to.
     *
     * @return The connection. If doDBCopy return the connection to write to.
     */
    public Connection getConnection() {
        if (dbConnection == null) {
            IguiLogger.error("No DB connection");
        }
        return dbConnection;
    }

    /**
     * This needs to be a singleton so we can manage the connection to the
     * database properly.
     *
     * @return Instance of the singleton.
     */
    public static synchronized ConnectionManager getInstance() {
        if (connMgrInstance == null) {
            connMgrInstance = new ConnectionManager();
        }
        return connMgrInstance;
    }

    /**
     * Add the schema name in front of the table name in for ORACLE connections.
     * Fix query for the standard connection.
     *
     * @param query the original SQL query.
     * @return the query with fixed schema name.
     */
    public String fixSchemaName(final String query) {

        if (this.getInitInfo().getSchemaName().length() == 0) {
            return query;
        }

        final String[] tableNames = TableNames.allTableName;

        final String prefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";

        String fixedQuery = query;
        for (int i = 0; i < tableNames.length; ++i) {
            fixedQuery = fixedQuery.replace(" " + tableNames[i], " " + prefix
                    + tableNames[i]);
        }

        return fixedQuery;
    }

    /**
     *
     * @param connection
     * @return
     */
    public int getSchemaVersion(final Connection connection) {

        if (dbSchemaVersion >= 0) {
            return dbSchemaVersion;
        }

        int schema_version = 0;
        final String schemaPrefix = this.getInitInfo().getSchemaName().toUpperCase() + ".";
        final String run3query = "SELECT TS_TAG FROM " + schemaPrefix + "TRIGGER_SCHEMA";
        try (PreparedStatement ps = connection.prepareStatement(run3query)) {
            final ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                String tag = rset.getString("TS_TAG");
                final String regex = "^(Trigger-Run3-Schema-v)([0-9])+$";
                if(tag.matches(regex)) {
                    String tagVersion = tag.replaceAll(regex, "$2");
                    try {
                        schema_version = max(schema_version, Integer.parseInt(tagVersion));
                    } catch (NumberFormatException e) {
                        System.err.println("Schema tag '" + tag + "' is of wrong format, number part not recognized (" + tagVersion + ")");
                    }
                } else {
                    System.err.println("Schema tag is of wrong format: " + tag);
                }
                return schema_version;
            }
        } catch (final SQLException ex) {
            final String msg = "Will proceed assuming Run2 DB as caught error during Run3 schema version check: "
                    + "query was '" + run3query + "' gave error: "+ ex.getMessage() ;
            IguiLogger.error(msg);
        }
        dbSchemaVersion = schema_version;
        return dbSchemaVersion;
    }

    public static void main(String[] args) { 
        final String triggerdbAlias = "TRIGGERDB_RUN3";
        try {
            System.out.println("Test connecting to " + triggerdbAlias);
            InitInfo initInfo = new InitInfo();
            initInfo.setConnectionFromCommandLine(triggerdbAlias);
            ConnectionManager.getInstance().connect(initInfo);
            Connection connection = ConnectionManager.getInstance().getConnection();
            connection.prepareStatement("Select 1 from dual").executeQuery();
            ConnectionManager.getInstance().disconnect();
            System.out.println("Connection to Trigger DB " + triggerdbAlias + ": SUCCESS");
        } catch (Exception e) {
            System.out.println("Exception caught when connecting to TriggerDB" + e);
            System.out.println("Connection to Trigger DB " + triggerdbAlias + ": FAILURE");
            e.printStackTrace();
            System.exit(1);
        }
        System.exit(0);
    }
}
