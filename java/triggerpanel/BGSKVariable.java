package triggerpanel;

import Igui.IguiLogger;

class BGSKVariable extends OKSVariable<Integer> {

    BGSKVariable() {
        super("BGSK");
    }

    @Override
    protected void writeToRDB(Integer value, config.Configuration rdb) {
        dal.L1TriggerConfiguration l1conf = TPGlobal.getConfigHelper().getL1TriggerConfiguration(rdb);
        if (l1conf==null) return;
        try{
            l1conf.set_Lvl1BunchGroupKey(value);
        }
        catch(config.ConfigException ex){
            IguiLogger.error("Could not set BGRPK. Reason: " + ex);
        }
    }

    @Override
    protected Integer readFromRDB(config.Configuration rdb) {
        dal.L1TriggerConfiguration l1conf = TPGlobal.getConfigHelper().getL1TriggerConfiguration(rdb);
        if (l1conf==null) return 0;

        Integer BGRPK = 0;
        try{
            l1conf.update();
            BGRPK = l1conf.get_Lvl1BunchGroupKey();
        }
        catch(config.ConfigException ex){
            IguiLogger.error("Could not retrieve BGRPK. Reason: " + ex);
        }
        return BGRPK;
    }

}
