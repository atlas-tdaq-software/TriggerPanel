package triggerpanel;

import java.awt.*;
import javax.swing.*;
import java.io.*;

/**
 * @author Joerg Stelzer <stelzer@cern.ch>
 */
public class BunchGroupUploadDialog extends JDialog {

    /** Creates BunchGroupUploadDialog
     * @param parent
     * @param modal
     **/
    public BunchGroupUploadDialog(JFrame parent, boolean modal) 
    {
        super(parent, modal);
        m_cmd = defineCommand();
        m_parent = parent;
        initComponents();

        setTitle("Bunchgroup upload from LHC to database");
        // setAlwaysOnTop(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        // DB writing 
        m_bgrwWorker.execute();        

    }

    public int getBGK() {
        return m_bgrwWorker.m_bgsetKey;
    }

    private String defineCommand() {
        String cmd = "echo Either run in ATLAS at P1 (TDAQ_PARTITION=ATLAS and TDAQ_SETUP_POINT1=1) or define variables TEST_TDAQ_PARTITION and TEST_TRIGGERDB";
        String partition = System.getenv("TDAQ_PARTITION");
        String atPoint1 = System.getenv("TDAQ_SETUP_POINT1");
        if(partition!=null && atPoint1!=null && partition.equals("ATLAS") && atPoint1.equals("1")) {
            // certain we run in the ATLAS partition at P1
            cmd = "ReadBunchGroup.py --fromLHC -u";
        } else {
            System.out.println("env TDAQ_PARTITION = '" + partition + "'");
            System.out.println("env TDAQ_SETUP_POINT1 = '" + atPoint1 + "'");
            String testPartition = System.getenv("TEST_TDAQ_PARTITION");
            String testTriggerDB = System.getenv("TEST_TRIGGERDB");
            if(testPartition!=null && testTriggerDB!=null) {
                cmd = "ReadBunchGroup.py --fromLHC -p " + testPartition + " -u --db " + testTriggerDB;
            }
        }
        return cmd;
    }
    /**
     * SwingWorker to write the bg set to the db
     */
    private class BGWriterWorker extends SwingWorker<Void, Void> {

        private int m_bgsetKey = 0;
        private int m_activeBgsetKey = 0;
        private int m_exitCode=0;
        BGWriterWorker() {}

        @Override
        protected Void doInBackground() {
            ProcessBuilder processBuilder = new ProcessBuilder();
            processBuilder.command("bash", "-c", m_cmd);

            try {

                Process process = processBuilder.start();
                BufferedReader br =
                    new BufferedReader(new InputStreamReader(process.getInputStream()));
                BufferedReader ebr =
                    new BufferedReader( new InputStreamReader(process.getErrorStream()));

                while (true) {
                    String line = br.readLine();
                    if ( line == null ) {
                        break;
                    }
                    if( line.startsWith("= bgs key: ")) {
                        try {
                            m_bgsetKey = Integer.parseInt(line.replaceFirst("= bgs key: ",""));
                        } catch (NumberFormatException e) {
                            m_bgsetKey = 0;
                        }
                    } else if( line.startsWith("= active bgs key: ")) {
                        try {
                            m_activeBgsetKey = Integer.parseInt(line.replaceFirst("= active bgs key: ",""));
                        } catch (NumberFormatException e) {
                            m_activeBgsetKey = 0;
                        }
                    }
                    ta_output.append(line+"\n");
                }

                while (true) {
                    String errline = ebr.readLine();
                    if ( errline == null ) {
                        break;
                    }
                    ta_output.append(errline+"\n");
                }

                m_exitCode = process.waitFor();      
                ta_output.append("\nExited with code : " + m_exitCode + (m_exitCode==0?" (success)":" (failure)"));
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
            
            return null;
        }

        @Override
        protected void done() {
            super.done();
            if(m_exitCode!=0) {
                lbl_busy.setForeground(Color.red);
                lbl_busy.setText ( "Error (exit code " + m_exitCode + "), please inform the Trigger expert on-call!");
            } else if(m_bgsetKey>0) {
                lbl_busy.setText("Done");
                String msg = "Bunch Group Set Key: " + m_bgsetKey + "\n\n";
                if(m_bgsetKey != m_activeBgsetKey) {
                    msg += "The active bunchgroup set is " + m_activeBgsetKey + ". The ongoing LHC fill corresponds to bunchgroup set " + m_bgsetKey + ".\n";
                    msg += "If this matches the instructions on the Trigger Whiteboard then please inform Shift Leader and\nRun Control shifter to change the bunchgroup set to " + m_bgsetKey + "!\n";
                    msg += "If the whiteboard suggests a different bunchgroup set then please inform the Trigger expert on-call.";
                } else {
                    msg += "The active key is already " + m_bgsetKey + ",\n";
                    msg += "nothing needs to be done!";
                }
                JOptionPane.showMessageDialog(m_parent, msg, "Key upload information", JOptionPane.INFORMATION_MESSAGE);
            } else {
                lbl_busy.setForeground(Color.red);
                lbl_busy.setText("Error, could not determine the uploaded key, please inform the Trigger expert on-call!");
            }
            btn_close.setEnabled(true);
        }
        
    }//class BGWriterWorker

    /** This method is called from within the constructor to initialize the form.
     */
    private void initComponents() {

        // title line
        final JLabel lbl_title = new JLabel("Upload bunchgroup set based on LHC to the Trigger DB:");
        lbl_title.setFont(new Font("Serif", Font.BOLD, 16));
        lbl_title.setForeground(new Color(0, 0, 153));
        lbl_title.setHorizontalAlignment(SwingConstants.LEFT);
        lbl_title.setVerticalAlignment(SwingConstants.BOTTOM);
        lbl_title.setAlignmentX(0.5F);
        // buttons
        btn_close.addActionListener((evt) -> {
            dispose();
        });
        btn_close.setEnabled(false);

        ta_output.setLineWrap(true);
        ta_output.setWrapStyleWord(true);

        /**** arrange the components *****/
        JPanel panel_main = new JPanel();
        panel_main.setBorder(BorderFactory.createEmptyBorder(20, 10, 10,10));
        panel_main.setLayout(new BorderLayout());

        /**** the title panel *****/
        JPanel panel_title = new JPanel();
        panel_title.setLayout(new BoxLayout(panel_title, BoxLayout.LINE_AXIS));
        panel_title.add(lbl_title);
        panel_main.add(panel_title, BorderLayout.NORTH);

        /**** the panel with the text box *****/
        JPanel panel_textbox = new JPanel();
        panel_textbox.setLayout(new BoxLayout(panel_textbox, BoxLayout.LINE_AXIS));
        panel_textbox.setBorder(BorderFactory.createEmptyBorder(30, 10, 20,10));
        JScrollPane scrollpane = new JScrollPane(ta_output);
        panel_textbox.add(scrollpane);
        panel_main.add(panel_textbox, BorderLayout.CENTER);

        /**** the button panel *****/
        final JPanel panel_buttons = new JPanel();
        panel_buttons.setLayout(new BoxLayout(panel_buttons, BoxLayout.LINE_AXIS));
        panel_buttons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_buttons.add(lbl_busy);
        panel_buttons.add(Box.createHorizontalGlue());
        panel_buttons.add(btn_close);
        // panel_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        // panel_buttons.add(btn_write);
        panel_main.add(panel_buttons, BorderLayout.SOUTH);

        setContentPane(panel_main);

        pack();
        setLocationRelativeTo(null);
        Dimension minsize = getMinimumSize();
        minsize.width += 10;
        setMinimumSize(minsize);

    }

    final private JFrame m_parent;
    private String m_cmd = "";
    private BGWriterWorker m_bgrwWorker = new BGWriterWorker();
    private JLabel lbl_busy = new JLabel("Performing bunchgroup set extraction and upload ...");
    private JButton btn_close = new JButton("Close");
    private JTextArea ta_output = new JTextArea(40,60);

    public static void main(String[] args) {
        /* Set the look and feel
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         **/
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BunchGroupUploadDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                BunchGroupUploadDialog dialog = new BunchGroupUploadDialog(null, true);
                dialog.ta_output.append("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                dialog.setVisible(true);     
            }
        });
    }
}
