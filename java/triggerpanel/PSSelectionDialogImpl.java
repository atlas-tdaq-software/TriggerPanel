package triggerpanel;

import Igui.IguiLogger;
import TTCInfo.TrigConfL1BgKeyNamed;
import TTCInfo.TrigConfL1PsKeyNamed;
import is.InfoNotCompatibleException;
import is.RepositoryNotFoundException;

import java.awt.event.*;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

class PSSelectionDialogImpl extends PSSelectionDialog {
    private static final long serialVersionUID = 1L;

    /** public methods */
    boolean isCancelled() {
        return !return_status;
    }

    KeyDescriptor selectedL1PS() {
        return selectedl1;
    }

    KeyDescriptor selectedHLTPS() {
        return selectedhlt;
    }

    KeyDescriptor selectedBGS() {
        return selectedbgs;
    }

    /** Trigger Configuration panel */
    final private TriggerPanel triggerPanel;

    // Are we modifying standby, emittance or physics keys?
    private RunState state;

    // does the user want to modify something?
    private boolean return_status = false;

    // IS Named objects, provide access to the current keys
    private TrigConfL1PsKeyNamed l1IS = null;
    private TrigConfHltPsKeyNamed hltIS = null;
    private TrigConfL1BgKeyNamed bgsIS = null;
    private KeyDescriptor currentl1 = null;
    private KeyDescriptor currenthlt = null;
    private KeyDescriptor currentbgs = null;

    // the selected keys
    private KeyDescriptor selectedl1 = null;
    private KeyDescriptor selectedhlt = null;
    private KeyDescriptor selectedbgs = null;

    private boolean modifiedl1 = false;
    private boolean modifiedhlt = false;
    private boolean modifiedbgs = false;

    public PSSelectionDialogImpl(JFrame parent, boolean modal, TriggerPanel tp, RunState state, RunState currentState) {
        super(state, currentState, parent, modal);
        this.state = state;
        this.triggerPanel = tp;

        TPGlobal.theDbKeysLoader().FlagL1PskAsNotLoaded();
        TPGlobal.theDbKeysLoader().FlagHLTPskAsNotLoaded();

        // set up the panel
        try {
            // read the prescales for the current SMK - will block EDT, but need to because
            // can't do anything
            // till drop down is ready
            TPGlobal.theDbKeysLoader().UpdatePrescales(triggerPanel.getCurrentSMK());
        } catch (SQLException ex) {
            Logger.getLogger(PSSelectionDialogImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        // get the current keys from IS (no need to subscribe, since they can't change
        // underneath)
        readPSKeysFromIS();

        fillPrescaleDropDownBoxes();

        setButtonStatus();

        notifyOfCurrentState(currentState);

    }

    /**
     * Function reads keys from IS.
     */
    private void readPSKeysFromIS() {

        String firstpart = "";
        switch (state) {
            case STANDBY:
                firstpart = "RunParams.Standby.";
                break;
            case EMITTANCE:
                firstpart = "RunParams.Emittance.";
                break;
            case PHYSICS:
                firstpart = "RunParams.Physics.";
                break;
        }
        l1IS = new TrigConfL1PsKeyNamed(TPGlobal.igui.getPartition(), firstpart + "L1PsKey");
        hltIS = new TrigConfHltPsKeyNamed(TPGlobal.igui.getPartition(), firstpart + "HltPsKey");

        try {
            l1IS.checkout();
        } catch (is.InfoNotFoundException | RepositoryNotFoundException | InfoNotCompatibleException ex) {
            IguiLogger.warning("L1PS IS object not available." + ex.getMessage());
            l1IS.L1PrescaleKey = 0;
            l1IS.L1PrescaleComment = "";
        }
        try {
            hltIS.checkout();
        } catch (is.InfoNotFoundException | RepositoryNotFoundException | InfoNotCompatibleException ex) {
            IguiLogger.warning("HLTPS IS object not available." + ex.getMessage());
            hltIS.HltPrescaleKey = 0;
            hltIS.HltPrescaleComment = "";
        }
        currentl1 = triggerPanel.getKeyDescriptor(l1IS.L1PrescaleKey, LVL.L1);
        currenthlt = triggerPanel.getKeyDescriptor(hltIS.HltPrescaleKey, LVL.HLT);
        if (currentl1 != null) {
            curL1Label.setText(Integer.toString(currentl1.key));
            curL1Label.setToolTipText(currentl1.comment);
        }
        if (currenthlt != null) {
            curHLTLabel.setText(Integer.toString(currenthlt.key));
            curHLTLabel.setToolTipText(currenthlt.comment);
        }

    }

    private void fillDropDownBox(final JComboBox<KeyDescriptor> ddBox, final Vector<KeyDescriptor> dbKeys,
            final int selectedKey, final boolean showHidden) {
        ddBox.removeAllItems();
        for (KeyDescriptor kd : dbKeys) {
            if (!showHidden && kd.hidden) {
                continue;
            }
            ddBox.addItem(kd);
            if (kd.key == selectedKey) { // mark current key as selected
                ddBox.setSelectedItem(kd);
            }
        }
    }

    private void fillPrescaleDropDownBoxes() {
        fillDropDownBox(dropdown_L1, TPGlobal.theDbKeysLoader().GetL1Prescales(), l1IS.L1PrescaleKey,
                showHidden.isSelected());
        fillDropDownBox(dropdown_HLT, TPGlobal.theDbKeysLoader().GetHLTPrescales(), hltIS.HltPrescaleKey,
                showHidden.isSelected());
    }

    private void setButtonStatus() {

        boolean forceEnableSelectButton = false;

        if (state == GUIState.getCurrentRunState()) {
            if ((currentl1 != null && (triggerPanel.getCurrentL1PSK() != currentl1.key)) ||
                (currenthlt != null && (triggerPanel.getCurrentHLTPSK() != currenthlt.key))) {
                // Current prescales do not match current state so we activate the OK button
                // IguiLogger.info("triggerPanel.OKS " + TPGlobal.OKS);
                // IguiLogger.info("triggerPanel.getCurrentL1PSK() " + triggerPanel.getCurrentL1PSK());
                // IguiLogger.info("triggerPanel.getCurrentHLTPSK() " + triggerPanel.getCurrentHLTPSK());
                // IguiLogger.info("currentl1.key " + currentl1.key);
                // IguiLogger.info("currenthlt.key " + currenthlt.key);
                forceEnableSelectButton = true;
            }
        }

        cancelButton.setEnabled(true);
        selectButton.setEnabled(
                modifiedl1 || modifiedhlt || (isBGSSelectionActive() && modifiedbgs) || forceEnableSelectButton);
    }

    @Override
    protected final void selectionL1KeyChanged(ItemEvent evt) {
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            selectedl1 = (KeyDescriptor) evt.getItem();
            modifiedl1 = (selectedl1.key != l1IS.L1PrescaleKey);
            IguiLogger.info("Check modified L1 = " + selectedl1.key + " != " + l1IS.L1PrescaleKey + " = " + modifiedl1);
            setButtonStatus();
        }
    }

    @Override
    protected final void selectionHLTKeyChanged(ItemEvent evt) {
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            selectedhlt = (KeyDescriptor) evt.getItem();
            modifiedhlt = (selectedhlt.key != hltIS.HltPrescaleKey);
            IguiLogger.info(
                    "Check modified HLT = " + selectedhlt.key + " != " + hltIS.HltPrescaleKey + " = " + modifiedhlt);
            setButtonStatus();
        }
    }

    @Override
    protected final void selectionBGSKeyChanged(ItemEvent evt) {
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            selectedbgs = (KeyDescriptor) evt.getItem();
            modifiedbgs = (selectedbgs.key != this.triggerPanel.getCurrentBG());
            IguiLogger.info(
                    "Check modified BGS = " + selectedbgs.key + " != " + bgsIS.L1BunchGroupKey + " = " + modifiedbgs);
            setButtonStatus();
        }
    }

    @Override
    protected void actionShowBGSSelection(ItemEvent evt) {
        super.actionShowBGSSelection(evt);

        if (!isBGSSelectionActive()) { // do nothing if checkbox was disabled
            return;
        }
        new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {

                // we always want to reload the list of available bunchgroup sets
                TPGlobal.theDbKeysLoader().UpdateBunchgroups();

                // read bgs key IS
                if (bgsIS == null) {
                    bgsIS = new TrigConfL1BgKeyNamed(TPGlobal.igui.getPartition(), "RunParams.TrigConfL1BgKey");
                }
                try {
                    bgsIS.checkout();
                } catch (is.InfoNotFoundException | RepositoryNotFoundException | InfoNotCompatibleException ex) {
                    IguiLogger.warning("RunParams.TrigConfL1BgKey IS object not available." + ex.getMessage());
                    bgsIS.L1BunchGroupKey = 0;
                    bgsIS.L1BunchGroupComment = "";
                }
                return null;
            }

            @Override
            protected void done() {
                final int bgsk = triggerPanel.getCurrentBG(); // from OKS in INITIAL
                currentbgs = triggerPanel.getKeyDescriptor(bgsk, LVL.BGS);
                if (currentbgs != null) {
                    lb_current_bgsk.setText(Integer.toString(currentbgs.key));
                    lb_current_bgsk.setToolTipText(currentbgs.comment);
                }

                fillBGSDropDownBox(bgsk);

                setButtonStatus();
            }
        }.execute();
    }

    private void fillBGSDropDownBox(final int bgsk) {
        fillDropDownBox(dropdown_BGS, TPGlobal.theDbKeysLoader().GetBunchgroups(), bgsk, showHidden.isSelected());
    }

    @Override
    protected final void actionShowHidden(ActionEvent evt) {
        Thread worker = new Thread() {
            @Override
            public void run() {
                fillPrescaleDropDownBoxes();
                if (isBGSSelectionActive()) {
                    fillBGSDropDownBox(triggerPanel.getCurrentBG());
                }
                // Report the result using invokeLater().
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        pack();
                    }
                });
            }
        };
        worker.start();

    };

    @Override
    protected final void actionSelectButton(ActionEvent evt) {
        return_status = true;
        dispose();
    }

    @Override
    protected final void actionCancelButton(ActionEvent evt) {
        return_status = false;
        // reset
        selectedl1 = currentl1;
        selectedhlt = currenthlt;
        selectedbgs = currentbgs;
        dispose();
    }

}
