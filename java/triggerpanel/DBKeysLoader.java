package triggerpanel;

import Igui.IguiLogger;
import static ers.Logger.error;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import static triggerpanel.TPGlobal.OKS;

/**
 *
 * @author Joerg Stelzer
 */
final class DBKeysLoader {
    
    private Vector<KeyDescriptor> mL1PrescaleKeys = new Vector<>();
    private Vector<KeyDescriptor> mHLTPrescaleKeys = new Vector<>();
    private Vector<KeyDescriptor> mSuperMasterKeys   = null;
    private Vector<KeyDescriptor> mBunchgroupsetKeys  = new Vector<>();
    private PrescaleSetAliasHandle aliases = new PrescaleSetAliasHandle();
            
    // control the state of the object
    private int currentSMK = 0;
    private boolean _l1psAreLoaded;
    private boolean _hltpsAreLoaded;
    // private boolean psAliasesAreLoaded = false;
    
    
    // InitInfo object for connected to DB
    private final InitInfo initInfo = new InitInfo();
    private String aliasForInitInfo = ""; 

    void setL1psAreLoaded(final boolean loaded, final String setter) {
        _l1psAreLoaded = loaded;
        IguiLogger.info("Setting l1psAreLoaded to " + _l1psAreLoaded + " by " + setter);
    }

    boolean getL1psAreLoaded() {
        IguiLogger.info("Accessing l1psAreLoaded: " + _l1psAreLoaded);
        return _l1psAreLoaded;
    }

    void setHLTpsAreLoaded(final boolean loaded, final String setter) {
        _hltpsAreLoaded = loaded;
        IguiLogger.info("Setting hltpsAreLoaded to " + _hltpsAreLoaded + " by " + setter);
    }

    boolean getHLTpsAreLoaded() {
        IguiLogger.info("Accessing hltpsAreLoaded: " + _hltpsAreLoaded);
        return _hltpsAreLoaded;
    }

    DBKeysLoader() {
        
        setL1psAreLoaded(false, "constructor DBKeysLoader()");
        setHLTpsAreLoaded(false, "constructor DBKeysLoader()");

        OKS.bind(() -> {
            // it gets called during readOKS
            // set connection string
            SetConnectionAlias(OKS.getDBConnVar().value());
        });
    }

    // Setting the SMK, invalidating the prescale sets and aliases
    void SetSMK(final int smk) {
        if(smk == this.currentSMK) {
            return;
        }
        currentSMK = smk;
        setL1psAreLoaded(false, "SetSMK(" + smk + ")");
        setHLTpsAreLoaded(false, "SetSMK(" + smk + ")");
        // psAliasesAreLoaded = false;
    }
    
    // access functions
    Vector<KeyDescriptor> GetPrescales(final LVL level) {
        return level == LVL.L1 ? mL1PrescaleKeys : mHLTPrescaleKeys;
    }

    Vector<KeyDescriptor> GetL1Prescales()  {
        return mL1PrescaleKeys;
    }
    
    Vector<KeyDescriptor> GetHLTPrescales() { return mHLTPrescaleKeys; }

    Vector<KeyDescriptor> GetMenus()        { return mSuperMasterKeys; }

    Vector<KeyDescriptor> GetBunchgroups() {
        if(mBunchgroupsetKeys.size()==0) {
            mBunchgroupsetKeys = LoadBunchgroupsetKeysFromDb();
        }
        return mBunchgroupsetKeys;
    }
    
    PrescaleSetAliasHandle GetAliases() throws SQLException {
        // UpdateAliasComponents(); broken since start of Run 
        return aliases;
    }
    
    /**
     * Access to the full configuration description
     *
     * @param key key for which the description should be retrieved
     * @param level level for which the key is valid (MENU, LVL1, HLT)
     * @return the full configuration description
     */
    KeyDescriptor getKeyDescriptor(final int key, final LVL level) {
        if (key == -1) {
            return KeyDescriptor.NotInIS;
        }

        if (key == 0) {
            return KeyDescriptor.NULL; // 0 is an invalid key and used as such throughout the code, -1 means 'not present in IS'
        }

        Vector<KeyDescriptor> keylist = null;
        switch(level) {
            case L1:
            case HLT:
                keylist = TPGlobal.theDbKeysLoader().GetPrescales(level);
                break;
            case MENU:
                keylist = TPGlobal.theDbKeysLoader().GetMenus();
                break;
            case BGS:
                keylist = TPGlobal.theDbKeysLoader().GetBunchgroups();
        }
        if (keylist == null) {
            throw new RuntimeException("No Vector<KeyDescriptor> for level " + level);
        }
        for (KeyDescriptor kd : keylist) {
            if (kd.key == key) {
                return kd;
            }
        }
        IguiLogger.warning("Did not find key " + key + " in " + level + " ( " + keylist.size() + " keys available)");
        return KeyDescriptor.NotValidForSMK;
    }
    
    void FlagL1PskAsNotLoaded() {
        setL1psAreLoaded(false, "FlagL1PskAsNotLoaded");
    }
    
    void FlagHLTPskAsNotLoaded() {
        setHLTpsAreLoaded(false, "FlagHLTPskAsNotLoaded");
    }

    
    /********************************************************************
     * 
     * Load data from the database
     * 
     ********************************************************************/
    
    void UpdateMenus() {
        mSuperMasterKeys = LoadSupermasterKeysFromDb();
    }

    void UpdateBunchgroups() {
        mBunchgroupsetKeys = LoadBunchgroupsetKeysFromDb();
    }

    void UpdatePrescales(final int smk) throws SQLException {
        IguiLogger.info("UpdatePrescales called for " + smk + "\n  with currentSMK " + currentSMK + ", l1psAreLoaded " + getL1psAreLoaded() + ", hltpsAreLoaded " + getHLTpsAreLoaded());
        
        if(smk == currentSMK && getL1psAreLoaded() && getHLTpsAreLoaded()) {
            return;
        }
        if(smk != currentSMK) {
            setL1psAreLoaded(false,"UpdatePrescales if SMK is not current");
            setHLTpsAreLoaded(false,"UpdatePrescales if SMK is not current");
            // psAliasesAreLoaded = false;
            currentSMK = smk;
        }
        IguiLogger.info("Update L1 and HLT prescales from DB for SMK " + smk);
        boolean connectedHere = DbConnect();
        if( ! getL1psAreLoaded() ) {
            LoadL1PrescaleKeysFromDb ( smk  );
        }
        if( ! getHLTpsAreLoaded() ) {
            mHLTPrescaleKeys = LoadHLTPrescaleKeysFromDb( smk );
        }
        if ( connectedHere ) {
            //DbDisconnect();
        }
    }

    void UpdateL1Prescales(final int smk) throws SQLException {
        if(smk == currentSMK && getL1psAreLoaded()) {
            return;
        }
        if(smk != currentSMK) {
            setL1psAreLoaded(false,"UpdateL1Prescales if SMK is not current");
            setHLTpsAreLoaded(false,"UpdateL1Prescales if SMK is not current");
            // psAliasesAreLoaded = false;
            currentSMK = smk;
        }

        boolean connectedHere = DbConnect();
        if( ! getL1psAreLoaded() ) {
            LoadL1PrescaleKeysFromDb ( smk  );
        }
        if ( connectedHere ) {
            //DbDisconnect();
        }
    }

    void UpdateHLTPrescales(final int smk) throws SQLException {
        if(smk == currentSMK && getHLTpsAreLoaded()) {
            return;
        }
        if(smk != currentSMK) {
            setL1psAreLoaded(false,"UpdateHLTPrescales if SMK is not current");
            setHLTpsAreLoaded(false,"UpdateHLTPrescales if SMK is not current");
            currentSMK = smk;
        }

        boolean connectedHere = DbConnect();
        if( ! getHLTpsAreLoaded() ) {
            mHLTPrescaleKeys = LoadHLTPrescaleKeysFromDb( smk );
        }
        if ( connectedHere ) {
            //DbDisconnect();
        }
    }

    // private void UpdateAliasComponents() throws SQLException {
        
    //     boolean connectedHere = DbConnect();
    //     this.aliases = new PrescaleAliasLoader().load(currentSMK);
    //     if ( connectedHere ) {
    //         //DbDisconnect();
    //     }
    //     psAliasesAreLoaded = true;
    // }
    
    boolean DbConnect() {
        if ( IsConnected() ) {
            return false;
        }
        try {
            IguiLogger.info("Connecting to TriggerDB\n" + initInfo);
            ConnectionManager.getInstance().connect(initInfo);
            return true;
        } catch (SQLException ex) {
            IguiLogger.error("Could not establish connection to TriggerDB" + ex, ex);
            ex.printStackTrace();
            return false;
        }
    }

    void DbDisconnect() {
        if ( ! IsConnected() ) {
            return;
        }
        try {
            ConnectionManager.getInstance().getConnection().close();
            IguiLogger.info("Closing connection to TriggerDB");
        } catch (SQLException ex) {
            IguiLogger.error("Could not close connection to TriggerDB" + ex, ex);
        }
    }

    private boolean IsConnected() {
        try {
            return ConnectionManager.getInstance().isConnectionOpenAndValid();
        } catch (SQLException ex) {
            IguiLogger.error("Could not determine TriggerDB connection status" + ex, ex);
        }
        return false;
    }

    void SetConnectionAlias(final String connectionalias) {

        if(aliasForInitInfo.equals(connectionalias)) {
            return; // initInfo already configured with this alias
        }
        aliasForInitInfo = connectionalias;
        try {
            IguiLogger.info("TriggerDBConnection alias: " + connectionalias);
            String dblookup = System.getenv("CORAL_DBLOOKUP_PATH");
            if(dblookup==null) { dblookup="not defined"; }
            IguiLogger.info("TriggerDBConnection dblookup path: " + dblookup);
            String dbauth = System.getenv("CORAL_AUTH_PATH");
            if(dbauth==null) { dbauth="not defined"; }
            IguiLogger.info("TriggerDBConnection dbauth path: " + dbauth);
            initInfo.setConnectionFromCommandLine(connectionalias);
            // now this is a terrible hack, but at the moment I don't see another way.
            final String schema = initInfo.getSchemaName();
            if(schema.endsWith("_R")) { // remove the _R
                initInfo.setSchemaName(schema.substring(0,schema.length()-2));
            }
            initInfo.setgoodInfo(true);
            IguiLogger.info(""+initInfo);
            
        } catch (Exception ex) {
            TPL.error("Could not set connection in InitInfo");
            error(new TriggerPanelIssue("Could not set db connection in InitInfo", ex));
            ex.printStackTrace();
        }
    }
    
    int GetL1MenuId(final int smk) {
        int menuid = 0;
        boolean connectedHere = DbConnect();
        String schemaPrefix = ConnectionManager.getInstance().getInitInfo().getSchemaName().toUpperCase() + ".";
        String query = "SELECT SMT_L1_MENU_ID " +
            "FROM " + schemaPrefix + "SUPER_MASTER_TABLE " +
            "WHERE SMT_ID = ?";
        IguiLogger.info(query);
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, smk);
            ResultSet rset = ps.executeQuery();
            if (rset.next()) {
                menuid = rset.getInt(1);
            }
        } catch (SQLException ex) {
            error(new TriggerPanelIssue("SQLException when getting l1 menu id from smk", ex));            
            if ( connectedHere ) {
                //DbDisconnect();
            }
        }
        if ( connectedHere ) {
            //DbDisconnect();
        }
        IguiLogger.info("Got menu id " + menuid);
        return menuid;
    }

    /**
     * Load from database
     *   L1 prescale keys
     *   HLT prescale keys
     *   Bunchgroup set keys
     *   Supermaster keys
     */
    private void LoadL1PrescaleKeysFromDb(final int smk) {
        // clear the old set
        mL1PrescaleKeys.clear();
        if(! IsConnected()) {
            TPL.error("No TriggerDB connection, will not load prescales");
            error(new TriggerPanelIssue("No TriggerDB connection, cannot load L1 prescales"));
            return;
        }
        IguiLogger.info("Loading L1 prescale keys for smk " + smk);
        String schemaPrefix = ConnectionManager.getInstance().getInitInfo().getSchemaName().toUpperCase() + ".";
        String query = "SELECT L1PS_ID, L1PS_NAME, L1PS_VERSION, L1PS_COMMENT, L1PS_HIDDEN, L1PS_PARTITION " +
            "FROM " + schemaPrefix + "L1_PRESCALE_SET, " + schemaPrefix + "SUPER_MASTER_TABLE, " + schemaPrefix + "L1_MENU " +
            "WHERE SMT_ID=? " +
            "AND SMT_L1_MENU_ID = L1TM_ID " +
            "AND L1PS_L1_MENU_ID = L1TM_ID " +
            "ORDER BY L1PS_ID DESC";
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, smk);
            ResultSet rset = ps.executeQuery();
            while (rset.next()) {
                final int key = rset.getInt(1);
                final String name = rset.getString(2);
                final int version = rset.getInt(3);
                final String comment = (rset.getString(4)!=null && !rset.getString(4).equals("~"))?rset.getString(4):"";
                final boolean hidden = (Integer.parseInt(rset.getString(5))==1);
                mL1PrescaleKeys.add(new KeyDescriptor(key, name, version, comment, hidden));
            }
        } catch (SQLException ex) {
            TPL.error("SQLException when loading L1 prescales: " + ex.getMessage());
            error(new TriggerPanelIssue("SQLException when loading L1 prescales", ex));            
        }
        IguiLogger.info("Loaded " + mL1PrescaleKeys.size() + " L1 prescale keys for smk " + smk);
        setL1psAreLoaded(true,"LoadL1PSKeys");
    }

    private Vector<KeyDescriptor> LoadHLTPrescaleKeysFromDb(final int smk) {
        IguiLogger.info("Loading HLT prescale keys for smk " + smk);
        if(! IsConnected()) {
            TPL.error("No TriggerDB connection, will not load prescales");
            error(new TriggerPanelIssue("No TriggerDB connection, cannot load HLT prescales"));
            return new Vector<>();
        }
        Vector<KeyDescriptor> pskV = new Vector<>();
        String schemaPrefix = ConnectionManager.getInstance().getInitInfo().getSchemaName().toUpperCase() + ".";
        String query = "SELECT  HPS_ID, HPS_NAME, HPS_VERSION, HPS_COMMENT, HPS_HIDDEN " +
            "FROM " + schemaPrefix + "HLT_PRESCALE_SET, " + schemaPrefix + "SUPER_MASTER_TABLE, " + schemaPrefix + "HLT_MENU " +
            "WHERE SMT_ID=? " +
            "AND SMT_HLT_MENU_ID = HTM_ID " +
            "AND HPS_HLT_MENU_ID = HTM_ID " +
            "ORDER BY HPS_ID DESC";
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query)) {
            ps.setInt(1, smk);
            try (ResultSet rset = ps.executeQuery()) {
                while (rset.next()) {
                    final int key = rset.getInt(1);
                    final String name = rset.getString(2);
                    final int version = rset.getInt(3);
                    final String comment = (rset.getString(4)!=null && !rset.getString(4).equals("~"))?rset.getString(4):"";
                    final boolean hidden = (Integer.parseInt(rset.getString(5))==1);
                    pskV.add(new KeyDescriptor(key, name, version, comment, hidden));
                }
            }
        } catch (SQLException ex) {
            TPL.error("SQLException when loading HLT prescales: " + ex.getMessage());
            error(new TriggerPanelIssue("SQLException when loading HLT prescales", ex));            
        }
        setHLTpsAreLoaded(true,"LoadHLTPSKeys");
        IguiLogger.info("Loaded " + pskV.size() + " HLT prescale keys for smk " + smk);
        return pskV;
    }

    private Vector<KeyDescriptor> LoadBunchgroupsetKeysFromDb()  {
        DbConnect();
        if(! IsConnected() ) {
            TPL.error("No TriggerDB connection, will not load bunchgroups");
            error(new TriggerPanelIssue("No TriggerDB connection, will not load bunchgroups"));
            return new Vector<>();
        }
        IguiLogger.info("Loading bunch group keys");
        Vector<KeyDescriptor> bgs = new Vector<>();
        String schemaPrefix = ConnectionManager.getInstance().getInitInfo().getSchemaName().toUpperCase() + ".";
        String query = "SELECT L1BGS_ID, L1BGS_NAME, L1BGS_VERSION, L1BGS_COMMENT, L1BGS_HIDDEN " +
            "FROM " + schemaPrefix + "L1_BUNCH_GROUP_SET " +
            "ORDER BY L1BGS_ID DESC";
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                final int key = rset.getInt(1);
                final String name = rset.getString(2);
                final int version = rset.getInt(3);
                final String comment = rset.getString(4);
                final boolean hidden = rset.getBoolean(5);
                bgs.add(new KeyDescriptor(key,name, version, comment, hidden));
            }
        } catch(SQLException ex) {
            TPL.error("SQLException caught when loading bunchgroups");
            error(new TriggerPanelIssue("SQLException caught when loading bunchgroups", ex));            
        }
        //DbDisconnect();
        return bgs;
    }

    private Vector<KeyDescriptor> LoadSupermasterKeysFromDb() {
        DbConnect();
        if(! IsConnected() ) {
            TPL.error("No TriggerDB connection, will not load menus");
            error(new TriggerPanelIssue("No TriggerDB connection, will not load menus"));            
            return new Vector<>();
        }
        TPL.info("Loading menu keys and description from TriggerDB");
        Vector<KeyDescriptor> menus = new Vector<>();
        String schemaPrefix = ConnectionManager.getInstance().getInitInfo().getSchemaName().toUpperCase() + ".";
        String query = "SELECT SMT_ID, SMT_NAME, SMT_VERSION, SMT_COMMENT, SMT_HIDDEN " +
            "FROM " + schemaPrefix + "SUPER_MASTER_TABLE " +
            "ORDER BY SMT_ID DESC";
        try (PreparedStatement ps = ConnectionManager.getInstance().getConnection().prepareStatement(query); ResultSet rset = ps.executeQuery()) {
            while (rset.next()) {
                final int key = rset.getInt(1);
                final String name = rset.getString(2);
                final int version = rset.getInt(3);
                final String comment = (rset.getString(4)!=null && !rset.getString(4).equals("~"))?rset.getString(4):"";
                final boolean hidden = (rset.getInt(5)==1);  // SMT_HIDDEN==1 means key is hidden
                menus.add(new KeyDescriptor(key, name, version, comment, hidden));
            }
        } catch (SQLException ex) {
            TPL.error("SQLException when loading SMK: " + ex.getMessage());
            error(new TriggerPanelIssue("SQLException when loading SMK: ", ex));            
        }
        //DbDisconnect();
        return menus;
    }

    
}
