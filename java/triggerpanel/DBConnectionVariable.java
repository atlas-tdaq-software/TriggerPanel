package triggerpanel;

class DBConnectionVariable extends OKSVariable<String> {

    DBConnectionVariable() {
        super("DBConn");
    }

    @Override
    protected void writeToRDB(String value, config.Configuration rdb) {
        throw new UnsupportedOperationException("DB Connection not writeable");
    }

    @Override
    protected String readFromRDB(config.Configuration rdb) {
    
        dal.TriggerDBConnection dbconn = TPGlobal.getConfigHelper().getTriggerDBConnection(rdb);
        if (dbconn==null) return "";

        String Alias = "";
        try{
             Alias = dbconn.get_Alias();
        }
        catch(config.ConfigException ex){
             TPL.error("Could not retrieve Alias" + ex);
        }
        return Alias;

    }
}
