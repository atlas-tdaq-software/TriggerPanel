package triggerpanel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

/**
 *
 * @author William, Joerg
 */
public class AliasSelectionDialog extends JDialog {

    /**
     * Creates AliasSelectionDialog
     *
     * @param parent
     * @param modal
     */
    public AliasSelectionDialog(JFrame parent, boolean modal) {
        super(parent, modal);
        initComponents();

        setTitle("Alias selection");
        setAlwaysOnTop(true);
        setLocationRelativeTo(parent);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        SelectButton.setEnabled(false);
        matchWarningLabel.setForeground(TPGlobal.uncommittedclr);
        matchWarningLabel.setVisible(false);
    }

    public String getSelectedAliasName() {
        return (String) aliasComboBox.getSelectedItem();
    }

    /****** private methods *******/
    private void initComponents() {

        titleLabel.setFont(new java.awt.Font("Tahoma", Font.BOLD, 18));
        matchWarningLabel.setFont(new Font("Tahoma", Font.ITALIC, 13));
        currentPrescalesLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
        currentAliasLabel.setFont(new java.awt.Font("Tahoma", Font.BOLD, 13));
        newAliasLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
        newAliasLabel.setMaximumSize(new Dimension(94, 16));
        newAliasLabel.setMinimumSize(new Dimension(94, 16));
        newAliasLabel.setName("");
        newAliasLabel.setPreferredSize(new Dimension(94, 16));
        newPrescalesLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
        overrideWarningLabel.setFont(new Font("Tahoma", Font.ITALIC, 13));

        CancelButton.addActionListener((evt) -> {
            cancelButtonAction(evt);
        });

        SelectButton.addActionListener((evt) -> {
            selectButtonAction(evt);
        });

        aliasComboBox.setModel(new DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        aliasComboBox.addActionListener((evt) -> {
            aliasComboBoxAction(evt);
        });

        Box box_current = Box.createVerticalBox();
        box_current.setBorder(BorderFactory.createTitledBorder("Current alias"));
        box_current.add(Box.createVerticalStrut(10));
        Box hbox = (Box)box_current.add(Box.createHorizontalBox());
        hbox.add(Box.createRigidArea(new Dimension(10,0)));
        hbox.add(currentAliasLabel);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(currentAliasNameLabel);
        hbox.add(Box.createHorizontalGlue());
        box_current.add(Box.createVerticalStrut(10));
        hbox = (Box)box_current.add(Box.createHorizontalBox());
        hbox.add(Box.createRigidArea(new Dimension(10,25)));
        hbox.add(currentPrescalesLabel);
        hbox.add(Box.createHorizontalGlue());
        hbox = (Box)box_current.add(Box.createHorizontalBox());
        hbox.add(Box.createRigidArea(new Dimension(10,25)));
        hbox.add(currentL1Label);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(currentL1ValueLabel);
        hbox.add(Box.createHorizontalStrut(60));
        hbox.add(currentHLTLabel);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(currentHLTValueLabel);
        hbox.add(Box.createHorizontalGlue());


        Box box_new = Box.createVerticalBox();
        box_new.setBorder(BorderFactory.createTitledBorder("New alias selection"));
        box_new.add(Box.createVerticalStrut(10));
        hbox = (Box)box_new.add(Box.createHorizontalBox());
        hbox.add(Box.createRigidArea(new Dimension(10,0)));
        hbox.add(newAliasLabel);
        hbox.add(Box.createHorizontalStrut(30));
        hbox.add(aliasComboBox);
        hbox.add(Box.createHorizontalStrut(10));
        box_new.add(Box.createVerticalStrut(10));
        hbox = (Box)box_new.add(Box.createHorizontalBox());        
        hbox.add(Box.createRigidArea(new Dimension(10,25)));
        hbox.add(newPrescalesLabel);
        hbox.add(Box.createHorizontalGlue());
        hbox = (Box)box_new.add(Box.createHorizontalBox());        
        hbox.add(Box.createRigidArea(new Dimension(10,25)));
        hbox.add(newL1Label);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(newL1ValueLabel);
        hbox.add(Box.createHorizontalStrut(60));
        hbox.add(newHLTLabel);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(newHLTValueLabel);
        hbox.add(Box.createHorizontalGlue());

        Box panel_selection = Box.createVerticalBox();
        panel_selection.add(matchWarningLabel);
        panel_selection.add(box_current);
        panel_selection.add(Box.createVerticalStrut(10));
        panel_selection.add(box_new);
        panel_selection.add(Box.createVerticalGlue());

        /******* panel with the buttons *******/
        Box panel_buttons = Box.createHorizontalBox();
        panel_buttons.setBorder(BorderFactory.createEmptyBorder(10, 10, 10,10));
        panel_buttons.add(overrideWarningLabel);
        panel_buttons.add(Box.createHorizontalGlue());
        panel_buttons.add(CancelButton);
        panel_buttons.add(Box.createRigidArea(new Dimension(10, 0)));
        panel_buttons.add(SelectButton);

        /****** final arrangement */
        JPanel panel_main = new JPanel(new BorderLayout(5,5));
        panel_main.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        panel_main.add(titleLabel, BorderLayout.NORTH);
        panel_main.add(panel_selection);
        panel_main.add(panel_buttons, BorderLayout.SOUTH);

        add(panel_main);

        pack();

        // set min and max dimensions, allowing extension only in width
        int currentHeight = getSize().height+10;
        setMinimumSize(new Dimension(650, currentHeight));
        setMaximumSize(new Dimension(getMaximumSize().width, currentHeight));

    }

    // actions, to override in dialog implementation 
    protected void aliasComboBoxAction(ActionEvent evt)
    {}

    protected void cancelButtonAction(ActionEvent evt) {
        dispose();
    }

    protected void selectButtonAction(ActionEvent evt)
    {}

    /****** private panel components *****/
    protected final JLabel titleLabel = new JLabel("Select Alias for MODE");

    // current
    private final JLabel currentAliasLabel = new JLabel("Current Alias: ");
    protected final JLabel currentAliasNameLabel = new JLabel("currentAlias");
    private final JLabel currentPrescalesLabel = new JLabel("Current Prescales:");
    private final JLabel currentL1Label = new JLabel("L1");
    protected final JLabel currentL1ValueLabel = new JLabel("PSK");
    private final JLabel currentHLTLabel = new JLabel("HLT");
    protected final JLabel currentHLTValueLabel = new JLabel("PSK");

    // new
    protected final JComboBox<String> aliasComboBox = new JComboBox<>();
    private final JLabel newAliasLabel = new JLabel("New Alias: ");
    protected final JLabel newPrescalesLabel = new JLabel("New Prescales:");
    protected final JLabel newL1Label = new JLabel("L1");
    protected final JLabel newL1ValueLabel = new JLabel("PSK");
    protected final JLabel newHLTLabel = new JLabel("HLT");
    protected final JLabel newHLTValueLabel = new JLabel("PSK");

    // info
    protected final JLabel matchWarningLabel = new JLabel("Selected Alias Matches Current One!");
    protected final JLabel overrideWarningLabel = new JLabel("WARNING: changing physics alias will remove override!");

    // buttons
    protected final JButton CancelButton = new JButton("Cancel");
    protected final JButton SelectButton = new JButton("Select");


    public static void main(String[] args) {

        try {
            // Possible values for the look and feel: Metal, Nimbus, CDE/Motif, GTK+
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Gtk+".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(AliasSelectionDialog.class.getName()).log(Level.SEVERE, null, ex);
        }

        EventQueue.invokeLater(() -> {
            AliasSelectionDialog dialog = new AliasSelectionDialog(new JFrame(), true);
            dialog.setVisible(true);
        });
    }
}
