package triggerpanel;

import Igui.IguiLogger;

abstract class OKSVariable < T > {

    private final String var_name;
    
    public OKSVariable(final String var_name) {
        this.var_name = var_name;
        TPGlobal.OKS.addVar(this);
    }

    /***********
     * @brief returns the variable name
     *
     */
    final String name() { 
        return var_name;
    }


    /***********
     * @brief returns the name of the partition
     *
     */
    final String getPartitionName() { 
        return TPGlobal.igui.getPartition().getName();
    }


    /***********
     * @brief access to the local R/O copy of the RDB server
     *
     */
    private config.Configuration roDb() {
        return TPGlobal.getRoDb();
    }


    /***********
     * @brief access to the local R/W RDB server
     *
     * This is only available if the IGUI is in control, otherwise it is null
     *
     */
    private config.Configuration rwDb() {
        return TPGlobal.getRwDb();
    }

    /***********
     * @brief returns the value of the variable 
     *
     * If the IGUI is in Control, this is taken from the RW RDB
     * server, otherwise from the (perhaps outdated) RO RDB server
     *
     */
    final T value() { 
        T val;
        if( rwDb() != null ) { 
            val = rdbvalue();
        } else {
            val = oksvalue();
        }
        return val;
    }

    final T rdbvalue() { return readFromRDB( rwDb() ); }
    final T oksvalue() { return readFromRDB( roDb() ); }
    boolean isCommitted() { 
        return rwDb()==null || rdbvalue().equals(oksvalue());
    }  

    void setValue(final T value) {
        if(rwDb() == null) {
            IguiLogger.error("Can not update OKS for " + name() + " since this IGUI is only in Viewer mode!");
        } else {
            writeToRDB(value, rwDb());
            IguiLogger.info("RDB server updated for " + name() + " with " + value);
        }
    }



    void setRdbValue(final T value) { setValue(value); }
    void setOksValue(final T value) {}
    void abortRdbChanges() {}

    abstract protected void writeToRDB( final T value, config.Configuration rdb_rw);
    abstract protected T  readFromRDB( final config.Configuration rdb);

    @Override
    final public String toString() {
        if(GUIState.isControl() && !isCommitted() ) {
            return "'" + name() + "': " + value() + " ( oks = " + oksvalue() + " )";
        } else {
            return "'" + name() + "': " + value();
        }
    }
}
