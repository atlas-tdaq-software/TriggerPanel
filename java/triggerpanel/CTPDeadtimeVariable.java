/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package triggerpanel;

import config.ConfigObject;

/**
 *
 * @author William
 */
public class CTPDeadtimeVariable extends OKSVariable<String> {

    CTPDeadtimeVariable() {
        super("CTPDeadtimeConfig");
    }

    @Override
    protected void writeToRDB(String value, config.Configuration rdb) {
        TPGlobal.getConfigHelper().setCTPDeadtimeConfig(rdb, value);
    }

    @Override
    protected String readFromRDB(config.Configuration rdb) {

        ConfigObject ctpDeadtimeConfig = TPGlobal.getConfigHelper().getCTPDeadtimeConfig(rdb);
        String deadtimeConfig = "Not in OKS";
        if(ctpDeadtimeConfig != null)
            deadtimeConfig = ctpDeadtimeConfig.UID();
        return deadtimeConfig;

    }
}
