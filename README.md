# Igui panel for the Trigger

Panel for the shifter to interact with the trigger

## Testing at P1

To test the Trigger DB connection at P1, copy `scripts/testDbConnection.sh` to P1, and source it after setting up the tdaq release.
