rm -rf tmp
mkdir -p tmp
cd tmp

cp /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-05-03-02_patches/installed/share/data/L1CTISData/schema/is_trigconf_l1.schema.xml .
cp /afs/cern.ch/atlas/project/tdaq/level1/ctp/l1ct-05-03-02_patches/installed/share/data/L1CTISData/schema/is_ctpcore.schema.xml .

is_generator.sh -d . -java -p triggerpanel -cn TrigConfL1PsKey -n is_trigconf_l1.schema.xml
is_generator.sh -d . -java -p triggerpanel -cn TrigConfL1BgKey -n is_trigconf_l1.schema.xml
is_generator.sh -d . -java -p triggerpanel -cn ISCTPCOREBGKey -n is_ctpcore.schema.xml

cp -v *.java ../java/triggerpanel
